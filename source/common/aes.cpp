#include "aes.h"
#include "string.h"
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <math.h>
#include "base64.h"
#include "global_parameters.h"

#define AES_BLOCK_SIZE				16
/*Encrypt and decrypt variable*/
/* A 256 bit key */
unsigned char aes_key[32]; // = (unsigned char *)"abcdefghijklmnopqrstuvwxyz123456";

/* A 128 bit IV */
unsigned char aes_iv[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

int setKey(unsigned char *key, unsigned key_len) {
	if (key_len != 32) {
		return -1;
	}
	memcpy(aes_key, key, 32);
	return 0;
}

void handleErrors(void) {
	ERR_print_errors_fp(stderr);
}

int aes_encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key, unsigned char *iv, unsigned char *ciphertext) {
	EVP_CIPHER_CTX *ctx;

	int len;

	int ciphertext_len;

	/* Create and initialise the context */
	if (!(ctx = EVP_CIPHER_CTX_new())) {
		handleErrors();
		return -1;
	}

	/* Initialise the encryption operation. IMPORTANT - ensure you use a key
	 * and IV size appropriate for your cipher
	 * In this example we are using 256 bit AES (i.e. a 256 bit key). The
	 * IV size for *most* modes is the same as the block size. For AES this
	 * is 128 bits */
	if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv)) {
		handleErrors();
		return -1;
	}

	/* Provide the message to be encrypted, and obtain the encrypted output.
	 * EVP_EncryptUpdate can be called multiple times if necessary
	 */
	if (1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len)) {
		handleErrors();
		return -1;
	}
	ciphertext_len = len;

	/* Finalise the encryption. Further ciphertext bytes may be written at
	 * this stage.
	 */
	if (1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len)) {
		handleErrors();
		return -1;
	}
	ciphertext_len += len;

	/* Clean up */
	EVP_CIPHER_CTX_free(ctx);

	return ciphertext_len;
}

int aes_decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *key, unsigned char *iv, unsigned char *plaintext) {
	EVP_CIPHER_CTX *ctx;

	int len;

	int plaintext_len;

	/* Create and initialise the context */
	if (!(ctx = EVP_CIPHER_CTX_new())) {
		handleErrors();
		return -1;
	}

	/* Initialise the decryption operation. IMPORTANT - ensure you use a key
	 * and IV size appropriate for your cipher
	 * In this example we are using 256 bit AES (i.e. a 256 bit key). The
	 * IV size for *most* modes is the same as the block size. For AES this
	 * is 128 bits */
	if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv)) {
		handleErrors();
		return -1;
	}

	/* Provide the message to be decrypted, and obtain the plaintext output.
	 * EVP_DecryptUpdate can be called multiple times if necessary
	 */
	if (1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len)) {
		handleErrors();
		return -1;
	}
	plaintext_len = len;

	/* Finalise the decryption. Further plaintext bytes may be written at
	 * this stage.
	 */
	if (1 != EVP_DecryptFinal_ex(ctx, plaintext + len, &len)) {
		handleErrors();
		return -1;
	}
	plaintext_len += len;

	/* Clean up */
	EVP_CIPHER_CTX_free(ctx);

	return plaintext_len;
}

int iot_final_encrypt(unsigned char* client_message, int client_message_length) {

	/*Aes cipher test*/
	unsigned char ciphertext[client_message_length + AES_BLOCK_SIZE];
	int ciphertext_len;

	/* Initialise the library */
	ERR_load_crypto_strings();
	OpenSSL_add_all_algorithms();
	OPENSSL_config(NULL);

	/* Encrypt the plaintext */
	ciphertext_len = aes_encrypt(client_message, client_message_length, aes_key, aes_iv, ciphertext);

	int encodedSize = 4 * ceil((double) ciphertext_len / 3);

	//if encode is larger than cipher
	if (encodedSize > MAX_MESSAGE_LEN) {
		printf("REALOC===============================\n");
		//increase(&client_message, encodedSize+1);
		uint8_t *temp = (uint8_t*) realloc(client_message, (encodedSize + 1) * sizeof(uint8_t));
		if (temp != NULL) {
			client_message = temp;
		} else {
			free(client_message);
			printf("Error allocating memory!\n");
			return -1;
		}
	}

	int b64lenght = Base64encode((char*) client_message, reinterpret_cast<const char*>(ciphertext), ciphertext_len);

	/* Clean up */
	EVP_cleanup();
	ERR_free_strings();
	return b64lenght;
}

int iot_final_decrypt(unsigned char* decryptedtext, unsigned char* ciphertext, int len_ciphertext) {
	/* Buffer for the decrypted text */
	int decryptedtext_len;

	/* Initialise the library */
	ERR_load_crypto_strings();
	OpenSSL_add_all_algorithms();
	OPENSSL_config(NULL);

	/*Find the lenght of the cipher text*/
	int lenght = calcDecodeLength((const char *) ciphertext, len_ciphertext);

	//	printf("cipher text mqtt trong final decrypt: %s\n", ciphertext);
	/*Decode base 64*/
	char afterdecode[lenght];
	for (int var = 0; var < lenght; ++var) {
		afterdecode[var] = 0;
	}

	//	char* afterdecode;
	Base64decode(afterdecode, (const char *) ciphertext);

	/* Decrypt the ciphertext */
	decryptedtext_len = aes_decrypt((unsigned char*) afterdecode, lenght, aes_key, aes_iv, (unsigned char *) decryptedtext);

	/*Add null*/
	*(decryptedtext + decryptedtext_len) = '\0';

	/* Clean up */
	EVP_cleanup();
	ERR_free_strings();
	return decryptedtext_len;
}
