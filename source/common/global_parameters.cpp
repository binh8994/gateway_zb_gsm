/*
 * global_parameters.cpp
 *
 *  Created on: Jun 14, 2017
 *      Author: TuDT13
 */

#include "global_parameters.h"
#include "../app/app_dbg.h"
uint8_t lastSensordataMessage[255];

uint8_t g_u8ServerStatus = ONLINE;
uint8_t g_strTokenFactory[255];
string g_version = "01.0.5h";

//string VALID_SERVER_CODE = "valid_code=pVw3xE4GzH5HtHas";
string VALID_SERVER_CODE = "valid_code=e7uDa4zeVURYGy9L";

string g_strTokenCommunicate = "da87153d2bbd1e30f80980f49a2b1941";
string g_strCommand[] = {
		"1001", //0: add device
		"1002", //1: remove device
		"1003", //2: sensor data
		"1004", //3: control device.
		"1005", //4: cancel add device.
        "1006", //5: update scene
		"0013", //6: reset gateway
		};

string g_strCommandError[] = { "2001", //0: error_adddevice
		"2002", //0: error_removedevice
		};
string g_strSensorType[] = { "UNKNOWN", //unknown
		"ON_OFF",           //on_off sensor
		"TEMPERATURE",      //temp
		"HUMIDITY",         //humi
		"OCCUPANCY_SENSING",         //motion signal
		"INFO", 			//basic cluster
		"NAME",             // name sensor ;
		"BATTERY_VOLTAGE",  // Battery
		"PRE_STATUS", // previos status
		"STATUS",   // current status
		"LOAD_POWER", // load power of smart plug
		"LOAD_VOLTAGE", //load voltage of smart plug
		"VALUE_1",       // Value 1 of data string atribute 0xFF01
		"VALUE_2",  // Value 2 of data string atribute 0xFF01
		"SMOKING_ALARM",  //Smoking alarm
		};
// mac address
char s_ieee_address[17];

zclOutgoingMsg_t g_outgoingMsg_lastData;
void bytetoHexChar(uint8_t ubyte, uint8_t *uHexChar);
void bytestoHexChars(uint8_t *ubyte, int32_t len, uint8_t *uHexChar);

void hexChartoByte(uint8_t *uHexChar, uint8_t *ubyte);
void hexCharsToBytes(uint8_t *uHexChar, int32_t len, uint8_t *ubyte);
parse_communicate_string_t parseClientString(uint8_t *client_message, int32_t len);
void string_copy(uint8_t *dest, uint8_t *src, uint8_t len);

void bytetoHexChar(uint8_t ubyte, uint8_t *uHexChar) {
	uHexChar[1] = ((ubyte & 0x0F) < 10) ? ((ubyte & 0x0F) + '0') : (((ubyte & 0x0F) - 10) + 'A');
	uHexChar[0] = ((ubyte >> 4 & 0x0F) < 10) ? ((ubyte >> 4 & 0x0F) + '0') : (((ubyte >> 4 & 0x0F) - 10) + 'A');
}

void bytestoHexChars(uint8_t *ubyte, int32_t len, uint8_t *uHexChar) {
	for (int8_t i = 0; i < len; i++) {
		bytetoHexChar(ubyte[i], (uint8_t*) &uHexChar[i * 2]);
	}
}

void hexChartoByte(uint8_t *uHexChar, uint8_t *ubyte) {

	*ubyte = 0;
	*ubyte = ((uHexChar[0] <= '9' && uHexChar[0] >= '0') ? ((uHexChar[0] - '0') << 4) : *ubyte);
	*ubyte = ((uHexChar[0] <= 'F' && uHexChar[0] >= 'A') ? ((uHexChar[0] - 'A' + 10) << 4) : *ubyte);

	*ubyte = ((uHexChar[1] <= '9' && uHexChar[1] >= '0') ? *ubyte | (uHexChar[1] - '0') : *ubyte);
	*ubyte = ((uHexChar[1] <= 'F' && uHexChar[1] >= 'A') ? *ubyte | ((uHexChar[1] - 'A') + 10) : *ubyte);

}

void hexCharsToBytes(uint8_t *uHexChar, int32_t len, uint8_t *ubyte) {
	for (int8_t i = 0; i < len; i += 2) {
		hexChartoByte((uint8_t*) &uHexChar[i], (uint8_t *) &ubyte[i / 2]);
	}
}

/* parse client message follow */
parse_communicate_string_t parseClientString(uint8_t *client_message, int32_t len) {
	parse_communicate_string_t parseString;
	parseString.totalString = 0;
	parseString.position[0] = 0;
	for (int32_t i = 0; i < len; i++) {
		if (client_message[i] == SEPARATE_CHAR) {
			parseString.position[parseString.totalString] = i + 1;
			if (parseString.totalString > 0) {
				parseString.len[parseString.totalString - 1] = parseString.position[parseString.totalString] - parseString.position[parseString.totalString - 1] - 1;
			}
			APP_PRINT("position [%d] = %d \n", parseString.totalString, parseString.position[parseString.totalString]);
			parseString.totalString++;
			if (parseString.totalString == (LEN_COMMUNICATE_STRING - 1)) {
				APP_PRINT("overload totalString!");
				break;
			}
		}
	}
	parseString.position[parseString.totalString] = len;
	if (parseString.totalString > 0) {
		parseString.len[parseString.totalString - 1] = parseString.position[parseString.totalString] - parseString.position[parseString.totalString - 1];
	}

	APP_PRINT("total = %d \n", parseString.totalString);
	return parseString;
}

void string_copy(uint8_t *dest, uint8_t *src, uint8_t len) {
	if (len > 0) {
		memcpy(dest, src, len);
		dest[len] = '\0';
	}
}
#ifdef AES_ENCRYPT_FLAG
int remove_all_chars(char* str, char c, int len) {
	char *pr = str, *pw = str;
	int new_len=len;
	while (len <= 0) {
		if(*pr != c) {
			pr++;
			new_len--;
		}
		else {
			*pw = *pr;
			pw++;
			pr++;
		}
		len--;
	}
	return new_len;
}
#endif
