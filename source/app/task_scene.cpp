#include "task_scene.h"

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

#include "../ak/ak.h"

#include "../sys/sys_dbg.h"

#include "scene.h"

#include "app.h"
#include "app_dbg.h"
#include "global_parameters.h"

#include "task_list.h"

q_msg_t gw_task_scene_mailbox;
static size_t download_callback(void *ptr, size_t size, size_t nmemb, FILE *stream);

void* gw_task_scene_entry(void*) {
	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("gw_task_scene_entry\n");
	scene_initializer("/scene.txt");

	while (1) {
		while (msg_available(ID_TASK_SCENE)) {
			/* get messge */
			ak_msg_t* msg = rev_msg(ID_TASK_SCENE);

			/* handler message */
			switch (msg->header->sig) {
			case DELETE_SCENE: {
				APP_PRINT("delete scene\n");
				delete_scene_file();
			}
				break;
			case GET_SCENE: {
				//zclOutgoingMsg_t* scene_senser_in = (zclOutgoingMsg_t*)msg->header->payload;
				parser_scene_file((uint8_t*) msg->header->payload, msg->header->len);
			}
				break;
			case UPDATE_SCENE: {
				APP_PRINT("GET_SCENE_FROM_SERVER\n");
				char * url = (char*) msg->header->payload;
				APP_PRINT("url=====================: %s\n", url);
				CURL *curl;
				CURLcode res;
				FILE *scene_file;
				char filepath[256];
				get_scene_path_file(filepath);
				printf("File path: %s\n", filepath);
				curl = curl_easy_init();
				if (curl) {
					scene_file = fopen(filepath, "wb+");
					curl_easy_setopt(curl, CURLOPT_URL, url);
					curl_easy_setopt(curl, CURLOPT_POSTFIELDS, VALID_SERVER_CODE.c_str());
					curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, download_callback);
					curl_easy_setopt(curl, CURLOPT_WRITEDATA, scene_file);
					curl_easy_setopt(curl, CURLOPT_TIMEOUT, 20L);
					/* get it! */
					res = curl_easy_perform(curl);

					/* check for errors */
					if (res != CURLE_OK) {
						APP_PRINT("curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
					} else {

					}
					curl_easy_cleanup(curl);
					fclose(scene_file);
				}
				APP_PRINT("Scene file Updated");
			}
				break;
			default:
				break;
			}

			/* free message */
			free_msg(msg);
		}

		usleep(1000);
	}

	return (void*) 0;
}

size_t download_callback(void *ptr, size_t size, size_t nmemb, FILE *stream) {
	size_t written;
	written = fwrite(ptr, size, nmemb, stream);
	return written;
}

