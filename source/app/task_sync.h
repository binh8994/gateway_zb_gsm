#ifndef TASK_SYNC_H
#define TASK_SYNC_H

#include "../ak/ak.h"
#include "../ak/message.h"

#define SYNC_INIT				0
#define SYNC_PING				1

extern void tsm_sync_ping_on_state(tsm_state_t);

extern tsm_t* tsm_sync_ping_table[];
extern tsm_tbl_t tsm_sync_ping;

extern q_msg_t gw_task_sync_mailbox;
extern void* gw_task_sync_entry(void*);

#endif // TASK_SYNC_H
