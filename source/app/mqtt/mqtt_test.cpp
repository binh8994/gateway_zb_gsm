#include <string.h>

#include "../ak/ak.h"

#include "mqtt_test.h"

#include "app.h"
#include "app_dbg.h"
#include "../common/global_parameters.h"
#ifdef AES_ENCRYPT_FLAG
#include "../common/aes.h"
#endif
#include "task_list.h"

mqtt_test::mqtt_test(const char *id, const char *host, int port, char* username, char* password) :
		mosquittopp(id, false) {
	/* init private data */
	m_connect_ok_flag = -1;
	m_mid = 1;

	/* init mqtt */
	mosqpp::lib_init();

	/* connect */
	username_pw_set(username, password);
	connect_async(host, port, 60);
	loop_start();
}

mqtt_test::~mqtt_test() {
	loop_stop();
	mosqpp::lib_cleanup();
}

void mqtt_test::test_public(const char* topic, uint8_t* msg, uint32_t len, bool retain) {
	APP_DBG("[mqtt_test][public] msg:%s len:%d\n", msg, len);
	publish(&m_mid, topic, len, msg, 1, true); //qos = 1
}

void mqtt_test::on_connect(int rc) {
	if (rc == 0) {
		m_connect_ok_flag = 0;
		APP_DBG("[mqtt_test] on_connect OK: %d\n", rc);
	} else {
		APP_DBG("[mqtt_test] on_connect ERROR: %d\n", rc);
	}
}

void mqtt_test::on_disconnect(int rc) {
	APP_DBG("[mqtt_test] on_disconnect: %d\n", rc);

	//reconnect_async();
}


void mqtt_test::on_message(const struct mosquitto_message *message) {
	if (message->payloadlen > 0) {
		APP_DBG("[mqtt_test][on _message] topic:%s\tpayloadlen:%d\n", message->topic, message->payloadlen);

		if (message->retain == true) {
			publish(&m_mid, message->topic, 0, NULL, 1, true);
		}

	}
}

