#include <sys/types.h>
#include <sys/stat.h>

#include <string.h>

#include "../ak/ak.h"

#include "app.h"
#include "app_dbg.h"

app_config gateway_configure;
config_parameter_t g_config_parameters;

//string ac_firmware_name = "/ac_application.bin";
//string wr_firmware_name = "/wr_application.bin";

static app_connect_cloud_state_mng_t  app_connect_cloud_state;

void set_app_connect_cloud_state(connect_cloud_state_t stt){
	pthread_mutex_lock(&(app_connect_cloud_state.mt));
	app_connect_cloud_state.current_state = stt;
	pthread_mutex_unlock(&(app_connect_cloud_state.mt));
}

connect_cloud_state_t get_app_connect_cloud_state() {
	connect_cloud_state_t ret;
	pthread_mutex_lock(&(app_connect_cloud_state.mt));\
	ret = app_connect_cloud_state.current_state;
	pthread_mutex_unlock(&(app_connect_cloud_state.mt));
	return ret;
}

void set_enable_connect_gsm(bool stt) {
	pthread_mutex_lock(&(app_connect_cloud_state.mt));
	app_connect_cloud_state.enable_connect_gsm = stt;
	pthread_mutex_unlock(&(app_connect_cloud_state.mt));
}

bool get_enable_connect_gsm() {
	bool ret;
	pthread_mutex_lock(&(app_connect_cloud_state.mt));\
	ret = app_connect_cloud_state.enable_connect_gsm;
	pthread_mutex_unlock(&(app_connect_cloud_state.mt));
	return ret;
}
