/*
 * task_handle_msg.h
 *
 *  Created on: Apr 24, 2017
 *      Author: TuDT13
 */

#ifndef SOURCE_APP_TASK_HANDLE_MSG_H_
#define SOURCE_APP_TASK_HANDLE_MSG_H_

#include "../ak/message.h"

extern q_msg_t gw_task_handle_msg_mailbox;
extern void* gw_task_handle_msg_entry(void*);

#endif /* SOURCE_APP_TASK_HANDLE_MSG_H_ */
