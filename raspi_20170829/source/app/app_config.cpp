#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../common/global_parameters.h"
#include "../common/aes.h"
#include "app.h"
#include "app_dbg.h"
#include "app_config.h"

string config_folder;
string config_file_path;

app_config::app_config() {

}
void app_config::initializer(char * file_name) {
    struct stat st = { 0 };
	/* create config path */
	string config_folder = static_cast<string>(APP_ROOT_PATH) + static_cast<string>("/config");
	if (stat(config_folder.data(), &st) == -1) {
		mkdir(config_folder.data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	}

	string config_file_path = config_folder + static_cast<string>((const char*) file_name);

	APP_PRINT("config_file_path: %s\n", config_file_path.c_str());
	strcpy(m_config_path, config_file_path.c_str());
}

void app_config::set_config_path_file(char* path) {
	strcpy(m_config_path, (const char*) path);
}

void app_config::get_config_path_file(char* path) {
	strcpy(path, (const char*) m_config_path);
}

int app_config::parser_config_file(config_parameter_t* cfg, uint8_t *aes_key) {
	struct stat file_info;
	int32_t configure_file_obj = -1;
	int buffer_len;
	char * buffer;

	/*Open config file*/
	configure_file_obj = open(m_config_path, O_RDONLY);

	if (configure_file_obj < 0) {
		return -1;
	}

	fstat(configure_file_obj, &file_info);

	buffer_len = file_info.st_size + 1;
	buffer = (char*) malloc(buffer_len);
	buffer[buffer_len - 1] = '\0';
	char* root_buffer = buffer;

	if (buffer == NULL) {
		return -1;
	}

	/*get data*/
	pread(configure_file_obj, buffer, file_info.st_size, 0);
//	APP_PRINT("buffer: %s\n", buffer);
	close(configure_file_obj);
	/*Parse data*/
	int32_t len = strlen(buffer);
#ifdef AES_ENCRYPT_FLAG
	setKey((unsigned char*) aes_key, 32);
	len = iot_final_decrypt((unsigned char *) buffer, (unsigned char *) buffer, len);
	if (len <= 0) {
		free(root_buffer);
		return -1;
	}
#endif
	parse_communicate_string_t parseMessage = parseClientString((uint8_t*) buffer, len);
	if (parseMessage.totalString == 9) {

		uint8_t port_str[10];
		int i = 0;
		if (parseMessage.len[i] > 9 || parseMessage.len[i] < 0) {

		} else {
			//UDP port
			string_copy(port_str, (uint8_t*) &buffer[parseMessage.position[i]], parseMessage.len[i]);
			cfg->broadcast_udp_port = atoi((const char*) port_str);
		}

		//host
		i++;
		string_copy(cfg->mqtt_host, (uint8_t*) &buffer[parseMessage.position[i]], parseMessage.len[i]);

		//port
		i++;
		if (parseMessage.len[i] > 9 || parseMessage.len[i] < 0) {

		} else {
			//UDP port
			string_copy(port_str, (uint8_t*) &buffer[parseMessage.position[i]], parseMessage.len[i]);
			cfg->mqtt_port = atoi((const char*) port_str);
		}

		//user name
		i++;
		string_copy(cfg->mqtt_username, (uint8_t*) &buffer[parseMessage.position[i]], parseMessage.len[i]);
		//password
		i++;
		string_copy(cfg->mqtt_password, (uint8_t*) &buffer[parseMessage.position[i]], parseMessage.len[i]);
		//topic
		i++;
		string_copy(cfg->mqtt_topic, (uint8_t*) &buffer[parseMessage.position[i]], parseMessage.len[i]);
		//token factory
		i++;
		string_copy(cfg->token_communicate, (uint8_t*) &buffer[parseMessage.position[i]], parseMessage.len[i]);
		//security key
		i++;
		string_copy(cfg->security_key, (uint8_t*) &buffer[parseMessage.position[i]], parseMessage.len[i]);
		//start up success
		i++;
		if (parseMessage.len[i] > 3 || parseMessage.len[i] < 0) {

		} else {
			string_copy(port_str, (uint8_t*) &buffer[parseMessage.position[i]], parseMessage.len[i]);
			cfg->start_up_success = atoi((const char*) port_str);
		}
	} else {
		printf("parse error\n");
		// terminate
		close(configure_file_obj);
		free(root_buffer);
		return -1;
	}
	free(root_buffer);
	return 0;
}

int app_config::write_config_data(config_parameter_t * cfg, uint8_t *aes_key) {

	FILE* configure_file_obj = NULL;
	configure_file_obj = fopen(m_config_path, "w+b");
	if (configure_file_obj == NULL) {
		return -1;
	}

	int i = 0;
	uint8_t *message_str = (uint8_t *) malloc(MAX_MESSAGE_LEN * sizeof(uint8_t));
	memcpy(message_str, mqtt_host_domain_str, strlen(mqtt_host_domain_str));
	i = strlen(mqtt_host_domain_str);

	//user name
	message_str[i] = SEPARATE_CHAR;
	i++;
	string_copy((uint8_t*) &message_str[i], cfg->mqtt_username, strlen((const char*) cfg->mqtt_username));
	i += strlen((const char*) cfg->mqtt_username);
	//password
	message_str[i] = SEPARATE_CHAR;
	i++;
	string_copy((uint8_t*) &message_str[i], cfg->mqtt_password, strlen((const char*) cfg->mqtt_password));
	i += strlen((const char*) cfg->mqtt_password);
	//topic
	message_str[i] = SEPARATE_CHAR;
	i++;
	string_copy((uint8_t*) &message_str[i], cfg->mqtt_topic, strlen((const char*) cfg->mqtt_topic));
	i += strlen((const char*) cfg->mqtt_topic);
	//token factory
	message_str[i] = SEPARATE_CHAR;
	i++;
	string_copy((uint8_t*) &message_str[i], cfg->token_communicate, strlen((const char*) cfg->token_communicate));
	APP_PRINT("\nlen comm = %d\n", strlen((const char* ) cfg->token_communicate));
	i += strlen((const char*) cfg->token_communicate);
	message_str[i] = SEPARATE_CHAR;
	i++;
	string_copy((uint8_t*) &message_str[i], cfg->security_key, strlen((const char*) cfg->security_key));
	i += strlen((const char*) cfg->security_key);
	message_str[i] = SEPARATE_CHAR;
	i++;
	uint8_t str_start[2];
	bytetoHexChar(cfg->start_up_success, str_start);
	string_copy((uint8_t*) &message_str[i], str_start, 2);
	i += 2;
	message_str[i] = '\0';
	i++;
#ifdef AES_ENCRYPT_FLAG
	setKey((unsigned char*) aes_key, 32);
	int postmessagelen = iot_final_encrypt(message_str, i);
	if (postmessagelen <= 0) {
		free(message_str);
		fclose(configure_file_obj);
		return -1;
	}
	i = postmessagelen;
#endif
	fwrite(message_str, strlen((const char*) message_str), 1, configure_file_obj);
	APP_PRINT("[%s]", message_str);
	fclose(configure_file_obj);
	free(message_str);
	return 0;
}
uint8_t read_config_file(uint8_t * aes_key) {
	memset((void*) &g_config_parameters, 0, sizeof(config_parameter_t));
	gateway_configure.initializer((char*) "/gateway_config.txt");
	while (gateway_configure.parser_config_file(&g_config_parameters, aes_key) != 0) {
		gateway_configure.write_config_data(&g_config_parameters, aes_key);
	}

	/*Gateway*/

	APP_DBG("VERSION:				%s\n", g_version.c_str());
	APP_DBG("Initialize udp_port:			%d\n", g_config_parameters.broadcast_udp_port);
	APP_DBG("Initialize mqtt_host:			%s\n", g_config_parameters.mqtt_host);
	APP_DBG("Initialize mqtt_port:			%d\n", g_config_parameters.mqtt_port);

	/*Gateway-server*/
	APP_DBG("Initialize mqtt_username:		%s\n", g_config_parameters.mqtt_username);
	APP_DBG("Initialize mqtt_password:		%s\n", g_config_parameters.mqtt_password);
	APP_DBG("Initialize mqtt_topic:			%s\n", g_config_parameters.mqtt_topic);
	APP_DBG("Initialize token_communicate:	%s\n", g_config_parameters.token_communicate);
	APP_DBG("start up coordinator success   %d\n", g_config_parameters.start_up_success);

	return 0;
}

