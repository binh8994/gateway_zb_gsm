#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../ak/ak.h"

#include "app.h"
#include "app_dbg.h"

#include "if_console.h"
#include "../../common/global_parameters.h"
#include "task_list.h"
#include "../znp/znp_serial.h"
#include "scene.h"

#define FACC_LOGIN_OPT					(0x00)
#define INTERNAL_LOGIN_OPT				(0x01)

q_msg_t gw_task_if_console_mailbox;

static unsigned char cmd_buf[CMD_BUFFER_SIZE];
static int i_get_command(unsigned char* cmd_buf);

const char* __internal = "__internal\n";
const char* __facc = "__facc\n";

void* gw_task_if_console_entry(void*) {
	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("gw_task_if_console_entry\n");

	while (1) {
//#if USE_IF_CONSOLE
		if (i_get_command(cmd_buf) == 0) {
			APP_PRINT("if console = %s \n", cmd_buf);
			if (cmd_buf[0] == '1') {
				uint8_t str_ac_sensor = TIME_ENABLE_PERMIT_JOIN;
				ak_msg_t* s_msg = get_dynamic_msg();
				set_msg_sig(s_msg, ZB_PERMIT_JOINING_REQUEST);
				set_data_dynamic_msg(s_msg, (uint8_t*) &str_ac_sensor, 1);

				set_msg_src_task_id(s_msg, ID_TASK_IF_CONSOLE);
				task_post(ID_TASK_IF_ZNP, s_msg);

			} else if (cmd_buf[0] == '2') {
				uint8_t str_ac_sensor = 0x01;
				ak_msg_t* s_msg = get_dynamic_msg();
				set_msg_sig(s_msg, UTIL_GET_DEVICE_INFO);
				set_data_dynamic_msg(s_msg, (uint8_t*) &str_ac_sensor, 1);

				set_msg_src_task_id(s_msg, ID_TASK_IF_CONSOLE);
				task_post(ID_TASK_IF_ZNP, s_msg);
			} else if (cmd_buf[0] == '3') {
				cmd_buf[34] = '\0';
				APP_PRINT("%s", cmd_buf);

				zdo_mgmt_leave_req_t zdoMgmtLeaveReq;

				int8_t i;
				i = 1;
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2 * sizeof(uint16_t), (uint8_t*) &zdoMgmtLeaveReq.short_address);
				i += 2 * sizeof(uint16_t);
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2 * 8, (uint8_t*) &zdoMgmtLeaveReq.device_address[0]);
				i += 2 * 8;
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2, (uint8_t*) &zdoMgmtLeaveReq.flags);
				i += 2;
				//memcpy((uint8_t*) &zdoMgmtLeaveReq, (uint8_t*) &client_message[10], sizeof(zdo_mgmt_leave_req_t));

				ak_msg_t* s_msg = get_dynamic_msg();
				set_msg_sig(s_msg, ZDO_MGMT_LEAVE_REQ);
				set_data_dynamic_msg(s_msg, (uint8_t*) &zdoMgmtLeaveReq, sizeof(zdo_mgmt_leave_req_t));
				set_msg_src_task_id(s_msg, ID_TASK_IF_CONSOLE);
				task_post(ID_TASK_IF_ZNP, s_msg);
			} else if (cmd_buf[0] == '4') {
				cmd_buf[34] = '\0';
				APP_PRINT("%s", cmd_buf);

				af_data_request_t afDataRequest;

				int32_t i;
				i = 1;
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2 * sizeof(uint16_t), (uint8_t*) &afDataRequest.dst_address);
				i += 2 * sizeof(uint16_t);
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2, (uint8_t*) &afDataRequest.dst_endpoint);
				i += 2;
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2, (uint8_t*) &afDataRequest.src_endpoint);
				i += 2;
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2 * sizeof(uint16_t), (uint8_t*) &afDataRequest.cluster_id);
				i += 2 * sizeof(uint16_t);
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2, (uint8_t*) &afDataRequest.trans_id);
				i += 2;
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2, (uint8_t*) &afDataRequest.options);
				i += 2;
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2, (uint8_t*) &afDataRequest.radius);
				i += 2;
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2, (uint8_t*) &afDataRequest.len);
				i += 2;

				uint8_t *data = (uint8_t*) malloc(sizeof(af_data_request_t) + afDataRequest.len);
				memcpy(data, (uint8_t*) &afDataRequest, sizeof(af_data_request_t));
				hexCharsToBytes((uint8_t*) &cmd_buf[i], 2 * afDataRequest.len, (uint8_t*) &data[sizeof(af_data_request_t)]);
				i += 2 * afDataRequest.len;

				ak_msg_t* s_msg = get_dynamic_msg();
				set_msg_sig(s_msg, AF_DATA_REQUEST);
				set_data_dynamic_msg(s_msg, data, (sizeof(af_data_request_t) + afDataRequest.len));
				set_msg_src_task_id(s_msg, ID_TASK_IF_CONSOLE);
				task_post(ID_TASK_IF_ZNP, s_msg);

				free(data);
			} else if (cmd_buf[0] == '5') {
				uint8_t datax[] = ":5E33:00:00:{:EDA5:0301000600000003010001:}\n:5E33:00:01:{:EDA5:0301000600000003010000:EDA5:03010006000000050000000000:}";
				uint8_t *datay;
				printf("memcpy data\n");

				uint32_t len = strlen((const char*) datax);
				datay = (uint8_t*) malloc(len);
				uint8_t* data;

				printf("len x %d", strlen((const char*) datax));
				memcpy(datay, &datax[0], strlen((const char*) datax));

				add_new_scene_file(datay, len);
				// add_new_scene_file

			} else if (cmd_buf[0] == '7') {

				delete_scene_file();
				// add_new_scene_file

			} else if(cmd_buf[0] == '8' ) {
				ak_msg_t* smsg= get_dynamic_msg();
				set_data_dynamic_msg(smsg, (uint8_t*)"Test sms =))", strlen("Test sms =))"));
				set_msg_sig(smsg, GSM_SEND_SMS);;
				task_post(ID_TASK_GSM, smsg);
			}


			/* clean command buffer */
			memset(cmd_buf, 0, CMD_BUFFER_SIZE);
		}
		usleep(1000);
	}
	return (void*) 0;
}

int i_get_command(unsigned char* cmd_buf) {
	unsigned char c = 0;
	unsigned char index = 0;

	do {
		c = getchar();
		if (c >= 0x20 && c < 0x7F) {
			cmd_buf[index++] = c;
		}
		if (index > CMD_BUFFER_SIZE) {
			index = 0;
			APP_PRINT("ERROR: buffer overload !\n");
			return (-1);
		}

		/* sleep 100us */
		usleep(1000);

	} while (c != '\n');

	cmd_buf[index] = 0;
	return (0);
}
