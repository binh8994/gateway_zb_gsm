#ifndef __APP_H__
#define __APP_H__

#include <string>
#include "app_config.h"
#include "../app/znp/zcl.h"
using namespace std;

/*****************************************************************************/
/*  gateway task mqtt define
 */
/*****************************************************************************/

/* define signal of task handle message*/
enum{
	HANDLE_MESSAGE_DATA_INCOMING,
};

/* define signal of task MQTT*/
enum{
	MQTT_CTRL_ADD_NEW_ZED_REQ,	/*add a new ZED (zigbee end device)*/
	MQTT_CTRL_GET_DEVICE_LIST_REQ,  /*get device list from  UTIL_GET_DEVICE_INFO (0x2700)*/
	MQTT_CTRL_GET_DEVICE_LIST_RESPONSE,
	MQTT_START_TOKEN_FACTORY,
	MQTT_STOP_TOKEN_FACTORY,
	MQTT_START_TOKEN_COMMUNICATION,
	MQTT_ADD_GATEWAY,
	MQTT_DATA_COMMUNICATION,
	MQTT_SENSOR_DATA,
};


/* define signal of task task console*/
enum{
	TASK_CONSOLE_ADD_NEW_ZED_REQ,	/*add a new ZED (zigbee end device)*/
	TASK_CONSOLE_ADD_NEW_ZED_RESPONSE,	/*add a new ZED (zigbee end device)*/
	TASK_CONSOLE_INCOMING_MSG,
	TASK_CONSOLE_GET_DEVICE_LIST,

};

/* define signal of task IF_SOCKET*/
enum{
	IF_SOCKET_ADD_NEW_ZED_RESPONSE,	/*add a new ZED (zigbee end device)*/
	IF_SOCKET_SENSOR_DATA, /*data from sensor devices send to socket*/
	IF_SOCKET_LEAVE_DEVICE_RESPONSE,
	IF_SOCKET_ALIVE,
	IF_SOCKET_GET_DEVICE_LIST,

};

/*define signal of task IF_ADD_GATEWAY*/
enum{
	IF_ADD_GATEWAY_USERNAME,
	IF_ADD_GATEWAY_PASSWORD,
	IF_ADD_GATEWAY_TOPIC,
	IF_ADD_GATEWAY_TOKEN_COMMUNICATE,
};

/* define signal of task LOG */
enum{
	LOG_SENSOR_DATA,
	LOG_SENSOR_DATA_ACTIVITY_DATA,
	LOG_GATEWAY_ACTIVITY,
	LOG_GATEWAY_RESET,
};

/* define signal of task socket online*/
enum{
	IF_SOCKET_ONLINE_SENSOR_DATA, /*data from sensor devices send online socket*/
	IF_SOCKET_ONLINE_ADD_NEW_ZED_RESPONSE,	/*add a new ZED (zigbee end device)*/
};

/* define signal of task scene*/
enum{
    UPDATE_SCENE,      		/*update new scene*/
    DELETE_SCENE,   		/*delete scene */
    GET_SCENE,      		/*get output scene*/
};

/*****************************************************************************/
/*  task MT_UDP_BROADCAST define.
 */
/*****************************************************************************/
/* define timer */
#define GW_UDP_BROADCAST_IF_TIMER_PACKET_DELAY_INTERVAL		(5000) /* milliseconds */

/* define signal */
enum{
	GW_UDP_BROADCAST_SHOT, /*create to execute broadcast udp 1 shot*/
	GW_UDP_MAC_ADDRESS_RSP,/*MAC ADDRESS response from if_znp*/
};



/* define signal of task sync*/
#define MT_SYNC_COMMON_INTERVAL								(1000)
enum{
	SYNC_CHECK_CONNECT,
	SYNC_CREATE_SOCKET,
	SYNC_SEND_ICMP,
	SYNC_RECV_ICMP,
};

/* define signal of task gsm*/
#define MT_GSM_COMMON_INTERVAL								(50)
enum{
	GSM_INIT,
	GSM_VERIFY_REQ,
	GSM_READY_POL,
	GSM_TCP_INIT,
	GSM_CHECK_TCP_STATE,
	GSM_START_TCP,
	GSM_SHUT_TCP,
	GSM_MQTT_PING,
	GSM_MQTT_SEND_CONNECT,
	GSM_MQTT_SEND_DISCONNECT,
	GSM_MQTT_PUB_MSG,
	GSM_SEND_SMS,
	GSM_AT_CMD,
};


/* connect state: GSM or LAN*/
typedef enum {APP_CONNECT_BY_GSM = 1, APP_CONNECT_BY_LAN = 2} connect_cloud_state_t;

typedef struct {
	pthread_mutex_t mt;

	connect_cloud_state_t current_state;
	bool enable_connect_gsm;
} app_connect_cloud_state_mng_t;

typedef struct  {
	int topic_len;
	char* topic;

	int messages_len;
	char* messages;
} mqtt_publish_message_t;

/*****************************************************************************/
/*  global define variable
 */
/*****************************************************************************/
#define APP_OK									(0x00)
#define APP_NG									(0x01)

#define APP_FLAG_OFF							(0x00)
#define APP_FLAG_ON								(0x01)

#define APP_ROOT_PATH							"./FPT_Smarthome"
#define LOG_FILE_NAME							"log_data_"
#define SCENE_FILE_NAME							"scene_data"

extern app_config gateway_configure;
extern config_parameter_t g_config_parameters;
extern zclOutgoingMsg_t g_outgoingMsg_lastData;
extern char mobile_number[20];

extern void set_app_connect_cloud_state(connect_cloud_state_t);
extern connect_cloud_state_t get_app_connect_cloud_state();
extern void set_enable_connect_gsm(bool);
extern bool get_enable_connect_gsm();
#endif // __APP_H__
