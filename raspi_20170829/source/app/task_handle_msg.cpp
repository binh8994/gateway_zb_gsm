/*
 * task_handle_msg.cpp
 *
 *  Created on: Apr 24, 2017
 *      Author: TuDT13
 */

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

#include "app.h"
#include "app_dbg.h"

#include "task_list.h"

#include "znp/zcl.h"
#include "znp/znp_serial.h"
#include "../common/global_parameters.h"
#include "../znp/zcl_general.h"
#include "../znp/zcl.h"
#include "../znp/hal_defs.h"
#ifdef AES_ENCRYPT_FLAG
#include "../common/aes.h"
#endif
q_msg_t gw_task_handle_msg_mailbox;

int8_t isTokenCommunicate(uint8_t *client_message, int32_t len);
int8_t isTime(uint8_t *client_message, int32_t len);

void setErrCommand(uint8_t*client_message, int8_t posStrError);
int8_t getCommand(uint8_t *client_message, int32_t len);
int8_t processingDataCommunication(uint8_t *client_message, int32_t len, uint32_t src_task_id, int32_t *outCmd);
void resetDelete();

void sendSensorData(zclOutgoingMsg_t outgoingMsg);

typedef struct
		__attribute__((__packed__))
{
	uint16_t b16ZoneStatus; //zbmap16
	uint8_t b8ExtendedStatus; //zbmap8
	uint8_t u8ZoneId;
	uint16_t u16Delay;
} tsCLD_IASZone_StatusChangeNotificationPayload;

void* gw_task_handle_msg_entry(void*) {
	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("gw_task_handle_msg_entry\n");

	/* start timer to query AC sensor data */
	//timer_set(ID_TASK_HANDLE_MSG, GW_SENSOR_AC_SENSOR_REPORT_REQ, GW_SENSOR_TIMER_AC_SENSOR_REPORT_REQ_INTERVAL, TIMER_PERIODIC);
	while (1) {
		while (msg_available(ID_TASK_HANDLE_MSG)) {
			/* get messge */
			ak_msg_t* msg = rev_msg(ID_TASK_HANDLE_MSG);

			/* handler message */
			switch (msg->header->sig) {
			case HANDLE_MESSAGE_DATA_INCOMING: {
				zclOutgoingMsg_t outgoingMsg; //data send to mqtt
				uint8_t* data = (uint8_t *) msg->header->payload;
				outgoingMsg = *(zclOutgoingMsg_t *) data;
				data += sizeof(zclOutgoingMsg_t);
				outgoingMsg.data = data;
				sendSensorData(outgoingMsg);
			}
				break;
			default:
				APP_PRINT("unknown signal handle message!");
				break;
			}
			/* free message */
			free_msg(msg);
		}

		usleep(1000);
	}

	return (void*) 0;
}

/*sensor data*/
void sendSensorData(zclOutgoingMsg_t outgoingMsg) {

	//	APP_DBG("sensor data begin\n");
	uint8_t topic_string[MAX_MESSAGE_LEN];
	uint8_t topic_len = 0;
	//	uint8_t client_message[MAX_MESSAGE_LEN];
	uint8_t* client_message = (uint8_t *) malloc(MAX_MESSAGE_LEN * sizeof(uint8_t));

	int32_t message_len = 0;
	for (int var = 0; var < MAX_MESSAGE_LEN; ++var) {
		client_message[var] = 0;
	}
	int32_t i = 0;
	int8_t flag_unknown_cluster = 0;
	//add token communicate
	//	client_message[i] = SEPARATE_CHAR;
	//	i++;
	//	memcpy((uint8_t*) &client_message[i], (uint8_t*) g_strSensorType[TOKEN_COMMUINICATION].c_str(), g_strSensorType[TOKEN_COMMUINICATION].size());
	//	i += g_strSensorType[TOKEN_COMMUINICATION].size();

	client_message[i] = SEPARATE_CHAR;
	i++;
	memcpy((uint8_t*) &client_message[i], g_strTokenCommunicate.c_str(), g_strTokenCommunicate.size());
	i += g_strTokenCommunicate.size();

	//add time
	//	client_message[i] = SEPARATE_CHAR;
	//	i++;
	//	memcpy((uint8_t*) &client_message[i], (uint8_t*) g_strSensorType[TIME].c_str(), g_strSensorType[TIME].size());
	//	i += g_strSensorType[TIME].size();

	client_message[i] = SEPARATE_CHAR;
	i++;

	time_t rawtime = time(NULL);
	bytestoHexChars((uint8_t*) &rawtime, sizeof(time_t), (uint8_t*) &client_message[i]);
	i += sizeof(time_t) * 2;

	//add command sensor data
	//	client_message[i] = SEPARATE_CHAR;
	//	i++;
	//	memcpy((uint8_t*) &client_message[i], (uint8_t*) g_strSensorType[COMMAND_NAME].c_str(), g_strSensorType[COMMAND_NAME].size());
	//	i += g_strSensorType[COMMAND_NAME].size();

	client_message[i] = SEPARATE_CHAR;
	i++;

	memcpy((uint8_t*) &client_message[i], g_strCommand[sensordata].c_str(), g_strCommand[sensordata].size());
	i += g_strCommand[sensordata].size();

	//add device id 2bytes
	//	client_message[i] = SEPARATE_CHAR;
	//	i++;
	//
	//	memcpy((uint8_t*) &client_message[i], (uint8_t*) g_strSensorType[SHORT_ADDRESS].c_str(), g_strSensorType[SHORT_ADDRESS].size());
	//	i += g_strSensorType[SHORT_ADDRESS].size();

	client_message[i] = SEPARATE_CHAR;
	i++;
	bytestoHexChars((uint8_t*) &outgoingMsg.short_addr, sizeof(outgoingMsg.short_addr), (uint8_t*) &client_message[i]);

	memcpy((uint8_t*) &topic_string[topic_len], (uint8_t*) &client_message[i], (sizeof(outgoingMsg.short_addr) * 2));
	topic_len = sizeof(outgoingMsg.short_addr) * 2;
	topic_string[topic_len] = '/';
	topic_len++;

	i += sizeof(outgoingMsg.short_addr) * 2;

	//	APP_DBG("add tokencommunicate, time , device id success!\n");
	//add sensor type
	client_message[i] = SEPARATE_CHAR;
	i++;
	switch (outgoingMsg.cluster_id) {
	case ZCL_CLUSTER_ID_SS_IAS_ZONE: {
		tsCLD_IASZone_StatusChangeNotificationPayload ss_ias_zone_value;
		if (outgoingMsg.dataLen == sizeof(tsCLD_IASZone_StatusChangeNotificationPayload)) {
			ss_ias_zone_value = *(tsCLD_IASZone_StatusChangeNotificationPayload*) &outgoingMsg.data[0];

			memcpy((uint8_t*) &topic_string[topic_len], (uint8_t*) &client_message[i], (g_strSensorType[SMOKING_ALARM].size()));
			topic_len += g_strSensorType[SMOKING_ALARM].size();
			memcpy((uint8_t*) &client_message[i], (uint8_t*) g_strSensorType[SMOKING_ALARM].c_str(), g_strSensorType[SMOKING_ALARM].size());
			i += g_strSensorType[SMOKING_ALARM].size();
			//add sensordata
			client_message[i] = SEPARATE_CHAR;
			i++;
			bytestoHexChars((uint8_t*) &ss_ias_zone_value.b16ZoneStatus, sizeof(ss_ias_zone_value.b16ZoneStatus), (uint8_t*) &client_message[i]);
			i += sizeof(ss_ias_zone_value.b16ZoneStatus) * 2;
		}

	}
		break;
	case ZCL_CLUSTER_ID_MS_RELATIVE_HUMIDITY: {	//humidity
		memcpy((uint8_t*) &client_message[i], (uint8_t*) g_strSensorType[RELATIVE_HUMIDITY].c_str(), g_strSensorType[RELATIVE_HUMIDITY].size());
		memcpy((uint8_t*) &topic_string[topic_len], (uint8_t*) &client_message[i], (g_strSensorType[RELATIVE_HUMIDITY].size()));
		topic_len += g_strSensorType[RELATIVE_HUMIDITY].size();
		i += g_strSensorType[RELATIVE_HUMIDITY].size();
		//add sensordata
		client_message[i] = SEPARATE_CHAR;
		i++;
		bytestoHexChars((uint8_t*) &outgoingMsg.data[0], outgoingMsg.dataLen, (uint8_t*) &client_message[i]);
		i += outgoingMsg.dataLen * 2;
	}
		break;
	case ZCL_CLUSTER_ID_MS_TEMPERATURE_MEASUREMENT: {	//temperature
		memcpy((uint8_t*) &client_message[i], g_strSensorType[TEMPERATURE_MEASUREMENT].c_str(), g_strSensorType[TEMPERATURE_MEASUREMENT].size());
		memcpy((uint8_t*) &topic_string[topic_len], (uint8_t*) &client_message[i], (g_strSensorType[TEMPERATURE_MEASUREMENT].size()));
		topic_len += g_strSensorType[TEMPERATURE_MEASUREMENT].size();
		i += g_strSensorType[TEMPERATURE_MEASUREMENT].size();
		//add sensordata
		client_message[i] = SEPARATE_CHAR;
		i++;
		bytestoHexChars((uint8_t*) &outgoingMsg.data[0], outgoingMsg.dataLen, (uint8_t*) &client_message[i]);
		i += outgoingMsg.dataLen * 2;
	}
		break;
	case ZCL_CLUSTER_ID_MS_OCCUPANCY_SENSING: {	//motion
		memcpy((uint8_t*) &client_message[i], g_strSensorType[OCCUPANCY_SENSING].c_str(), g_strSensorType[OCCUPANCY_SENSING].size());
		memcpy((uint8_t*) &topic_string[topic_len], (uint8_t*) &client_message[i], (g_strSensorType[OCCUPANCY_SENSING].size()));
		topic_len += g_strSensorType[OCCUPANCY_SENSING].size();
		i += g_strSensorType[OCCUPANCY_SENSING].size();
		//add sensordata
		client_message[i] = SEPARATE_CHAR;
		i++;
		bytestoHexChars((uint8_t*) &outgoingMsg.data[0], outgoingMsg.dataLen, (uint8_t*) &client_message[i]);
		i += outgoingMsg.dataLen * 2;
	}
		break;
	case ZCL_CLUSTER_ID_GEN_ON_OFF: {	//on-off door  F000
		//APP_PRINT("size = %s\n", g_strCommand[ON_OFF].c_str());
		if ((outgoingMsg.attrID == 0xF000)) {
			char data[10];
			snprintf(data, 10, "0x%X", outgoingMsg.cluster_id);
			memcpy((uint8_t*) &client_message[i], data, strlen((const char*) data));

			i += strlen((const char*) data);
			flag_unknown_cluster = 1;
		} else {
			memcpy((uint8_t*) &client_message[i], (uint8_t*) g_strSensorType[ON_OFF].c_str(), g_strSensorType[ON_OFF].size());
			memcpy((uint8_t*) &topic_string[topic_len], (uint8_t*) &client_message[i], (g_strSensorType[ON_OFF].size()));
			topic_len += g_strSensorType[ON_OFF].size();
			i += g_strSensorType[ON_OFF].size();
		}
		//add sensordata
		client_message[i] = SEPARATE_CHAR;
		i++;
		bytestoHexChars((uint8_t*) &outgoingMsg.data[0], outgoingMsg.dataLen, (uint8_t*) &client_message[i]);
		i += outgoingMsg.dataLen * 2;
	}
		break;
	case ZCL_CLUSTER_ID_GEN_ANALOG_INPUT_BASIC: {	//0x000C // load power of smart plug
		//APP_PRINT("size = %s\n", g_strCommand[ON_OFF].c_str());
		memcpy((uint8_t*) &client_message[i], (uint8_t*) g_strSensorType[LOAD_POWER].c_str(), g_strSensorType[LOAD_POWER].size());
		memcpy((uint8_t*) &topic_string[topic_len], (uint8_t*) &client_message[i], (g_strSensorType[LOAD_POWER].size()));
		topic_len += g_strSensorType[LOAD_POWER].size();
		i += g_strSensorType[LOAD_POWER].size();

		//add sensordata
		client_message[i] = SEPARATE_CHAR;
		i++;

		bytestoHexChars((uint8_t*) &outgoingMsg.data[0], outgoingMsg.dataLen, (uint8_t*) &client_message[i]);
		i += outgoingMsg.dataLen * 2;
	}
		break;
	case ZCL_CLUSTER_ID_GEN_BASIC: {	// data basic cluster id = 0x0000; // ZCL_CLUSTER_ID_GEN_BASIC

		memcpy((uint8_t*) &topic_string[topic_len], (uint8_t*) g_strSensorType[INFO].c_str(), (g_strSensorType[INFO].size()));
		topic_len += g_strSensorType[INFO].size();

		switch (outgoingMsg.attrID) {
		case ATTR_ID_NAME: {
			APP_PRINT("send message attr ID  = %04X\n", outgoingMsg.attrID);
			memcpy((uint8_t*) &client_message[i], (uint8_t*) g_strSensorType[SENSOR_NAME].c_str(), g_strSensorType[SENSOR_NAME].size());
			i += g_strSensorType[SENSOR_NAME].size();
			//add sensordata
			client_message[i] = SEPARATE_CHAR;
			i++;
			//bytestoHexChars((uint8_t*) &outgoingMsg.data[1], outgoingMsg.dataLen-1, (uint8_t*) &client_message[i]);
			memcpy((uint8_t*) &client_message[i], (uint8_t*) &outgoingMsg.data[1], outgoingMsg.dataLen - 1);
			i += (outgoingMsg.dataLen - 1) * 2;

		}
			break;
		case 0xFF01: {

			uint16_t len_temp = 0;
			uint16_t len_data;
			uint8_t *pData = &outgoingMsg.data[1]; // point to data of tring

			while (len_temp < outgoingMsg.dataLen - 1) {
				//pData++;
				len_temp++;
				//                APP_PRINT("PData type : %02X\n",*pData);
				switch (*pData++) {
				case 0x01: {
					len_data = zclGetAttrDataLength(*pData, pData, outgoingMsg.dataLen - 1);
					len_temp++; // move to data
					pData++; // pass type data

					memcpy((uint8_t*) &client_message[i], (uint8_t*) g_strSensorType[BATTERY_VOLTAGE].c_str(), g_strSensorType[BATTERY_VOLTAGE].size());
					i += g_strSensorType[BATTERY_VOLTAGE].size();

					//add separate char
					client_message[i] = SEPARATE_CHAR;
					i++;

					bytestoHexChars((uint8_t*) pData, len_data, (uint8_t*) &client_message[i]);
					i += len_data * 2;
					len_temp += len_data;
					pData += len_data;

					//add separate char
					client_message[i] = SEPARATE_CHAR;
					i++;

				}
					break;
#ifdef PRE_CURRENT_STATE
				case 0x04: { // previos _ current _state
					len_data = zclGetAttrDataLength(*pData, pData, outgoingMsg.dataLen - 1);
					len_temp++;// move to data
					pData++;// pass type data

					memcpy((uint8_t*) &client_message[i], (uint8_t*) g_strSensorType[PRE_STATUS].c_str(), g_strSensorType[PRE_STATUS].size());
					i += g_strSensorType[PRE_STATUS].size();

					//add separate char
					client_message[i] = SEPARATE_CHAR;
					i++;

					*pData = *(pData + 1) >> 4;
					bytestoHexChars((uint8_t*) pData, 1, (uint8_t*) &client_message[i]);
					i += 1 * 2;
					len_temp += 1;
					pData += 1;

					//add separate char
					client_message[i] = SEPARATE_CHAR;
					i++;

					memcpy((uint8_t*) &client_message[i], (uint8_t*) g_strSensorType[CURRENT_STATUS].c_str(), g_strSensorType[CURRENT_STATUS].size());
					i += g_strSensorType[CURRENT_STATUS].size();

					//add separate char
					client_message[i] = SEPARATE_CHAR;
					i++;

					*pData = *(pData) & 0x0f;
					bytestoHexChars((uint8_t*) pData, 1, (uint8_t*) &client_message[i]);
					i += 1 * 2;
					len_temp += 1;
					pData += 1;

					//add separate char
					client_message[i] = SEPARATE_CHAR;
					i++;

				}
					break;
#endif
				case 0x64: {
					len_data = zclGetAttrDataLength(*pData, pData, outgoingMsg.dataLen - 1);
					len_temp++; // move to data
					pData++; // pass type data

					memcpy((uint8_t*) &client_message[i], (uint8_t*) g_strSensorType[VALUE_1].c_str(), g_strSensorType[VALUE_1].size());
					i += g_strSensorType[VALUE_1].size();

					//add separate char
					client_message[i] = SEPARATE_CHAR;
					i++;

					bytestoHexChars((uint8_t*) pData, len_data, (uint8_t*) &client_message[i]);
					i += len_data * 2;
					len_temp += len_data;
					pData += len_data;

					//add separate char
					client_message[i] = SEPARATE_CHAR;
					i++;
				}
					break;
				case 0x65: {
					len_data = zclGetAttrDataLength(*pData, pData, outgoingMsg.dataLen - 1);
					len_temp++; // move to data
					pData++; // pass type data

					memcpy((uint8_t*) &client_message[i], (uint8_t*) g_strSensorType[VALUE_2].c_str(), g_strSensorType[VALUE_2].size());
					i += g_strSensorType[VALUE_2].size();

					//add separate char
					client_message[i] = SEPARATE_CHAR;
					i++;

					bytestoHexChars((uint8_t*) pData, len_data, (uint8_t*) &client_message[i]);
					i += len_data * 2;
					len_temp += len_data;
					pData += len_data;

					//add separate char
					client_message[i] = SEPARATE_CHAR;
					i++;
				}
					break;
				case 0x96: {

					len_data = zclGetAttrDataLength(*pData, pData, outgoingMsg.dataLen - 1);
					len_temp++; // move to data
					pData++; // pass type data

					memcpy((uint8_t*) &client_message[i], (uint8_t*) g_strSensorType[LOAD_VOLTAGE].c_str(), g_strSensorType[LOAD_VOLTAGE].size());
					i += g_strSensorType[LOAD_VOLTAGE].size();

					//add separate char
					client_message[i] = SEPARATE_CHAR;
					i++;

					bytestoHexChars((uint8_t*) pData, len_data, (uint8_t*) &client_message[i]);
					i += len_data * 2;
					len_temp += len_data;
					pData += len_data;

					//add separate char
					client_message[i] = SEPARATE_CHAR;
					i++;
				}
					break;
				case 0x98: {
					len_data = zclGetAttrDataLength(*pData, pData, outgoingMsg.dataLen - 1);
					len_temp++; // move to data
					pData++; // pass type data

					memcpy((uint8_t*) &client_message[i], (uint8_t*) g_strSensorType[LOAD_POWER].c_str(), g_strSensorType[LOAD_POWER].size());
					i += g_strSensorType[LOAD_POWER].size();

					//add separate char
					client_message[i] = SEPARATE_CHAR;
					i++;

					bytestoHexChars((uint8_t*) pData, len_data, (uint8_t*) &client_message[i]);
					i += len_data * 2;
					len_temp += len_data;
					pData += len_data;

					//add separate char
					client_message[i] = SEPARATE_CHAR;
					i++;
				}
					break;
				default: {
					len_data = zclGetAttrDataLength(*pData, pData, outgoingMsg.dataLen - 1);
					len_temp++; // move to data
					pData++; // pass type data

					len_temp += len_data;
					pData += len_data; // move to next data
				}
					break;
				}
			}
			i--;
		}
			break;
		case 0xFF02: {

			uint16_t len_data;
			uint8_t *pData = &outgoingMsg.data[0]; // point to data of tring
			uint16_t num_attr = BUILD_UINT16(pData[0], pData[1]);
			uint16_t j;

			pData += 2; // move to data

			//			APP_PRINT("number attribute : %04X", num_attr);
			for (j = 0; j < num_attr; j++) {
				switch (j) {
				case 0: { // value 1
					memcpy((uint8_t*) &client_message[i], (uint8_t*) g_strSensorType[VALUE_1].c_str(), g_strSensorType[VALUE_1].size());
					i += g_strSensorType[VALUE_1].size();

					//add sensordata
					client_message[i] = SEPARATE_CHAR;
					i++;

					len_data = zclGetAttrDataLength(*pData, pData, outgoingMsg.dataLen);
					pData++; // move pass type data

					bytestoHexChars((uint8_t*) pData, len_data, (uint8_t*) &client_message[i]);
					i += len_data * 2;
					pData += len_data;

					//add sensordata
					client_message[i] = SEPARATE_CHAR;
					i++;
				}
					break;
				case 1: {  //  battery
					memcpy((uint8_t*) &client_message[i], (uint8_t*) g_strSensorType[BATTERY_VOLTAGE].c_str(), g_strSensorType[BATTERY_VOLTAGE].size());
					i += g_strSensorType[BATTERY_VOLTAGE].size();

					//add sensordata
					client_message[i] = SEPARATE_CHAR;
					i++;

					len_data = zclGetAttrDataLength(*pData, pData, outgoingMsg.dataLen);
					pData++; // move pass type data

					bytestoHexChars((uint8_t*) pData, len_data, (uint8_t*) &client_message[i]);
					i += len_data * 2;
					pData += len_data;

					//add sensordata
					client_message[i] = SEPARATE_CHAR;
					i++;
				}
					break;
#ifdef PRE_CURRENT_STATE
				case 2: {  //  state
					memcpy((uint8_t*) &client_message[i], (uint8_t*) g_strSensorType[PRE_STATUS].c_str(), g_strSensorType[PRE_STATUS].size());
					i += g_strSensorType[PRE_STATUS].size();

					//add sensordata
					client_message[i] = SEPARATE_CHAR;
					i++;

					len_data = zclGetAttrDataLength(*pData, pData, outgoingMsg.dataLen);
					pData++;// move pass type data

					*pData = *(pData + 1) >> 4;
					bytestoHexChars((uint8_t*) pData, 1, (uint8_t*) &client_message[i]);
					i += 1 * 2;
					pData += 1;

					//add sensordata
					client_message[i] = SEPARATE_CHAR;
					i++;

					memcpy((uint8_t*) &client_message[i], (uint8_t*) g_strSensorType[CURRENT_STATUS].c_str(), g_strSensorType[CURRENT_STATUS].size());
					i += g_strSensorType[CURRENT_STATUS].size();

					//add sensordata
					client_message[i] = SEPARATE_CHAR;
					i++;

					*pData = *(pData) & 0x0f;
					bytestoHexChars((uint8_t*) pData, 1, (uint8_t*) &client_message[i]);
					i += 1 * 2;
					pData += 1;

					//add sensordata
					client_message[i] = SEPARATE_CHAR;
					i++;
				}
					break;
#endif
				default: {

					len_data = zclGetAttrDataLength(*pData, pData, outgoingMsg.dataLen);
					pData++;

					pData += len_data; // move next data

				}
					break;
				}
			}
			i--;
		}
			break;
		default:
			//APP_DBG("unknown data\n");
			char data[10];
			snprintf(data, 10, "0x%X", outgoingMsg.cluster_id);
			memcpy((uint8_t*) &client_message[i], data, strlen((const char*) data));

			i += strlen((const char*) data);
			flag_unknown_cluster = 1;
			break;
		}

	}
		break;
	default:
		//		APP_DBG("unknown data\n");
		char data[10];
		snprintf(data, 10, "0x%X", outgoingMsg.cluster_id);
		memcpy((uint8_t*) &client_message[i], data, strlen((const char*) data));

		i += strlen((const char*) data);
		flag_unknown_cluster = 1;
		break;
	}

	//add sensordata
	client_message[i] = SEPARATE_CHAR;
	i++;
	//end message
	client_message[i] = '\0';
	i++;
	{
		// send to task scene
		uint8_t *data_scene = (uint8_t*) client_message;
		ak_msg_t* s_msg = get_dynamic_msg();
		set_msg_sig(s_msg, GET_SCENE);
		set_data_dynamic_msg(s_msg, data_scene, i);
		set_msg_src_task_id(s_msg, ID_TASK_HANDLE_MSG);
		task_post(ID_TASK_SCENE, s_msg);
	}
	if (flag_unknown_cluster == 0) {

		APP_PRINT("\n sensor data = %s\n", client_message);
#ifdef AES_ENCRYPT_FLAG
		setKey((unsigned char*) g_config_parameters.security_key, 32);
		int postmessagelen = iot_final_encrypt(client_message, i);
		if (postmessagelen <= 0) {
			return;
		}
		i = postmessagelen;
#endif
		memcpy(lastSensordataMessage, client_message, i);
		{
			/*Prepare and signal next task*/
			uint8_t *data = (uint8_t*) client_message;
			ak_msg_t* s_msg = get_dynamic_msg();
			set_msg_sig(s_msg, IF_SOCKET_SENSOR_DATA);
			set_data_dynamic_msg(s_msg, data, i);

			set_msg_src_task_id(s_msg, ID_TASK_HANDLE_MSG);
			task_post(ID_TASK_IF_SOCKET, s_msg);
		}

		{
			/*Prepare and signal next task*/
			topic_string[topic_len] = '/';
			topic_len++;
			topic_string[topic_len] = '\0';
			topic_len++;
			message_len = i;
			client_message[i] = topic_len;
			i++;
			memcpy((uint8_t*) &client_message[i], (uint8_t*) topic_string, topic_len);
			i += topic_len;
			memcpy((uint8_t*) &client_message[i], (uint8_t*) &message_len, sizeof(message_len));
			i += sizeof(message_len);
			uint8_t *data = (uint8_t*) client_message;
			ak_msg_t* s_msg = get_dynamic_msg();
			set_msg_sig(s_msg, MQTT_SENSOR_DATA);
			set_data_dynamic_msg(s_msg, data, i);

			set_msg_src_task_id(s_msg, ID_TASK_HANDLE_MSG);
			task_post(ID_TASK_MQTT, s_msg);
		}

		{
			/*Prepare and signal next task*/
			uint8_t *data = (uint8_t*) client_message;
			ak_msg_t* s_msg = get_dynamic_msg();
			set_msg_sig(s_msg, LOG_SENSOR_DATA);
			set_data_dynamic_msg(s_msg, data, i);

			set_msg_src_task_id(s_msg, ID_TASK_HANDLE_MSG);
			task_post(ID_TASK_LOG, s_msg);
		}
	} else {
		//		APP_PRINT("\n unknown data = %s\n", client_message);
	}
	free(client_message);
}

int8_t processingDataCommunication(uint8_t *message, int32_t len, uint32_t src_task_id, int32_t *outCmd) {

#ifdef AES_ENCRYPT_FLAG
	setKey((unsigned char*) g_config_parameters.security_key, 32);
	len = iot_final_decrypt(message, (unsigned char *) message, len);
#endif
	uint8_t*client_message = message;
	APP_PRINT("\n");
	for (int j = 0; j < len; j++) {
		APP_PRINT("%c", message[j]);
	}
	APP_PRINT("\n");
	parse_communicate_string_t parseMessage = parseClientString(client_message, len);

	if (parseMessage.totalString < 3) {
		return FALSE;
	}
	int32_t i = 0;
	// check token key communicate
	if ((isTokenCommunicate((uint8_t*) &client_message[parseMessage.position[i]], (parseMessage.position[i + 1] - parseMessage.position[i])) != TRUE)) {
		return FALSE;
	}
	i++;

	//check time
	if ((isTime((uint8_t*) &client_message[parseMessage.position[i]], (parseMessage.position[i + 1] - parseMessage.position[i])) != TRUE)) {
		return FALSE;
	}
	i++;

	//get command
	int8_t cmd = getCommand((uint8_t*) &client_message[parseMessage.position[i]], (parseMessage.position[i + 1] - parseMessage.position[i]));
	*outCmd = cmd;
	i++;
	switch (cmd) {
	case adddevice: {
		//check server online
		if (g_u8ServerStatus == OFFLINE) {
			APP_PRINT("server offline");
			i--;
			setErrCommand((uint8_t*) &client_message[parseMessage.position[i]], error_adddevice);
			//			write(sock2, client_message, strlen((const char*) client_message));
			return FALSE;
		}
		{
			uint8_t timeout = TIME_ENABLE_PERMIT_JOIN;
			ak_msg_t* s_msg = get_dynamic_msg();
			set_msg_sig(s_msg, ZB_PERMIT_JOINING_REQUEST);
			set_data_dynamic_msg(s_msg, (uint8_t*) &timeout, 1);
			set_msg_src_task_id(s_msg, src_task_id);
			task_post(ID_TASK_IF_ZNP, s_msg);
		}
	}
		break;
	case removedevice: {

		zdo_mgmt_leave_req_t zdoMgmtLeaveReq;

		hexCharsToBytes((uint8_t*) &client_message[parseMessage.position[i]], parseMessage.len[i], (uint8_t*) &zdoMgmtLeaveReq.short_address);
		i++;
		hexCharsToBytes((uint8_t*) &client_message[parseMessage.position[i]], parseMessage.len[i], (uint8_t*) &zdoMgmtLeaveReq.device_address[0]);
		i++;
		zdoMgmtLeaveReq.flags = 0;
		//		hexCharsToBytes((uint8_t*) &client_message[parseMessage.position[i]], (parseMessage.position[i + 1] - parseMessage.position[i]), (uint8_t*) &zdoMgmtLeaveReq.flags);
		//		i++;
		{
			ak_msg_t* s_msg = get_dynamic_msg();
			set_msg_sig(s_msg, ZDO_MGMT_LEAVE_REQ);
			set_data_dynamic_msg(s_msg, (uint8_t*) &zdoMgmtLeaveReq, sizeof(zdo_mgmt_leave_req_t));
			set_msg_src_task_id(s_msg, src_task_id);
			task_post(ID_TASK_IF_ZNP, s_msg);
		}
		{
			uint8_t *data = (uint8_t*) client_message;
			ak_msg_t* s_msg = get_dynamic_msg();
			set_msg_sig(s_msg, LOG_SENSOR_DATA);
			set_data_dynamic_msg(s_msg, data, len);

			set_msg_src_task_id(s_msg, src_task_id);
			task_post(ID_TASK_LOG, s_msg);
		}
	}
		break;
	case sensordata: {
		//		write(sock2, lastSensordataMessage, strlen((const char*) lastSensordataMessage));
	}
		break;
	case controldevice: {
		if (parseMessage.totalString < 5) {
			return FALSE;
		}
		af_data_request_t afDataRequest;
		int32_t len = ((parseMessage.len[i + 1] + parseMessage.len[i]) / 2) + sizeof(afDataRequest.data);
		uint8_t *data = (uint8_t*) malloc(len);
		uint8_t j = 0;
		hexCharsToBytes((uint8_t*) &client_message[parseMessage.position[i]], (parseMessage.len[i]), (uint8_t*) &data[j]);
		j = (parseMessage.len[i]) / 2;
		i++;
		hexCharsToBytes((uint8_t*) &client_message[parseMessage.position[i]], (parseMessage.len[i]), (uint8_t*) &data[j]);
		i++;

		afDataRequest = *(af_data_request_t*) data;
		APP_PRINT("afDataRequest.len = %d, len = %d, %d, %d", afDataRequest.len, len, sizeof(afDataRequest), sizeof(afDataRequest.data));
		if (afDataRequest.len != (len - sizeof(afDataRequest))) {
			free(data);
			return FALSE;
		}
		uint8_t *data2 = (uint8_t*) malloc(afDataRequest.len);
		memcpy(data2, (uint8_t*) &data[sizeof(afDataRequest) - sizeof(afDataRequest.data)], afDataRequest.len);
		memcpy((uint8_t*) &data[sizeof(afDataRequest)], data2, afDataRequest.len);

		ak_msg_t* s_msg = get_dynamic_msg();
		set_msg_sig(s_msg, AF_DATA_REQUEST);
		set_data_dynamic_msg(s_msg, data, len);
		set_msg_src_task_id(s_msg, src_task_id);
		task_post(ID_TASK_IF_ZNP, s_msg);

		free(data);
		free(data2);
	}
		break;
	case canceladddevice: {
		//check server online
		if (g_u8ServerStatus == OFFLINE) {
			APP_PRINT("server offline");
			i--;
			setErrCommand((uint8_t*) &client_message[parseMessage.position[i]], error_adddevice);
			//			write(sock2, client_message, strlen((const char*) client_message));
			return FALSE;
		}
		uint8_t timeout = 0;
		ak_msg_t* s_msg = get_dynamic_msg();
		set_msg_sig(s_msg, ZB_PERMIT_JOINING_REQUEST);
		set_data_dynamic_msg(s_msg, (uint8_t*) &timeout, 1);
		set_msg_src_task_id(s_msg, src_task_id);
		task_post(ID_TASK_IF_ZNP, s_msg);
	}
		break;

	case updatescene: {
		//        if (parseMessage.totalString < 9) {
		//            return FALSE;
		//        }

		int32_t len = (parseMessage.position[parseMessage.totalString] - parseMessage.position[i] + 1);
		uint8_t *data = (uint8_t*) malloc(len);

		memcpy(data, (uint8_t*) &client_message[parseMessage.position[i]], len);
		i++;

		ak_msg_t* s_msg = get_dynamic_msg();
		set_msg_sig(s_msg, UPDATE_SCENE);
		set_data_dynamic_msg(s_msg, data, len);
		set_msg_src_task_id(s_msg, src_task_id);
		task_post(ID_TASK_SCENE, s_msg);

		free(data);
		//		free(client_message);
	}
		break;
	case resetgateway: {
		/*Delete files*/
		resetDelete();
		exit(0);
	}
		break;
	default:
		APP_PRINT("command not exists!\n");
		return FALSE;
	}

	return TRUE;
}

int8_t isTokenCommunicate(uint8_t *client_message, int32_t len) {

	int32_t token_len = g_strTokenCommunicate.size();
	if (len < token_len) {
		return FALSE;
	}
	int32_t i = 0;
	while (i < token_len) {
		APP_PRINT("%c", client_message[i]);
		if (client_message[i] == g_strTokenCommunicate[i]) {
			i++;
		} else {
			break;
		}
	}
	APP_PRINT("\n");
	if (i == token_len) {
		return TRUE;
	}
	return FALSE;
}

int8_t isTime(uint8_t *client_message, int32_t len) {

	time_t rawtime = time(NULL);
	time_t timeStamp, timeStamp2;

	uint8_t *data_time = (uint8_t*) &timeStamp;
	uint8_t *data_time2 = (uint8_t*) &timeStamp2;

	hexCharsToBytes(client_message, len, (uint8_t*) &timeStamp);
	for (unsigned int i = 0; i < sizeof(time_t); i++) {
		data_time2[sizeof(time_t) - i - 1] = data_time[i];
	}
	//	free(strTime);
	if (difftime(rawtime, timeStamp2) > DIFF_TIME) {
		return FALSE;
	}
	return TRUE;
}

int8_t getCommand(uint8_t *client_message, int32_t len) {
	//	APP_PRINT("len =%d", len);
	if (len > 4) {
		len = 4;
	}
	for (int8_t i = 0; i < commandlength; i++) {
		if (strncmp((const char*) g_strCommand[i].c_str(), (const char*) client_message, len) == 0) {
			return i;
		}
	}
	return commandlength;
}

/* set error command message */
void setErrCommand(uint8_t*client_message, int8_t posStrError) {
	if (posStrError < errorcommandlength) {
		memcpy(client_message, (uint8_t*) g_strCommandError[posStrError].c_str(), g_strCommandError[posStrError].size());
	}
}

/*Delete when reset*/
void resetDelete() {
	/*Delete config file*/
	try {
		string config_folder_path = static_cast<string>(APP_ROOT_PATH) + static_cast<string>("/config/gateway_config.txt");
		if (remove(config_folder_path.c_str()) != 0)
			perror("[Reset]Error deleting config file");
		else {
			APP_PRINT("[Reset]We have remove: %s\n", config_folder_path.c_str());
		}
	} catch (exception e) {
		printf("Error deleting file: %s", e.what());
	}
	/*Delete scene file*/
	try {
		string config_scene_path = static_cast<string>(APP_ROOT_PATH) + static_cast<string>("/scene/scene.txt");
		if (remove(config_scene_path.c_str()) != 0)
			perror("[Reset]Error deleting scene file");
		else {
			APP_PRINT("[Reset]We have remove: %s\n", config_scene_path.c_str());
		}
	} catch (exception e) {
		printf("Error deleting file: %s", e.what());
	}
	/*Delete log file*/
	try {
		string log_folder_path;
		log_folder_path = static_cast<string>(APP_ROOT_PATH) + static_cast<string>("/log/");
		DIR *log_folder = opendir(log_folder_path.c_str());
		struct dirent *next_file;
		while ((next_file = readdir(log_folder)) != NULL) {
			if (next_file->d_type == DT_REG) { /* If the entry is a regular file */
				string filename = log_folder_path + next_file->d_name;
				if (remove(filename.c_str()) != 0)
					perror("[Reset]Error deleting file");
				else {
					printf("[Reset]We have remove: %s\n", next_file->d_name);
				}
			}
		}
		closedir(log_folder);
	} catch (exception e) {
		printf("Error deleting file: %s", e.what());
	}
	/*write (1) config start_up_success*/
	memset((void*) &g_config_parameters, 0, sizeof(config_parameter_t));
	g_config_parameters.start_up_success = 1;
	APP_PRINT("write config start_up_success : %d\n", g_config_parameters.start_up_success);
	gateway_configure.initializer((char*) "/gateway_config.txt");
	string aes_key = string((const char*) s_ieee_address) + string((const char*) s_ieee_address);
	gateway_configure.write_config_data(&g_config_parameters, (uint8_t*) aes_key.c_str());
}
