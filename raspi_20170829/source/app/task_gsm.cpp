#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <semaphore.h>

#include "../ak/ak.h"
#include "../ak/timer.h"

#include "../sys/sys_dbg.h"

#include "app.h"
#include "app_dbg.h"
#include "app_config.h"

#include "task_list.h"
#include "task_gsm.h"

#define MODULE_800L

//#define GSM_DEVPATH					"/dev/ttyUSB0"
#define GSM_DEVPATH					"/dev/ttyS2"
#define BUF_SIZE					(1023)

struct {
	uint32_t	index;
	uint8_t		buffer[BUF_SIZE];
} uart_buf_mng;

q_msg_t gw_task_gsm_mailbox;

static int gsm_serial_fd;
static int gsm_serial_opentty(const char* devpath);

static uint64_t epoch_milli ;

char mobile_number[20]	="+841648645649";

static void		initialise_epoch (void);
static uint32_t	millis();
static void		wait_for_sim_power_on(void);

static int		sim_puts(char*, uint32_t);
static int		sim_putc(char);
static int		send_wait_until(const char*, const char*, uint32_t);

static pthread_t gsm_serial_rx_thread;
static void* gsm_serial_rx_thread_handler(void*);

void* gw_task_gsm_entry(void*) {
	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("[STARTED] gw_task_gsm_entry\n");

	wait_for_sim_power_on();

	if (gsm_serial_opentty(GSM_DEVPATH) < 0) {
		APP_DBG("[GSM MQTT] Cannot open %s !\n", GSM_DEVPATH);
	}
	else {
		APP_DBG("[GSM MQTT] Opened %s success !\n", GSM_DEVPATH);

		pthread_create(&gsm_serial_rx_thread, NULL, gsm_serial_rx_thread_handler, NULL);

		usleep(1000000);
		sim_puts((char*)"+++", strlen("+++"));
		usleep(1000000);

		while(send_wait_until("\r\nATE0\r\n", "OK", 2000) != 0) {
			//polling 3s
			usleep(3000000);
		}
		while(send_wait_until("\r\nAT+GSMBUSY=1\r\n", "OK", 2000) != 0) {
			//polling 3s
			usleep(3000000);
		}
		while( send_wait_until("\r\nAT+CREG?\r\n", "0,1", 3000) != 0) {
			//polling 3s
			usleep(3000000);
		}
	}

	initialise_epoch();

	while (1) {
		while (msg_available(ID_TASK_GSM)) {
			/* get message */
			ak_msg_t* msg = rev_msg(ID_TASK_GSM);

			switch (msg->header->sig) {
			case GSM_SEND_SMS: {
				APP_DBG("GSM_SEND_SMS\n");

				int len = get_data_len_dynamic_msg(msg);
				uint8_t* sms = (uint8_t*) malloc(len);
				get_data_dynamic_msg(msg, sms, len);
				if(sms != NULL) {
					if(send_wait_until("\r\nAT+CMGF=1\r\n", "OK", 2000) == 0 ) {
						string cmd = string("\r\nAT+CMGS=\"") + string(mobile_number) + string("\"\r\n");
						if( send_wait_until(cmd.data(), ">", 1000) == 0 ) {
							//Send SMS content
							sim_puts((char*)sms, len);
							usleep(100000);
							//Send Ctrl+Z / ESC to denote SMS message is complete
							sim_putc((char)26);
							usleep(100000);
						}
					}
					free(sms);
				}
			}
				break;

			default:
				break;
			}

			/* free message */
			free_msg(msg);
		}

		usleep(100);
	}

	return (void*)0;

}

int gsm_serial_opentty(const char* devpath) {
	struct termios options;
	SYS_DBG("[GSM SERIAL][gsm_serial_opentty] devpath: %s\n", devpath);

	gsm_serial_fd = open(devpath, O_RDWR | O_NOCTTY | O_NDELAY);
	if (gsm_serial_fd < 0) {
		return gsm_serial_fd;
	}
	else {
		fcntl(gsm_serial_fd, F_SETFL, 0);

		/* get current status */
		tcgetattr(gsm_serial_fd, &options);

		cfsetispeed(&options, B9600);
		cfsetospeed(&options, B9600);

		/* No parity (8N1) */
		options.c_cflag &= ~PARENB;
		options.c_cflag &= ~CSTOPB;
		options.c_cflag &= ~CSIZE;
		options.c_cflag |= CS8;

		options.c_cflag |= (CLOCAL | CREAD);
		options.c_cflag     &=  ~CRTSCTS;

		cfmakeraw(&options);

		tcflush(gsm_serial_fd, TCIFLUSH);
		if (tcsetattr (gsm_serial_fd, TCSANOW, &options) != 0) {
			SYS_DBG("error in tcsetattr()\n");
		}
	}
	return 0;
}

void* gsm_serial_rx_thread_handler(void*) {
	APP_DBG("gsm_serial_rx_thread_handler entry successed!\n");

	uart_buf_mng.index = 0;
	uart_buf_mng.buffer[0] = 0;

	uint8_t inChar;
	while(1) {
		if(read(gsm_serial_fd, &inChar, 1) > 0) {
			printf("%c", inChar);
			if(uart_buf_mng.index > BUF_SIZE) {
				uart_buf_mng.index = 0;
				uart_buf_mng.buffer[0] = 0;
			} else {
				uart_buf_mng.buffer[uart_buf_mng.index++] = inChar;
				uart_buf_mng.buffer[uart_buf_mng.index] = 0;
			}
		}
		else{
			APP_DBG("Error from read tty\n");
			close(gsm_serial_fd);
			FATAL("GSM", 0x01);
		}
		usleep(300);
	}

	return (void*)0;
}

int sim_puts(char * s, uint32_t len) {
	return write(gsm_serial_fd, s, len);
}

int sim_putc(char c) {
	return write(gsm_serial_fd, &c, 1);
}

void initialise_epoch () {
	struct timeval tv ;
	gettimeofday (&tv, NULL) ;
	epoch_milli = (uint64_t)tv.tv_sec * (uint64_t)1000 + (uint64_t)(tv.tv_usec / 1000) ;
}

uint32_t millis() {
	uint64_t now ;
	struct timeval tv ;
	gettimeofday (&tv, NULL) ;
	now  = (uint64_t)tv.tv_sec * (uint64_t)1000 + (uint64_t)(tv.tv_usec / 1000) ;
	return (uint32_t)(now - epoch_milli) ;
}

int send_wait_until(const char* cmd, const char* exp, uint32_t timeout) {

	uart_buf_mng.index = 0;
	uart_buf_mng.buffer[0] = 0;

	if (cmd != NULL) {
		sim_puts((char*)cmd, strlen(cmd));
	}
	uint32_t prev_millis = millis();

	while ( millis() - prev_millis < timeout ){
		if(strstr( (const char*)uart_buf_mng.buffer, exp)){
			return 0;
		}
		usleep(1000);
	}
	return -1;
}

void wait_for_sim_power_on(void) {
#if defined(MODULE_800L)
	//Reset pin 800L
	system("echo 3  > /sys/class/gpio/unexport");
	system("echo 3  > /sys/class/gpio/export");
	system("echo 0 > /sys/class/gpio/gpio3/active_low");
	//Reset sim 800L
	system("echo 0 > /sys/class/gpio/gpio3/value");
	system("echo out > /sys/class/gpio/gpio3/direction");
	usleep(500000);
	system("echo 1 > /sys/class/gpio/gpio3/value");
	return;
#endif

#if defined(MODULE_800C)
	FILE* fd = NULL;
	char buff[2];
	uint32_t status = 0;

	//Status pin 800C
	system("echo 65 > /sys/class/gpio/unexport");
	system("echo 65 > /sys/class/gpio/export");
	//Pwkey pin 800C
	system("echo 66 > /sys/class/gpio/unexport");
	system("echo 66 > /sys/class/gpio/export");

	system("echo 0 > /sys/class/gpio/gpio65/active_low");
	system("echo 0 > /sys/class/gpio/gpio66/active_low");

	//Triger pwkpin sim 800C
	system("echo 1 > /sys/class/gpio/gpio66/value");
	system("echo out > /sys/class/gpio/gpio66/direction");
	//usleep(1000000);
	//system("echo 0 > /sys/class/gpio/gpio66/value");

	//Status pin is INPUT
	system("echo in > /sys/class/gpio/gpio65/direction");

	while(status != 49) {
		if((fd = fopen("/sys/class/gpio/gpio65/value", "r"))<0) {
			APP_DBG("[GSM MQTT] open /gpio65/value\n");
			return ;
		}

		fscanf(fd, "%s", buff);
		usleep(500000);
		APP_DBG("[GSM MQTT] POWER SIM IS OFF\n");
		status = (uint32_t)buff[0];
	}
	APP_DBG("[GSM MQTT] POWER SIM IS ON\n");
#endif
}

