#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include "time.h"
#include <errno.h>

#include <sys/select.h>
#include <termios.h>

#include "znp_serial.h"
#include "../interfaces/if_znp.h"
#include "global_parameters.h"
// For options field of afSendData()
#define AF_MAC_ACK              		0x00		//Require Acknowledgement from next device on route
#define DEFAULT_RADIUS                  0x0F    //Maximum number of hops to get to destination

#define MAX_LEN_ZNP                  	150    //Maximum length znp
uint8_t sequenceNumber = 0;
uint8_t pre_zigbee_key[16] = { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1' };

uint8_t znpBuf[MAX_LEN_ZNP];

uint8_t setTCRequireKeyExchange(int fd, uint8_t bdb_TrustCenterRequireKeyExchange);

uint8_t waitingForMessage(int fd, uint16_t cmd);

uint8_t waitingForStatus(int fd, uint16_t cmd, uint8_t ustatus);

uint8_t getMsgReturn(int fd, uint16_t cmd, uint8_t status, uint8_t* rx_buffer, int32_t* len);

uint8_t calcFCS(uint8_t *pMsg, int8_t len) {
	uint8_t result = 0;
	while (len--) {
		result ^= *pMsg++;
	}
	return result;
}

uint8_t app_cnf_set_allowrejoin_tc_policy(int fd, uint8_t mode) {
	int8_t i = 0;
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(APP_CNF_SET_ALLOWREJOIN_TC_POLICY);
	i++;
	znpBuf[i] = LSB(APP_CNF_SET_ALLOWREJOIN_TC_POLICY);
	i++;
	znpBuf[i] = mode;
	i++;
	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;
	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}

	return waitingForMessage(fd, (APP_CNF_SET_ALLOWREJOIN_TC_POLICY | 0x6000));

}

uint8_t app_cnf_bdb_start_commissioning(int fd, uint8_t mode_config, uint8_t mode_receiving, uint8_t flagWaiting) {
	int8_t i = 0;
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(APP_CNF_BDB_START_COMMISSIONING);
	i++;
	znpBuf[i] = LSB(APP_CNF_BDB_START_COMMISSIONING);
	i++;
	znpBuf[i] = mode_config;
	i++;
	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;
	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}

	if (flagWaiting == 0) {
		return waitingForStatus(fd, APP_CNF_BDB_COMMISSIONING_NOTIFICATION, ZNP_SUCCESS);
	} else {
		return ZNP_SUCCESS;
	}
}

void print_receiving_bytes(int iNum, unsigned char *buf2) {
	int i1;
	APP_PRINT(" %d - ", iNum);

	for (i1 = 0; i1 < iNum; i1++) {
		APP_PRINT("0x%02x ", buf2[i1] & 0xFF);
	}
	APP_PRINT("\n");
}

#define RX_BUFFER_SIZE		256
uint8_t zbReadConfiguration(int fd, uint8_t config_id, uint8_t* rx_buffer, int32_t* len) {
	int8_t i = 0;
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(ZB_READ_CONFIGURATION);
	i++;
	znpBuf[i] = LSB(ZB_READ_CONFIGURATION);
	i++;
	znpBuf[i] = config_id;
	i++;
	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;
	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}

	return getMsgReturn(fd, ZB_READ_CONFIGURATION_RSP, ZNP_SUCCESS, rx_buffer, len);
}

uint8_t getMsgReturn(int fd, uint16_t cmd, uint8_t status, uint8_t* rx_buffer, int32_t* len) {
	int32_t rx_read_len, i;
	i = 10;
	while (i > 0) {
		*len = read(fd, rx_buffer, RX_BUFFER_SIZE);
		if (*len > 0) {
			int j = 0;
			for (j = 0; j < *len; j++) {
				APP_PRINT(" %02X", rx_buffer[j]);
			}
			APP_PRINT("\n");

			j = 0;
			while (j < *len) {
				if (rx_buffer[j] == ZNP_SOF) {
					if ( CONVERT_TO_INT(rx_buffer[j + 3], rx_buffer[j + 2]) == cmd) {
						rx_buffer = (rx_buffer + j);
						if (status == rx_buffer[j + 4]) {
							return ZNP_SUCCESS;
						} else {
							return ZNP_NOT_SUCCESS;
						}
					}
				}
				j++;
			}

		} else {

			APP_PRINT("Error from read: %d: %s\n", rx_read_len, strerror(errno));
			break;
		}
		i--;
//			usleep(NORMAL_CMD_WAIT);
		APP_PRINT("i = %d \n", i);
	}

	APP_PRINT("ZNP_NOT_SUCCESS\n");
	return ZNP_NOT_SUCCESS;
}

uint8_t waitingForMessage(int fd, uint16_t cmd) {
	uint8_t rx_buffer[RX_BUFFER_SIZE];
	int32_t rx_read_len, i;
//	APP_PRINT("waiting message SPSP\n");
//	usleep(NORMAL_CMD_WAIT);
	i = 100;
	while (i > 0) {
		rx_read_len = read(fd, rx_buffer, RX_BUFFER_SIZE);
		if (rx_read_len > 0) {
			int j = 0;
			for (j = 0; j < rx_read_len; j++) {
				APP_PRINT(" %02X", rx_buffer[j]);
			}
			APP_PRINT("\n");

			j = 0;
			while (j < rx_read_len) {
				if (rx_buffer[j] == ZNP_SOF) {
					if ( CONVERT_TO_INT(rx_buffer[j + 3], rx_buffer[j + 2]) == cmd) {
						return rx_buffer[j + 4];
					}
				}
				j++;
			}

		} else {

			APP_PRINT("Error from read: %d: %s\n", rx_read_len, strerror(errno));
			break;
		}
		i--;
//			usleep(NORMAL_CMD_WAIT);
		APP_PRINT("i = %d \n", i);
	}

	APP_PRINT("ZNP_NOT_SUCCESS\n");
	return ZNP_NOT_SUCCESS;
}

uint8_t waitingForStatus(int fd, uint16_t cmd, uint8_t ustatus) {
	uint8_t rx_buffer[RX_BUFFER_SIZE];
	int32_t rx_read_len, i;
//	APP_PRINT("waiting message SPSP\n");
//	usleep(NORMAL_CMD_WAIT);
	i = 100;
	while (i > 0) {
		rx_read_len = read(fd, rx_buffer, RX_BUFFER_SIZE);

		if (rx_read_len > 0) {
			int j = 0;
			for (j = 0; j < rx_read_len; j++) {
				APP_PRINT(" %02X", rx_buffer[j]);
			}
			APP_PRINT("\n");

			j = 0;
			while (j < rx_read_len) {
				if (rx_buffer[j] == ZNP_SOF) {
					if ( CONVERT_TO_INT(rx_buffer[j + 3], rx_buffer[j + 2]) == cmd && rx_buffer[j + 4] == ustatus) {
						return rx_buffer[j + 4];
					}
				}
				j++;
			}

		} else {
			APP_PRINT("Error from read: %d: %s\n", rx_read_len, strerror(errno));
			break;
		}
		i--;
//		usleep(NORMAL_CMD_WAIT);
		APP_PRINT("i = %d \n", i);
	}

	APP_PRINT("ZNP_NOT_SUCCESS\n");
	return ZNP_NOT_SUCCESS;
}

/*
 * znpSoftReset*/
#define HARD_RESET				0x00
#define SOFT_RESET				0x01
#define WAIT_ONE_SECOND			usleep(1000000)
uint8_t znpSoftReset(int fd) {
	int8_t i = 0;
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(SYS_RESET_REQ);
	i++;
	znpBuf[i] = LSB(SYS_RESET_REQ);
	i++;
	znpBuf[i] = SOFT_RESET;
	i++;
	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;

	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}
	return waitingForMessage(fd, SYS_RESET_IND);
}

/** Configures startup options on the ZNP. These will reset various parameters back to their factory defaults.
 The radio supports two types of clearing state, and both are supported:
 - STARTOPT_CLEAR_CONFIG restores all settings to factory defaults. Must restart the ZNP after using this option.
 - STARTOPT_CLEAR_STATE only clears network settings (PAN ID, channel, etc.)
 @note all ZB_WRITE_CONFIGURATION commands take approx. 3.5mSec between SREQ & SRSP; presumably to write to flash inside the CC2530ZNP.
 @param option which options to set. Must be zero, STARTOPT_CLEAR_CONFIG, or STARTOPT_CLEAR_STATE.
 @post znpResult contains the error code, or ZNP_SUCCESS if success.
 */

uint8_t setStartupOptions(int fd, uint8_t option) {
	int8_t i = 0;
	if (option > (STARTOPT_CLEAR_CONFIG + STARTOPT_CLEAR_STATE)) {
		APP_PRINT("ERROR: set option value! \n");
		return ZNP_NOT_SUCCESS;
	}

	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(ZB_WRITE_CONFIGURATION);
	i++;
	znpBuf[i] = LSB(ZB_WRITE_CONFIGURATION);
	i++;

	znpBuf[i] = ZCD_NV_STARTUP_OPTION;
	i++;
	znpBuf[i] = ZCD_NV_STARTUP_OPTION_LEN;
	i++;
	znpBuf[i] = option;
	i++;
	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;

	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}

	return waitingForMessage(fd, (ZB_WRITE_CONFIGURATION | 0x6000));
}

/** Configures the ZNP to only join a network with the given panId.
 If panId = ANY_PAN then the radio will join any network.
 @param panId the PANID to join, or ANY_PAN to join any PAN
 @post znpResult contains the error code, or ZNP_SUCCESS if success.
 */
uint8_t setPanId(int fd, uint16_t panId) {

	int8_t i = 0;
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(ZB_WRITE_CONFIGURATION);
	i++;
	znpBuf[i] = LSB(ZB_WRITE_CONFIGURATION);
	i++;
	znpBuf[i] = ZCD_NV_PANID;
	i++;
	znpBuf[i] = ZCD_NV_PANID_LEN;
	i++;
	znpBuf[i] = LSB(panId);
	i++;
	znpBuf[i] = MSB(panId);
	i++;
	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;
	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}
	return waitingForMessage(fd, (ZB_WRITE_CONFIGURATION | 0x6000));
}

//uint8_t setPanIdReset(int fd) {
//
//	int8_t i = 0;
//	uint16_t panId = 0xFFFF;
//	znpBuf[i] = ZNP_SOF;
//	i++;
//	znpBuf[i] = 0;
//	i++;
//	znpBuf[i] = MSB(ZB_WRITE_CONFIGURATION);
//	i++;
//	znpBuf[i] = LSB(ZB_WRITE_CONFIGURATION);
//	i++;
//	znpBuf[i] = ZCD_NV_PANID;
//	i++;
//	znpBuf[i] = ZCD_NV_PANID_LEN;
//	i++;
//	znpBuf[i] = LSB(panId);
//	i++;
//	znpBuf[i] = MSB(panId);
//	i++;
//	znpBuf[1] = i - 4;
//	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
//	i++;
//	if (write(fd, znpBuf, i) < 0) {
//		APP_PRINT("ERROR: write");
//		return ZNP_NOT_SUCCESS;
//	}
//	return ZNP_SUCCESS;
//}

/** Sets the Zigbee Device Type for the ZNP.
 @param deviceType the type of Zigbee device. Must be COORDINATOR, ROUTER, or END_DEVICE
 @post znpResult contains the error code, or ZNP_SUCCESS if success.
 */
uint8_t setZigbeeDeviceType(int fd, uint8_t deviceType) {
	int8_t i = 0;
	if (deviceType > END_DEVICE) {
		return ZNP_NOT_SUCCESS;
	}
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(ZB_WRITE_CONFIGURATION);
	i++;
	znpBuf[i] = LSB(ZB_WRITE_CONFIGURATION);
	i++;

	znpBuf[i] = ZCD_NV_LOGICAL_TYPE;
	i++;
	znpBuf[i] = ZCD_NV_LOGICAL_TYPE_LEN;
	i++;
	znpBuf[i] = deviceType;
	i++;
	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;
	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}

	return waitingForMessage(fd, (ZB_WRITE_CONFIGURATION | 0x6000));
}

/** Sets the RF Output power.
 @param Tx Output Power, in dB. e.g. -10 = -10dB, 3 = +3dB etc.
 */
uint8_t setTransmitPower(int fd, int8_t txPowerDb) {

	int8_t i = 0;
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(SYS_SET_TX_POWER);
	i++;
	znpBuf[i] = LSB(SYS_SET_TX_POWER);
	i++;
	znpBuf[i] = txPowerDb;
	i++;
	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;
	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}

	int8_t znpResult = waitingForMessage(fd, (SYS_SET_TX_POWER | 0x6000));
	if (znpResult == txPowerDb) {
		return ZNP_SUCCESS;
	}
	return ZNP_NOT_SUCCESS;
}

//void set_TX_Power(int fd, unsigned char power) {
//	unsigned char buf1[8], buf2[16];
//	int n_read;
//
//	buf1[1] = 0x02;
//	buf1[2] = 0x21;
//	buf1[3] = 0x14;
//	buf1[4] = 0x00;
//	buf1[5] = power;
//	buf1[6] = calcFCS((unsigned char *) &buf1[1], 5);
//
//	if (write(fd, buf1, 7) < 0) {
//		APP_PRINT("ERROR: write");
//	}
//	usleep(NORMAL_CMD_WAIT);
//
//	APP_PRINT("set_TX_Power() return ...");
//	n_read = read(fd, buf2, 16);
//	print_receiving_bytes(n_read, buf2);
//}

/*setChannelMask*/
uint8_t setChannelMask(int fd, uint8_t primary, uint32_t channelMask) {

	int8_t i = 0;
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(APP_CNF_BDB_SET_CHANNEL);
	i++;
	znpBuf[i] = LSB(APP_CNF_BDB_SET_CHANNEL);
	i++;

	znpBuf[i] = primary;
	i++;
	znpBuf[i] = LSB(channelMask);
	i++;
	znpBuf[i] = (channelMask & 0xFF00) >> 8;
	i++;
	znpBuf[i] = (channelMask & 0xFF0000) >> 16;
	i++;
	znpBuf[i] = channelMask >> 24;
	i++;
	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;

	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}

	return waitingForMessage(fd, (APP_CNF_BDB_SET_CHANNEL | 0x6000));
}

uint8_t afRegisterGenericApplication(int fd) {
	int8_t i = 0;
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(AF_REGISTER);
	i++;
	znpBuf[i] = LSB(AF_REGISTER);
	i++;
	znpBuf[i] = DEFAULT_ENDPOINT;
	i++;
	znpBuf[i] = LSB(DEFAULT_PROFILE_ID);
	i++;
	znpBuf[i] = MSB(DEFAULT_PROFILE_ID);
	i++;
	znpBuf[i] = LSB(DEVICE_ID);
	i++;
	znpBuf[i] = MSB(DEVICE_ID);
	i++;
	znpBuf[i] = DEVICE_VERSION;
	i++;
	znpBuf[i] = LATENCY_NORMAL;
	i++;
	znpBuf[i] = 0;
	i++;                // number of binding input clusters
	znpBuf[i] = 0;
	i++;            // number of binding output clusters
	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;

	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}

	return waitingForMessage(fd, (AF_REGISTER | 0x6000));

}

uint8_t zbAppRegisterRequest(int fd) {
	int8_t i = 0;
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(ZB_APP_REGISTER_REQUEST);
	i++;
	znpBuf[i] = LSB(ZB_APP_REGISTER_REQUEST);
	i++;
	znpBuf[i] = DEFAULT_ENDPOINT;
	i++;
	znpBuf[i] = LSB(DEFAULT_PROFILE_ID);
	i++;
	znpBuf[i] = MSB(DEFAULT_PROFILE_ID);
	i++;
	znpBuf[i] = LSB(DEVICE_ID);
	i++;
	znpBuf[i] = MSB(DEVICE_ID);
	i++;
	znpBuf[i] = DEVICE_VERSION;
	i++;
	znpBuf[i] = LATENCY_NORMAL;
	i++;
	znpBuf[i] = 0;
	i++;                // number of binding input clusters
	znpBuf[i] = 0;
	i++;            // number of binding output clusters

	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;

	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}

	return waitingForMessage(fd, (ZB_APP_REGISTER_REQUEST | 0x6000));

}

uint8_t zbStartRequest(int fd) {

	int8_t i = 0;
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(ZB_START_REQUEST);
	i++;
	znpBuf[i] = LSB(ZB_START_REQUEST);
	i++;
	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;

	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}
//	return waitingForMessage(fd, ( ZDO_STARTUP_FROM_APP | 0x6000));
	return waitingForMessage(fd, ZB_START_CONFIRM);
}

uint8_t zdoStartApplication(int fd) {

	int8_t i = 0;
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(ZDO_STARTUP_FROM_APP);
	i++;
	znpBuf[i] = LSB(ZDO_STARTUP_FROM_APP);
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;

	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}
//	return waitingForMessage(fd, ( ZDO_STARTUP_FROM_APP | 0x6000));
	return waitingForStatus(fd, ZDO_STATE_CHANGE_IND, 0x09); // waiting state 9
}

uint8_t util_get_device_info(int fd) {

	int8_t i = 0;
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(UTIL_GET_DEVICE_INFO);
	i++;
	znpBuf[i] = LSB(UTIL_GET_DEVICE_INFO);
	i++;
	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;

	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write\n");
		return ZNP_NOT_SUCCESS;
	}

	APP_PRINT("util_get_device_info\n");
	return ZNP_SUCCESS;
}

/** Enable/Disabless callbacks on the ZNP.
 @param cb must be either CALLBACKS_ENABLED or CALLBACKS_DISABLED
 @see section ZCD_NV_ZDO_DIRECT_CB in ZNP Interface Specification
 @post znpResult contains the error code, or ZNP_SUCCESS if success.
 */
uint8_t setCallbacks(int fd, uint8_t cb) {
	if ((cb != CALLBACKS_ENABLED) && (cb != CALLBACKS_DISABLED)) {
		return ZNP_NOT_SUCCESS;
	}
	int8_t i = 0;
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(ZB_WRITE_CONFIGURATION);
	i++;
	znpBuf[i] = LSB(ZB_WRITE_CONFIGURATION);
	i++;
	znpBuf[i] = ZCD_NV_ZDO_DIRECT_CB;
	i++;
	znpBuf[i] = ZCD_NV_ZDO_DIRECT_CB_LEN;
	i++;
	znpBuf[i] = cb;
	i++;
	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;

	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}
	return waitingForMessage(fd, (ZB_WRITE_CONFIGURATION | 0x6000));

}

uint8_t startZigbeeCoordinator(int serial_fd, uint8_t startup_option) {

	uint8_t znpResult;

	znpResult = znpSoftReset(serial_fd);
	if (znpResult == ZNP_NOT_SUCCESS) {
		APP_PRINT("ERROR: reset ZNP \n");
		return znpResult;
	}

	if (startup_option == 0) {
		APP_PRINT("Skipping startup option !\n");
	} else {
		znpResult = setStartupOptions(serial_fd, DEFAULT_STARTUP_OPTIONS);
		if (znpResult != ZNP_SUCCESS) {
			APP_PRINT("ERROR: startup option. \n");
			return znpResult;
		}

		znpResult = znpSoftReset(serial_fd);
		if (znpResult == ZNP_NOT_SUCCESS) {
			APP_PRINT("ERROR: reset ZNP \n");
			return znpResult;
		}

		// new_zigbee_key
		uint8_t new_zigbee_key[16];
		time_t now = time(0);
		struct tm mtime = *localtime(&now);
		new_zigbee_key[0] = (uint8_t) mtime.tm_sec;
		new_zigbee_key[1] = (uint8_t) mtime.tm_min;
		new_zigbee_key[2] = (uint8_t) mtime.tm_hour;
		new_zigbee_key[3] = (uint8_t) mtime.tm_mday;
		new_zigbee_key[4] = (uint8_t) mtime.tm_mon;
		new_zigbee_key[5] = (uint8_t) mtime.tm_wday;
		new_zigbee_key[6] = LSB(mtime.tm_year);
		new_zigbee_key[7] = MSB(mtime.tm_year);

		hexCharsToBytes((uint8_t*) s_ieee_address, 16, (uint8_t*) &new_zigbee_key[8]);
		APP_PRINT("new zigbee key :");
		for (int i = 0; i < 16; i++) {
			APP_PRINT(" %02X", new_zigbee_key[i]);
		}
		APP_PRINT("\n");

// security mode
		znpResult = setSecurityMode(serial_fd, SECURITY_MODE_COORD_DIST_KEYS);
		if (znpResult != ZNP_SUCCESS) {
			APP_PRINT("ERROR: SECURITY MODE \n");
			return znpResult;
		}
		// security key ;
		znpResult = setSecurityKey(serial_fd, new_zigbee_key);
		if (znpResult != ZNP_SUCCESS) {
			APP_PRINT("ERROR: SECURITY KEY \n");
			return znpResult;
		}
		znpResult = setPanId(serial_fd, (uint16_t) PAN_ID);
		if (znpResult != ZNP_SUCCESS) {
			APP_PRINT("ERROR: PAN ID \n");
			return znpResult;
		}

		znpResult = setZigbeeDeviceType(serial_fd, COORDINATOR);
		if (znpResult != ZNP_SUCCESS) {
			APP_PRINT("ERROR: Device type \n");
			return znpResult;
		}

		//Set primary channel mask & disable secondary channel mask
		znpResult = setChannelMask(serial_fd, CHANNEL_TRUE, (uint32_t) DEFAULT_CHANNEL_MASK);
		if (znpResult != ZNP_SUCCESS) {
			APP_PRINT("ERROR: set channel mask \n");
			return znpResult;
		}
		znpResult = setChannelMask(serial_fd, CHANNEL_FALSE, (uint32_t) 0);
		if (znpResult != ZNP_SUCCESS) {
			APP_PRINT("ERROR: set channel mask \n");
			return znpResult;
		}
		// Start commissioning using network formation as parameter to start coordinator

		app_cnf_bdb_start_commissioning(serial_fd, COMMISSIONING_MODE_INFORMATION, 2, 0);		// 0x04 is Network Formation
//		wait4_commissioning_notification(serial_fd, 0, 2);

//	if (startup_option > 0) {
//		util_get_device_info(serial_fd);
//		wait4_coordinator_up_ok(serial_fd, 0, 7, 9);
//	}
		sleep(2);
		znpResult = app_cnf_set_allowrejoin_tc_policy(serial_fd, 1);
		if (znpResult != ZNP_SUCCESS) {
			APP_PRINT("ERROR: set allow join \n");
			return znpResult;
		}
//	set_TX_Power(serial_fd, DEFAULT_TX_POWER);
		znpResult = setTransmitPower(serial_fd, DEFAULT_TX_POWER);
		if (znpResult != ZNP_SUCCESS) {
			APP_PRINT("ERROR: set transmit power \n");
			return znpResult;
		}
		// Set ZCD_NV_ZDO_DIRECT_CB
		znpResult = setCallbacks(serial_fd, CALLBACKS_ENABLED);
		if (znpResult != ZNP_SUCCESS) {
			APP_PRINT("ERROR: set callback \n");
			return znpResult;
		}

		znpResult = app_cnf_bdb_start_commissioning(serial_fd, COMMISSIONING_MODE_STEERING, 1, 0);		// 0x02 is Network Steering
		if (znpResult != ZNP_SUCCESS) {
			APP_PRINT("ERROR: Network Steering \n");
			return znpResult;
		}
		sleep(2);
	}

	znpResult = znpSoftReset(serial_fd);
	if (znpResult == ZNP_NOT_SUCCESS) {
		APP_PRINT("ERROR: reset ZNP \n");
		return znpResult;
	}

	sleep(2);
	znpResult = app_cnf_bdb_start_commissioning(serial_fd, COMMISSIONING_MODE_STEERING, 1, 0);		// 0x02 is Network Steering
	if (znpResult != ZNP_SUCCESS) {
		APP_PRINT("ERROR: Network Steering \n");
		return znpResult;
	}

	znpResult = setTCRequireKeyExchange(serial_fd, 0);
	if (znpResult != ZNP_SUCCESS) {
		APP_PRINT("ERROR: set TC key exchange \n");
		return znpResult;
	}
	znpResult = afRegisterGenericApplication(serial_fd);
	if (znpResult != ZNP_SUCCESS) {
		APP_PRINT("ERROR: af register \n");
		return znpResult;
	}
	znpResult = zdoStartApplication(serial_fd);
	if (znpResult != 0x09) { //state 9
		APP_PRINT("ERROR: ZDO start application \n");
		return znpResult;
	}
	znpResult = setPermitJoiningReq(serial_fd, SHORT_ADDRESS_COORDINATOR, DISABLE_PERMIT_JOIN, 0);
	if (znpResult != ZNP_SUCCESS) {
		APP_PRINT("ERROR: disabled permit join \n");
		return znpResult;
	}
	/*get MAC address of coordinator*/
	getMACAddressReq(serial_fd, SHORT_ADDRESS_COORDINATOR, 0x00, 0x00);

	return ZNP_SUCCESS;
}

uint8_t setTCRequireKeyExchange(int fd, uint8_t bdb_TrustCenterRequireKeyExchange) {

	int8_t i = 0;
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(APP_CNF_BDB_SET_TC_REQUIRE_KEY_EXCHANGE);
	i++;
	znpBuf[i] = LSB(APP_CNF_BDB_SET_TC_REQUIRE_KEY_EXCHANGE);
	i++;
	znpBuf[i] = bdb_TrustCenterRequireKeyExchange;
	i++;
	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;

	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}
	return waitingForMessage(fd, APP_CNF_BDB_SET_TC_REQUIRE_KEY_EXCHANGE_SRSP);
}

uint8_t setPermitJoiningReq(int fd, uint16_t short_addr, uint8_t Timeout, uint8_t flagWaiting) {
	int8_t i = 0;
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(ZB_PERMIT_JOINING_REQUEST);
	i++;
	znpBuf[i] = LSB(ZB_PERMIT_JOINING_REQUEST);
	i++;
	znpBuf[i] = LSB(short_addr);
	i++;
	znpBuf[i] = MSB(short_addr);
	i++;
	znpBuf[i] = Timeout;
	i++;
	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;

	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}

	if (flagWaiting == 0) {
		return waitingForMessage(fd, ZDO_MGMT_PERMIT_JOIN_RSP);
	} else {
		return ZNP_SUCCESS;
	}
}

/**For example, Device A sends a ZDO_MGMT_LEAVE_REQ to device B.
 *Next, Device A's module will send a ZDO_MGMT_LEAVE_RSP (0x45B4) to host Then Device A will send a ZDO_LEAVE_IND with source address = Device B's short address
 *Device B's module will leave the network and send a ZDO_LEAVE_IND (0x45C9) to host, with source address = Device A's short address
 *Device B's module will then reset and send a reset indication to host
 *Device B is now off the network
 */
uint8_t zdo_mgmt_leave_req(int fd, uint16_t short_add, uint8_t ieee_addr[8], uint8_t flags) {

//	uint8_t data[] = { 0xF4, 0xC2, 0xF6, 0x00, 0x00, 0x8D, 0x15, 0x00 }; /*sensor_motion*/
//	uint8_t data[] = { 0x5D, 0x35, 0x14, 0x01, 0x00, 0x8D, 0x15, 0x00 }; /*sensor_temperature*/
//	memcpy(ieee_addr, data, 8);
//	short_add = 0x7805;
//	flags = 0x00;
	int8_t i = 0;
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(ZDO_MGMT_LEAVE_REQ);
	i++;
	znpBuf[i] = LSB(ZDO_MGMT_LEAVE_REQ);
	i++;
	znpBuf[i] = LSB(short_add);
	i++;
	znpBuf[i] = MSB(short_add);
	i++;
	memcpy((uint8_t*) &znpBuf[i], ieee_addr, 8);
	i += 8;
	znpBuf[i] = flags;
	i++;

	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;

	for (int j = 0; j < i; j++) {
		APP_PRINT("%02X ", znpBuf[j]);
	}
	APP_PRINT("\n");

	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}

	return ZNP_SUCCESS;
//	return waitingForMessage(fd, (ZDO_MGMT_LEAVE_REQ | 0x6000));
}

/*Get MAC Address*/
uint8_t getMACAddressReq(int fd, uint16_t short_addr, uint8_t req_type, uint8_t start_index) {
	int8_t i = 0;
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(ZDO_IEEE_ADDR_REQ);
	i++;
	znpBuf[i] = LSB(ZDO_IEEE_ADDR_REQ);
	i++;
	znpBuf[i] = LSB(short_addr);
	i++;
	znpBuf[i] = MSB(short_addr);
	i++;
	znpBuf[i] = req_type;
	i++;
	znpBuf[i] = start_index;
	i++;
	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;

	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}
	return ZNP_SUCCESS;
}

/*send af_data_request*/
uint8_t send_af_data_request(int fd, af_data_request_t afDataRequest) {

	int32_t i = 0;
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(AF_DATA_REQUEST);
	i++;
	znpBuf[i] = LSB(AF_DATA_REQUEST);
	i++;
	znpBuf[i] = LSB(afDataRequest.dst_address);
	i++;
	znpBuf[i] = MSB(afDataRequest.dst_address);
	i++;
	znpBuf[i] = afDataRequest.dst_endpoint;
	i++;
	znpBuf[i] = afDataRequest.src_endpoint;
	i++;
	znpBuf[i] = MSB(afDataRequest.cluster_id);
	i++;
	znpBuf[i] = LSB(afDataRequest.cluster_id);
	i++;
	znpBuf[i] = afDataRequest.trans_id;
	i++;
	znpBuf[i] = afDataRequest.options;
	i++;
	znpBuf[i] = afDataRequest.radius;
	i++;
	znpBuf[i] = afDataRequest.len;
	i++;
	memcpy((uint8_t*) &znpBuf[i], afDataRequest.data, afDataRequest.len);
	i += afDataRequest.len;
	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;

	for (int j = 0; j < i; j++) {
		APP_PRINT("%02X ", znpBuf[j]);
	}
	APP_PRINT("\n");

	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}

	return ZNP_SUCCESS;
}

/** Configures the ZNP for the specified security mode.
 If securityMode = SECURITY_MODE_OFF then only ZCD_NV_SECURITY_MODE is written.
 Otherwise ZCD_NV_SECURITY_MODE and ZCD_NV_PRECFGKEYS_ENABLE are written.
 @note if NOT using pre-configured keys then the coordinator will distribute its key to all devices.
 @param securityMode SECURITY_MODE_OFF or SECURITY_MODE_PRECONFIGURED_KEYS or SECURITY_MODE_COORD_DIST_KEYS
 @post znpResult contains the error code, or ZNP_SUCCESS if success.
 */
uint8_t setSecurityMode(int fd, unsigned char securityMode) {

	if (securityMode > SECURITY_MODE_COORD_DIST_KEYS) {
		//znpResult = -1;
		return ZNP_NOT_SUCCESS;
	}
//#ifdef ZNP_INTERFACE_VERBOSE
//    APP_PRINT("Setting Security = %s\r\n", getSecurityModeName(securityMode));
//#endif
	//zb_write_config(fd,ZCD_NV_SECURITY_MODE,ZCD_NV_SECURITY_MODE_LEN,(securityMode > 0));
	int32_t i = 0;
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(ZB_WRITE_CONFIGURATION);
	i++;
	znpBuf[i] = LSB(ZB_WRITE_CONFIGURATION);
	i++;
	znpBuf[i] = ZCD_NV_SECURITY_MODE;
	i++;
	znpBuf[i] = ZCD_NV_SECURITY_MODE_LEN;
	i++;
	znpBuf[i] = (securityMode > 0);
	i++;
	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;

	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}
	if (waitingForMessage(fd, (ZB_WRITE_CONFIGURATION | 0x6000)) == ZNP_NOT_SUCCESS) {

		APP_PRINT("write Configure pre-configured key not success\n");
		return ZNP_NOT_SUCCESS;
	}

	if (securityMode != SECURITY_MODE_OFF) {   //if turning security off, don't need to set pre-configured keys on/off
		//2. Configure pre-configured keys on/off:
//        APP_PRINT("SECURITY MODE PRECONFIGURED KEYS : %s\n",getSecurityModeName(securityMode));
		i = 0;
		znpBuf[i] = ZNP_SOF;
		i++;
		znpBuf[i] = 0;
		i++;
		znpBuf[i] = MSB(ZB_WRITE_CONFIGURATION);
		i++;
		znpBuf[i] = LSB(ZB_WRITE_CONFIGURATION);
		i++;
		znpBuf[i] = ZCD_NV_PRECFGKEYS_ENABLE;
		i++;
		znpBuf[i] = ZCD_NV_PRECFGKEYS_ENABLE_LEN;
		i++;
		znpBuf[i] = (securityMode == SECURITY_MODE_PRECONFIGURED_KEYS);
		i++;
		znpBuf[1] = i - 4;
		znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
		i++;

		if (write(fd, znpBuf, i) < 0) {
			APP_PRINT("ERROR: write");
			return ZNP_NOT_SUCCESS;
		}
		return waitingForMessage(fd, (ZB_WRITE_CONFIGURATION | 0x6000));
	}
	return ZNP_SUCCESS;
}

/** Loads the specified key into the ZNP. Does not change security mode, need to use setSecurityMode() too.
 @note if NOT using pre-configured keys then the coordinator will distribute its key to all devices.
 @param key preConfiguredKey a 16B key to use
 @pre setSecurityMode() called
 @post znpResult contains the error code, or ZNP_SUCCESS if success.
 */
uint8_t setSecurityKey(int fd, uint8_t key[16]) {

	APP_PRINT("\nSetting Security Key: ");
	for (int j = 0; j < ZCD_NV_PRECFGKEY_LEN; j++) {
		APP_PRINT("%02X ", key[j]);
	}
	APP_PRINT("\n");
	int32_t i = 0;
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(ZB_WRITE_CONFIGURATION);
	i++;
	znpBuf[i] = LSB(ZB_WRITE_CONFIGURATION);
	i++;
	znpBuf[i] = ZCD_NV_PRECFGKEY;
	i++;
	znpBuf[i] = ZCD_NV_PRECFGKEY_LEN;
	i++;
	memcpy((uint8_t*) &znpBuf[i], key, ZCD_NV_PRECFGKEY_LEN);
	i += ZCD_NV_PRECFGKEY_LEN;
	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;

	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}

	return waitingForMessage(fd, (ZB_WRITE_CONFIGURATION | 0x6000));

}
//ZB_GET_DEVICE_INFO
uint8_t zb_get_device_info(int fd, uint8_t param, uint8_t* rx_buffer, int32_t* len) {
	int32_t i = 0;
	znpBuf[i] = ZNP_SOF;
	i++;
	znpBuf[i] = 0;
	i++;
	znpBuf[i] = MSB(ZB_GET_DEVICE_INFO);
	i++;
	znpBuf[i] = LSB(ZB_GET_DEVICE_INFO);
	i++;
	znpBuf[i] = param;
	i++;
	znpBuf[1] = i - 4;
	znpBuf[i] = calcFCS((uint8_t *) &znpBuf[1], (i - 1));
	i++;

	if (write(fd, znpBuf, i) < 0) {
		APP_PRINT("ERROR: write");
		return ZNP_NOT_SUCCESS;
	}

//    uint16_t cmd = ZB_GET_DEVICE_INFO | 0x6000;
//    i = 10;
//    while (i > 0) {
//        *len = read(fd, rx_buffer, RX_BUFFER_SIZE);
//        for (int j = 0; j < *len; j++) {
//            APP_PRINT(" %02X", rx_buffer[j]);
//        }
//        APP_PRINT("\n");
//        if (*len > 0) {
//            int j = 0;
//            while (j < *len) {
//                if (rx_buffer[j] == ZNP_SOF) {
//                    if ( CONVERT_TO_INT(rx_buffer[j + 3], rx_buffer[j + 2]) == cmd) {
//                        return ZNP_SUCCESS;
//                    }
//                }
//                j++;
//            }
//
//        } else {
//            i--;
//            APP_PRINT("i = %d \n", i);
//        }
//    }
	return getMsgReturn(fd, (ZB_GET_DEVICE_INFO | 0x6000), param, rx_buffer, len);

	//return waitingForMessage(fd, (ZB_GET_DEVICE_INFO | 0x6000));
}

