#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <semaphore.h>
#include <signal.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netdb.h>
#include <setjmp.h>
#include <errno.h>

#include "../ak/ak.h"
#include "../ak/timer.h"

#include "../sys/sys_dbg.h"

#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_sync.h"

#define MAX_ERROR_COUNT				4
#define SLEEP_INTERVAL				5
#define PACKET_SIZE					4096
#define TIMEOUT_RECVFROM			20//20s

tsm_tbl_t tsm_sync_ping;

static void sync_check_connect(ak_msg_t*);
static void sync_create_socket(ak_msg_t*);
static void sync_send_ICMP(ak_msg_t*);
static void sync_recv_ICMP(ak_msg_t*);

tsm_t sync_init[] = {
	{ SYNC_CHECK_CONNECT	,	SYNC_INIT	,	sync_check_connect	},
	{ SYNC_CREATE_SOCKET	,	SYNC_PING	,	sync_create_socket	},
};

tsm_t sync_ping[] = {
	{ SYNC_SEND_ICMP		,	SYNC_PING	,	sync_send_ICMP		},
	{ SYNC_RECV_ICMP		,	SYNC_INIT	,	sync_recv_ICMP		},
};

tsm_t* tsm_sync_ping_table[] {
	sync_init,
	sync_ping
};

static char sendpacket[PACKET_SIZE];
static char recvpacket[PACKET_SIZE];

static int sockfd, datalen = 56;
static struct sockaddr_in dest_addr;
static pid_t pid;
struct sockaddr_in from;
struct timeval tvrecv;

static uint32_t ip_ok_counter = 0;
static uint32_t ip_err_counter = 0;

static unsigned short cal_chksum(unsigned short *addr, int len);
static int pack(int pack_no);
static int unpack(char *buf, int len);
static void tv_sub(struct timeval *out, struct timeval *in);
static void timeout_recvfrom(int sig);

q_msg_t gw_task_sync_mailbox;

void* gw_task_sync_entry(void*) {
	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("[STARTED] mt_task_sync_entry\n");

//	set_app_connect_cloud_state(APP_CONNECT_BY_LAN);
//	set_enable_connect_gsm(false);

	tsm_init(&tsm_sync_ping, tsm_sync_ping_table, SYNC_INIT);
	tsm_sync_ping.on_state = tsm_sync_ping_on_state;

	memcpy(g_config_parameters.mqtt_host, "static1.888999.vn", strlen("static1.888999.vn"));
	timer_set(ID_TASK_SYNC, SYNC_CHECK_CONNECT, MT_SYNC_COMMON_INTERVAL, TIMER_ONE_SHOT);

	ip_err_counter = 0;
	ip_ok_counter = 0;

	while (1) {
		while (msg_available(ID_TASK_SYNC)) {
			/* get messge */
			ak_msg_t* msg = rev_msg(ID_TASK_SYNC);

			tsm_dispatch(&tsm_sync_ping, msg);

			/* free message */
			free_msg(msg);
		}

		usleep(100);
	}

	return (void*)0;

}

void tsm_sync_ping_on_state(tsm_state_t state) {
	(void)(state);
}

void sync_check_connect(ak_msg_t*){
	//APP_DBG("\n[PING] ip_ok_counter=%d\n", ip_ok_counter);
	//APP_DBG("[PING] ip_err_counter=%d\n", ip_err_counter);

	connect_cloud_state_t stt = get_app_connect_cloud_state();
	if(stt == APP_CONNECT_BY_LAN) {
//		if(ip_err_counter >= MAX_ERROR_COUNT){
//			//set_app_connect_cloud_state(APP_CONNECT_BY_GSM);
//			set_enable_connect_gsm(true);
//			ip_err_counter = 0;
//			ip_ok_counter = 0;

//			APP_DBG("[PING] set_enable_connect_gsm = true\n");
//		}
		APP_DBG("[PING] app_connect_cloud_state = APP_CONNECT_BY_LAN\n");
	}
	else if(stt == APP_CONNECT_BY_GSM) {
//		if(ip_ok_counter >= MAX_ERROR_COUNT){
//			APP_DBG("[PING] set_enable_connect_gsm = false\n");
//			//set_app_connect_cloud_state(APP_CONNECT_BY_LAN);
//			set_enable_connect_gsm(false);
//			ip_err_counter = 0;
//			ip_ok_counter = 0;
//		}
		APP_DBG("[PING] app_connect_cloud_state = APP_CONNECT_BY_GSM\n");
	}

	ak_msg_t* s_msg = get_pure_msg();
	set_msg_sig(s_msg, SYNC_CREATE_SOCKET);
	task_post(ID_TASK_SYNC, s_msg);
}

void sync_create_socket(ak_msg_t*){
	//APP_DBG("[PING] SYNC_CREATE_SOCKET\n");

	struct protoent *protocol;

	if ((protocol = getprotobyname("icmp")) == NULL) {
		APP_DBG("[PING] getprotobyname error\n");

		connect_cloud_state_t stt = get_app_connect_cloud_state();
		if(stt == APP_CONNECT_BY_LAN) {
			ip_err_counter++;
		}
		else if(stt == APP_CONNECT_BY_GSM) {
			ip_ok_counter = 0;
		}


		sleep(2);

		ak_msg_t* s_msg = get_pure_msg();
		set_msg_sig(s_msg, SYNC_CHECK_CONNECT);
		task_post(ID_TASK_SYNC, s_msg);

		return;

	}

	if ((sockfd = socket(AF_INET, SOCK_RAW, protocol->p_proto)) < 0) {
		APP_DBG("[PING] socket error\n");

		connect_cloud_state_t stt = get_app_connect_cloud_state();
		if(stt == APP_CONNECT_BY_LAN) {
			ip_err_counter++;
		}
		else if(stt == APP_CONNECT_BY_GSM) {
			ip_ok_counter = 0;
		}

		close(sockfd);

		sleep(2);

		ak_msg_t* s_msg = get_pure_msg();
		set_msg_sig(s_msg, SYNC_CHECK_CONNECT);
		task_post(ID_TASK_SYNC, s_msg);

		return;

	}

	int size = 50 * 1024;

	setuid(getuid());
	setsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, &size, sizeof(size));
	bzero(&dest_addr, sizeof(dest_addr));
	dest_addr.sin_family = AF_INET;

	if (inet_addr((const char*)g_config_parameters.mqtt_host) == INADDR_NONE) {
		struct hostent *host;
		if ((host = gethostbyname((const char*)g_config_parameters.mqtt_host)) == NULL) {
			APP_DBG("[PING] gethostbyname error\n");

			connect_cloud_state_t stt = get_app_connect_cloud_state();
			if(stt == APP_CONNECT_BY_LAN) {
				ip_err_counter++;
			}
			else if(stt == APP_CONNECT_BY_GSM) {
				ip_ok_counter = 0;
			}

			close(sockfd);

			sleep(2);

			ak_msg_t* s_msg = get_pure_msg();
			set_msg_sig(s_msg, SYNC_CHECK_CONNECT);
			task_post(ID_TASK_SYNC, s_msg);

			return;
		}
		memcpy((char*) &dest_addr.sin_addr, host->h_addr, host->h_length);
	}
	else {
		dest_addr.sin_addr.s_addr = inet_addr((const char*)g_config_parameters.mqtt_host);
	}
	pid = getpid();

	APP_DBG("[PING] %s(%s): %d bytes data in ICMP packets.\n", g_config_parameters.mqtt_host, inet_ntoa(dest_addr.sin_addr), datalen);

	ak_msg_t* s_msg = get_pure_msg();
	set_msg_sig(s_msg, SYNC_SEND_ICMP);
	task_post(ID_TASK_SYNC, s_msg);
}

void sync_send_ICMP(ak_msg_t*){
	//APP_DBG("[PING] SYNC_SEND_ICMP\n");

	int packetsize = pack(1);

	if (sendto(sockfd, sendpacket, packetsize, 0, (struct sockaddr*) &dest_addr, sizeof(dest_addr)) < 0) {
		APP_DBG("[PING] sendto error\n");

		connect_cloud_state_t stt = get_app_connect_cloud_state();
		if(stt == APP_CONNECT_BY_LAN) {
			ip_err_counter++;
		}
		else if(stt == APP_CONNECT_BY_GSM) {
			ip_ok_counter = 0;
		}

		close(sockfd);

		sleep(SLEEP_INTERVAL);

		tsm_tran(&tsm_sync_ping, SYNC_INIT);

		ak_msg_t* s_msg = get_pure_msg();
		set_msg_sig(s_msg, SYNC_CHECK_CONNECT);
		task_post(ID_TASK_SYNC, s_msg);
	}
	else {

		ak_msg_t* s_msg = get_pure_msg();
		set_msg_sig(s_msg, SYNC_RECV_ICMP);
		task_post(ID_TASK_SYNC, s_msg);

	}
}

void sync_recv_ICMP(ak_msg_t*){
	//APP_DBG("[PING] SYNC_RECV_ICMP\n");

	int n, fromlen;
	fromlen = sizeof(from);

	signal(SIGALRM, timeout_recvfrom);
	alarm(TIMEOUT_RECVFROM);

	if ((n = recvfrom(sockfd, recvpacket, sizeof(recvpacket), 0, (struct sockaddr*) &from, (socklen_t*) &fromlen)) < 0) {
		APP_DBG("[PING] recvfrom error\n");
		alarm(0);

		connect_cloud_state_t stt = get_app_connect_cloud_state();
		if(stt == APP_CONNECT_BY_LAN) {
			ip_err_counter++;
		}
		else if(stt == APP_CONNECT_BY_GSM) {
			ip_ok_counter = 0;
		}

		close(sockfd);

		tsm_tran(&tsm_sync_ping, SYNC_INIT);

		ak_msg_t* s_msg = get_pure_msg();
		set_msg_sig(s_msg, SYNC_CHECK_CONNECT);
		task_post(ID_TASK_SYNC, s_msg);

		return;
	}
	alarm(0);

	gettimeofday(&tvrecv, NULL);

	if (unpack(recvpacket, n) ==  - 1){
		APP_DBG("[PING] unpack error\n");

		connect_cloud_state_t stt = get_app_connect_cloud_state();
		if(stt == APP_CONNECT_BY_LAN) {
			ip_err_counter++;
		}
		else if(stt == APP_CONNECT_BY_GSM) {
			ip_ok_counter = 0;
		}

		close(sockfd);

		tsm_tran(&tsm_sync_ping, SYNC_INIT);

		ak_msg_t* s_msg = get_pure_msg();
		set_msg_sig(s_msg, SYNC_CHECK_CONNECT);
		task_post(ID_TASK_SYNC, s_msg);

		return;
	}
	else {

		connect_cloud_state_t stt = get_app_connect_cloud_state();
		if(stt == APP_CONNECT_BY_LAN) {
			ip_err_counter = 0;
		}
		else if(stt == APP_CONNECT_BY_GSM) {
			ip_ok_counter++;
		}

		close(sockfd);

		sleep(SLEEP_INTERVAL);

		ak_msg_t* s_msg = get_pure_msg();
		set_msg_sig(s_msg, SYNC_CHECK_CONNECT);
		task_post(ID_TASK_SYNC, s_msg);
	}
}

unsigned short cal_chksum(unsigned short *addr, int len) {
	int nleft = len;
	int sum = 0;
	unsigned short *w = addr;
	unsigned short answer = 0;

	while (nleft > 1)
	{
		sum +=  *w++;
		nleft -= 2;
	}
	if (nleft == 1)
	{
		*(unsigned char*)(&answer) = *(unsigned char*)w;
		sum += answer;
	}
	sum = (sum >> 16) + (sum &0xffff);
	sum += (sum >> 16);
	answer = ~sum;
	return answer;
}

int pack(int pack_no) {
	int packsize;
	struct icmp *icmp;
	struct timeval *tval;

	icmp = (struct icmp*)sendpacket;
	icmp->icmp_type = ICMP_ECHO;
	icmp->icmp_code = 0;
	icmp->icmp_cksum = 0;
	icmp->icmp_seq = pack_no;
	icmp->icmp_id = pid;
	packsize = 8+datalen;

	tval = (struct timeval*)icmp->icmp_data;
	gettimeofday(tval, NULL);
	icmp->icmp_cksum = cal_chksum((unsigned short*)icmp, packsize);

	return packsize;
}

int unpack(char *buf, int len) {
	int iphdrlen;
	struct ip *ip;
	struct icmp *icmp;
	struct timeval *tvsend;
	double rtt;
	ip = (struct ip*)buf;
	iphdrlen = ip->ip_hl << 2;
	icmp = (struct icmp*)(buf + iphdrlen);
	len -= iphdrlen;

	if (len < 8) {
		APP_DBG("[PING] ICMP packets\'s length is less than 8\n");
		return  - 1;
	}

	if ((icmp->icmp_type == ICMP_ECHOREPLY) && (icmp->icmp_id == pid)) {

		tvsend = (struct timeval*)icmp->icmp_data;
		tv_sub(&tvrecv, tvsend);
		rtt = tvrecv.tv_sec * 1000+tvrecv.tv_usec / 1000;
		APP_DBG("[PING] %d byte from %s: icmp_seq=%u ttl=%d rtt=%.3f ms\n", len,
				inet_ntoa(from.sin_addr), icmp->icmp_seq, ip->ip_ttl, rtt);
		return 0;
	}
	else{
		return  - 1;
	}
}

void tv_sub(struct timeval *out, struct timeval *in) {
	if ((out->tv_usec -= in->tv_usec) < 0) {
		--out->tv_sec;
		out->tv_usec += 1000000;
	}
	out->tv_sec -= in->tv_sec;
}


void timeout_recvfrom(int sig) {
	(void) sig;

	alarm(0);
	APP_DBG("[PING] timeout_recvfrom\n");

	shutdown(sockfd, SHUT_RDWR);
}
