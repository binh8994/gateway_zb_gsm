#include <string.h>

#include "../ak/ak.h"
#include "../ak/message.h"
#include "../ak/timer.h"

#include "mqtt_communication.h"

#include "app.h"
#include "app_dbg.h"
#include "../common/global_parameters.h"
#ifdef AES_ENCRYPT_FLAG
#include "../common/aes.h"
#endif
#include "task_list.h"

mqtt_communication::mqtt_communication(const char *id, const char *host, int port, char* username, char* password) :
		mosquittopp(id, false) {
	/* init private data */
	m_connect_ok_flag = -1;
	m_mid = 1;

	/* init mqtt */
	mosqpp::lib_init();

	/* connect */
	username_pw_set(username, password);
	string comm_topic_status = string((char*) g_config_parameters.mqtt_topic) + string("/status/");
	APP_PRINT(" comm_topic_status: %s \n", comm_topic_status.data());
	will_set(comm_topic_status.data(), strlen((const char*) "offline"), "offline", 1, true);
	connect_async(host, port, 3);
	loop_start();
}

mqtt_communication::~mqtt_communication() {
	loop_stop();
	mosqpp::lib_cleanup();
}

void mqtt_communication::set_topic_subcribe(const char* topic) {
	APP_DBG("[mqtt_communication] set_topic_subcribe\n");
	m_topic_subscribe = static_cast<string>(topic);
	subscribe(NULL, topic, 1);
}

void mqtt_communication::set_topic_publish(const char* topic) {
	m_topic_publish = static_cast<string>(topic);
}
void mqtt_communication::on_connect(int rc) {
	if (rc == 0) {
		m_connect_ok_flag = 0;
		APP_DBG("\n[mqtt_communication] on_connect OK\n\n");
		//		subscribe(NULL, m_topic_subscribe.data());

		if( get_enable_connect_gsm() == true ) {
			set_enable_connect_gsm(false);

			ak_msg_t* smsg = get_pure_msg();
			set_msg_sig(smsg, GSM_MQTT_SEND_DISCONNECT);
			task_post(ID_TASK_GSM, smsg);
		}
		set_app_connect_cloud_state(APP_CONNECT_BY_LAN);


	} else {
		APP_DBG("\n[mqtt_communication] on_connect ERROR: %d\n\n", rc);
	}
}

void mqtt_communication::on_disconnect(int rc) {
	APP_DBG("\n[mqtt_communication] on_disconnect: %d\n\n", rc);

	if(m_connect_ok_flag == 0) {
		m_connect_ok_flag = -1;

		set_enable_connect_gsm(true);
		ak_msg_t* smsg = get_pure_msg();
		set_msg_sig(smsg, GSM_MQTT_SEND_CONNECT);
		task_post(ID_TASK_GSM, smsg);
	}
	//reconnect_async();

}

void mqtt_communication::communication_public(uint8_t* msg, uint32_t len, bool retain) {
	APP_DBG("[mqtt_communication][public] msg:%s len:%d\n", msg, len);
	publish(&m_mid, m_topic_publish.data(), len, msg, 1, true); //qos = 1
}

void mqtt_communication::on_publish(int mid) {
	APP_DBG("[mqtt_communication][on_publish] mid: %d\n", mid);
}

void mqtt_communication::on_subscribe(int mid, int qos_count, const int *granted_qos) {
	(void) granted_qos;

	APP_DBG("[mqtt_communication][on_subscribe] mid:%d\tqos_count:%d\n", mid, qos_count);
}

void mqtt_communication::on_message(const struct mosquitto_message *message) {

//	if (!strcmp(message->topic, m_topic_subscribe.data())) {
	if (message->payloadlen > 0) {
		APP_DBG("[mqtt_communication][on _message] topic:%s\tpayloadlen:%d\n", message->topic, message->payloadlen);

#ifdef AES_ENCRYPT_FLAG
		int len = remove_all_chars((char*) message->payload, '\n', message->payloadlen);
#else
		int len = message->payloadlen;
#endif

		/* post message to mqtt task */
		ak_msg_t* s_msg = get_dymanic_msg();
		set_msg_sig(s_msg, MQTT_DATA_COMMUNICATION);
		set_data_dynamic_msg(s_msg, (uint8_t*) message->payload, len);

		set_msg_src_task_id(s_msg, ID_TASK_MQTT);
		task_post(ID_TASK_MQTT, s_msg);

		if (message->retain == true) {
			publish(&m_mid, message->topic, 0, NULL, 1, true);
		}

	}
}
