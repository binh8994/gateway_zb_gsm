#ifndef IF_UDP_BROADCAST_H
#define IF_UDP_BROADCAST_H

#include <stdint.h>

#include "../ak/message.h"

#include "app.h"
#include "app_dbg.h"

//extern if_parseinfo_frame_t if_parseinfo_frame;
extern q_msg_t gw_task_if_udp_mailbox;
extern void* gw_task_if_udp_broadcast_entry(void*);

#endif // IF_UDP_BROADCAST_H
