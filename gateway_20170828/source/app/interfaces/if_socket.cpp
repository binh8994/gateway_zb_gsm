/*
 * if_socket.cpp
 *
 *  Created on: Jun 2, 2017
 *      Author: tudt13
 */

#include<arpa/inet.h> //inet_addr
#include<pthread.h> //for threading , link with lpthread
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <time.h>

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <semaphore.h>
#include <iostream>
#include "../ak/ak.h"

#include "../sys/sys_dbg.h"

#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "../znp/zcl.h"
#include "../znp/znp_serial.h"
#include "../../common/global_parameters.h"

#define LISTEN_PORT	5362
#define MAX_CLIENT_CONNECTING 		10

int32_t socks[MAX_CLIENT_CONNECTING + 2];
pthread_t sniffer_threads[MAX_CLIENT_CONNECTING + 2];
int32_t thread_num = 0;
//the thread function
void *connection_handler(void *);
q_msg_t gw_task_if_socket_mailbox;
void* gw_task_if_socket_entry(void*);
static pthread_t if_socket_rx_thread;
static void* if_socket_rx_thread_handler(void*);
int32_t positionSocketEmpty(void);
void* gw_task_if_socket_entry(void*) {

	for (int i = 0; i < MAX_CLIENT_CONNECTING; i++) {
		socks[i] = -1;
		APP_PRINT("sock[%d] = %d", i, socks[i]);
	}
	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("[STARTED] gw_task_if_socket_entry\n");

	pthread_create(&if_socket_rx_thread, NULL, if_socket_rx_thread_handler, NULL);

	while (1) {
		while (msg_available(ID_TASK_IF_SOCKET)) {
			/* get message */
			ak_msg_t* msg = rev_msg(ID_TASK_IF_SOCKET);

			switch (msg->header->sig) {
			case IF_SOCKET_ADD_NEW_ZED_RESPONSE:
			case IF_SOCKET_SENSOR_DATA:
			case IF_SOCKET_LEAVE_DEVICE_RESPONSE: {
				APP_PRINT("thread_num = %d\n", thread_num);
				uint8_t *data = (uint8_t*) msg->header->payload;
				for (unsigned int i = 0; i < msg->header->len; i++) {
					APP_PRINT("%c", data[i]);
				}
				APP_PRINT("\n");
				for (int j = 0; j < MAX_CLIENT_CONNECTING; j++) {
					if (socks[j] >= 0) {
						APP_PRINT("write sock[%d]=%d\n", j, socks[j]);
						int n = send(socks[j], (uint8_t*) msg->header->payload, msg->header->len, 0);
						if (n < 0 && socks[j] != -1) {
							close(socks[j]);
							pthread_cancel(sniffer_threads[j]);
							thread_num--;
							socks[j] = -1;
							APP_PRINT("\nsend socks[%d] = -1\n", j);
						}
					}
				}
			}
				break;
			case IF_SOCKET_ALIVE: {

			}
				break;
			default:
				break;
			}
			/* free message */
			free_msg(msg);
		}

		usleep(1000);
	}

	return (void*) 0;
}

void* if_socket_rx_thread_handler(void*) {
	APP_DBG("if_socket_rx_thread_handler entry successfully!\n");

	int socket_desc, client_sock, c;
	struct sockaddr_in server, clients[MAX_CLIENT_CONNECTING + 2];

	//Create socket
	socket_desc = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_desc == -1) {
		APP_PRINT("Could not create socket");
	}
	puts("Socket created");

	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(LISTEN_PORT);

	/*Set socket option*/
	int32_t option_value = 1;
	if (setsockopt(socket_desc, SOL_SOCKET, SO_REUSEADDR, (const void *) &option_value, sizeof(int32_t)) < 0)
		perror("setsockopt(SO_REUSEADDR) failed");
	//	int timeout = 60000;  // user timeout in milliseconds [ms]
	//	setsockopt (socket_desc, SOL_TCP, TCP_USER_TIMEOUT, (char*) &timeout, sizeof (timeout));
	//Bind
	if (bind(socket_desc, (struct sockaddr *) &server, sizeof(server)) < 0) {
		//print the error message
		perror("bind failed. Error");
		return (void*) 1;
	}
	puts("bind done");

	//try to specify maximum of 3 pending connections for the master socket
	if (listen(socket_desc, MAX_CLIENT_CONNECTING) < 0) {
		perror("listen");
		return (void*) 1;
	}

	//Accept and incoming connection
	puts("Waiting for incoming connections...");

	while (1) {
		if (thread_num < MAX_CLIENT_CONNECTING) {
			int32_t pos = positionSocketEmpty(); //position don't have socket
			c = sizeof(struct sockaddr_in);
			client_sock = accept(socket_desc, (struct sockaddr *) &clients[pos], (socklen_t*) &c);
			if (client_sock < 0) {
				perror("accept failed");
			} else {

				APP_PRINT("Got a connection from %s on port %d\n", inet_ntoa(clients[pos].sin_addr), htons(clients[pos].sin_port));

				socks[pos] = client_sock;
				APP_PRINT("\n pos = %d, client_socks = %d\n", pos, socks[pos]);
				if (pthread_create(&sniffer_threads[pos], NULL, connection_handler, (void*) &socks[pos]) < 0) {
					perror("could not create thread");
					return (void*) 1;
				}
				//Now join the thread , so that we dont terminate before the thread
//				pthread_join(sniffer_threads[pos], NULL);
				thread_num++;
				puts("Handler assigned");
			}
		} else {
			puts("overload clients!");
			//clean all sockets have exist
			for (int i = 0; i < MAX_CLIENT_CONNECTING; i++) {
//				APP_PRINT("pos = %s %d= %s\n", inet_ntoa(clients[pos].sin_addr), i, inet_ntoa(clients[i].sin_addr));
//				if (strcmp(inet_ntoa(clients[i].sin_addr), "0.0.0.0") != 0) {
				if (socks[i] != -1) {
//						if (pos != i) {
//							if (strcmp(inet_ntoa(clients[i].sin_addr), inet_ntoa(clients[pos].sin_addr)) == 0) {

					close(socks[i]);
					pthread_cancel(sniffer_threads[i]);
					thread_num--;
					socks[i] = -1;
					APP_PRINT("\n destroy socks[%d] = -1\n", i);
//							}
//						}
//					}
				}
			}
		}
		usleep(1000);
	}
	close(socket_desc);
	return (void*) 0;
}

/*
 * This will handle connection for each client
 * */
void *connection_handler(void *pos) {
	//Get the socket descriptor
	int sock_client_handle = 0;
	int sock2 = *(int*) pos;
	while (sock_client_handle < MAX_CLIENT_CONNECTING) {
		if (socks[sock_client_handle] == sock2) {
			break;
		}
		sock_client_handle++;
	}

	int32_t read_size = 0;
	uint8_t client_message[MAX_MESSAGE_LEN];
//	fcntl(sock2, F_SETFL, fcntl(sock2, F_GETFL, 0) & ~O_NONBLOCK);
	//Receive a message from client
	APP_PRINT("create thread %d, socks =%d \n", sock_client_handle, socks[sock_client_handle]);
	while (1) {
		APP_PRINT("thread %d, socks =%d \n", sock_client_handle, socks[sock_client_handle]);
		//check close socket
		if (socks[sock_client_handle] == -1) {
			pthread_exit((void *) NULL); /* calling thread will terminate */
			break;
		}
		//Check if it was for closing , and also read the
		read_size = recv(sock2, client_message, MAX_MESSAGE_LEN, 0);
		if (read_size <= 0) {
			break;
		}
///*
//		if (strncmp((const char*)client_message, (const char*)":1006", 5) == 0) {
//			write(sock2, g_strTokenFactory, strlen((const char*) g_strTokenFactory));
//		}
//*/
		int32_t outCommand;
		if (processingDataCommunication(client_message, read_size, ID_TASK_IF_SOCKET, &outCommand) == TRUE) {
			if (outCommand == sensordata) {
				if(write(sock2, lastSensordataMessage, strlen((const char*) lastSensordataMessage))<0){
					break;
				}
			}
		}
		usleep(1000);
	}
	if (read_size == 0) {
		puts("Client disconnected");
		fflush (stdout);
	} else if (read_size == -1) {
		perror("recv failed");
	}

//Free the socket pointer
	thread_num--;
	close(sock2);
//	free(socket_desc);
	socks[sock_client_handle] = -1;
	pthread_cancel(sniffer_threads[sock_client_handle]);
	APP_PRINT("free sock_handle[%d]\n", sock_client_handle);
//

	return (void*) 0;
}

int32_t positionSocketEmpty(void) {
	int32_t i = 0;
	for (i = 0; i < MAX_CLIENT_CONNECTING; i++) {
		if (socks[i] == -1) {
			break;
		}
	}
	return i;
}
