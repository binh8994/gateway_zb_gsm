#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <ctime>
#include <curl/curl.h>
#include "../ak/ak.h"
#include "../ak/timer.h"

#include "../common/global_parameters.h"
#include "../common/aes.h"

#include "app.h"
#include "app_dbg.h"

#include "mqtt_communication.h"
//#include "mqtt_add_gateway.h"

#include "task_list.h"
#include "znp/zcl.h"
#include "znp/znp_serial.h"
using namespace std;

q_msg_t gw_task_mqtt_mailbox;
/*Generate random client id*/
const char alphanum[] = "0123456789"
		"QWERTYUIOPASDFGHJKLZXCVBNM"
		"qwertyuiopasdfghjklzxcvbnm";

const char http_get_config_parameters_mqtt_str[] = "http://smarthome.888999.vn/api/v1/gateway/";

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp) {
	((std::string*) userp)->append((char*) contents, size * nmemb);
	return size * nmemb;
}

void* gw_task_mqtt_entry(void*) {
	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("gw_task_mqtt_entry\n");

//	mqtt_add_gateway* mqttaddgateway = NULL;
	mqtt_communication* mqttcomm = NULL;

	while (1) {
		while (msg_available(ID_TASK_MQTT)) {
			/* get messge */
			ak_msg_t* msg = rev_msg(ID_TASK_MQTT);

			APP_DBG("MQTT task entry\n");

			switch (msg->header->sig) {
			case MQTT_CTRL_ADD_NEW_ZED_REQ: {

				APP_PRINT("MQTT send add new device\n");
				ak_msg_t* s_msg = get_dymanic_msg();
				set_msg_sig(s_msg, ZB_PERMIT_JOINING_REQUEST);
				set_msg_src_task_id(s_msg, ID_TASK_MQTT);
				task_post(ID_TASK_IF_ZNP, s_msg);
			}
				break;
			case MQTT_CTRL_GET_DEVICE_LIST_REQ: {

				APP_PRINT("MQTT send get device list\n");
				uint8_t str_ac_sensor = 0x01;
				ak_msg_t* s_msg = get_dymanic_msg();
				set_msg_sig(s_msg, UTIL_GET_DEVICE_INFO);
				set_data_dynamic_msg(s_msg, (uint8_t*) &str_ac_sensor, 1);

				set_msg_src_task_id(s_msg, ID_TASK_MQTT);
				task_post(ID_TASK_IF_ZNP, s_msg);
			}
				break;

			case MQTT_CTRL_GET_DEVICE_LIST_RESPONSE: {
				if_parseinfo_frame_t parseinfo_data;
				get_data_dynamic_msg(msg, (uint8_t*) &parseinfo_data, sizeof(if_parseinfo_frame_t));

				APP_PRINT("list short address: %d \n", parseinfo_data.data[13]);
				uint16_t *pShortAddrList = (uint16_t*) &parseinfo_data.data[14];
				for (int i = 0; i < parseinfo_data.data[13]; i++) {
					APP_PRINT("device %d : %04X\n", i, pShortAddrList[i]);
				}
			}
				break;
			case MQTT_START_TOKEN_FACTORY: {
				uint8_t *data = (uint8_t*) msg->header->payload;
				memcpy(s_ieee_address, data, 17);
				s_ieee_address[16] = '\0';
				APP_PRINT("[mac= %s]\n", s_ieee_address);
				string aes_key = string((const char*) s_ieee_address) + string((const char*) s_ieee_address);
//				init_mqtt((uint8_t*) aes_key.c_str());
				APP_PRINT("aes_key=%s\n comm = [%02X]\n", aes_key.c_str(), strlen((const char* ) g_config_parameters.token_communicate));
				if (g_config_parameters.token_communicate[0] != 0) {

					/* post message to mqtt task */
					ak_msg_t* s_msg = get_dymanic_msg();
					set_msg_sig(s_msg, MQTT_START_TOKEN_COMMUNICATION);
					set_msg_src_task_id(s_msg, ID_TASK_MQTT);
					task_post(ID_TASK_MQTT, s_msg);

				} else {

					CURL *curl;
					CURLcode res;
					std::string readBuffer;
					string http_get_mqtt_para = string(http_get_config_parameters_mqtt_str) + string((const char*) s_ieee_address) + string("/");
					curl = curl_easy_init();
					if (curl) {
						curl_easy_setopt(curl, CURLOPT_URL, http_get_mqtt_para.c_str());
						curl_easy_setopt(curl, CURLOPT_POSTFIELDS, VALID_SERVER_CODE.c_str());
						curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
						curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
						curl_easy_setopt(curl, CURLOPT_TIMEOUT, 20L);
						/* get it! */
						res = curl_easy_perform(curl);

						/* check for errors */
						if (res != CURLE_OK) {
							APP_PRINT("curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
						} else {
							/*
							 * Now, our chunk.memory points to a memory block that is chunk.size
							 * bytes big and contains the remote file.
							 *
							 * Do something nice with it!
							 */

							APP_PRINT("%s\n", readBuffer.data());
#ifdef AES_ENCRYPT_FLAG

							setKey((unsigned char*) aes_key.c_str(), 32);
							unsigned char*message = (unsigned char*) malloc(readBuffer.length() + 1);
							int len = iot_final_decrypt(message, (unsigned char *) readBuffer.c_str(), readBuffer.length());
							APP_PRINT("len = %d\n", len);
#endif
							if (len > 0) {
								/* post message to mqtt task */
								ak_msg_t* s_msg = get_dymanic_msg();
								set_msg_sig(s_msg, MQTT_ADD_GATEWAY);
								set_data_dynamic_msg(s_msg, message, len);
								set_msg_src_task_id(s_msg, ID_TASK_MQTT);
								task_post(ID_TASK_MQTT, s_msg);
							}
							free(message);
						}

						curl_easy_cleanup(curl);
					}
				}

				{
					ak_msg_t* smsg = get_pure_msg();
					set_msg_sig(smsg, GSM_VERIFY_REQ);
					task_post(ID_TASK_GSM, smsg);
				}
			}
				break;
			case MQTT_ADD_GATEWAY: {
				APP_PRINT("MQTT_ADD_GATEWAY\n");
				uint8_t *data = (uint8_t*) msg->header->payload;
				for (uint32_t j = 0; j < msg->header->len; j++) {
					APP_PRINT("%c", data[j]);
				}
				APP_PRINT("\n");
				parse_communicate_string_t parseMessage = parseClientString(data, msg->header->len);
				if (parseMessage.totalString == 5) {

					int32_t i = 0;
					string_copy(g_config_parameters.mqtt_username, (uint8_t*) &data[parseMessage.position[i]], parseMessage.len[i]);
					i++;
					string_copy(g_config_parameters.mqtt_password, (uint8_t*) &data[parseMessage.position[i]], parseMessage.len[i]);
					i++;
					string_copy(g_config_parameters.mqtt_topic, (uint8_t*) &data[parseMessage.position[i]], parseMessage.len[i]);
					i++;
					string_copy(g_config_parameters.token_communicate, (uint8_t*) &data[parseMessage.position[i]], parseMessage.len[i]);
					i++;
					string_copy(g_config_parameters.security_key, (uint8_t*) &data[parseMessage.position[i]], parseMessage.len[i]);
					i++;
					APP_PRINT("token comm: %s \n", g_config_parameters.token_communicate);

					string aes_key = string((const char*) s_ieee_address) + string((const char*) s_ieee_address);
					gateway_configure.write_config_data(&g_config_parameters, (uint8_t*) aes_key.c_str());

					{
						/* post message to mqtt task */
						ak_msg_t* s_msg = get_dymanic_msg();
						set_msg_sig(s_msg, MQTT_STOP_TOKEN_FACTORY);
//					set_data_dynamic_msg(s_msg, message->payload, message->payloadlen);
						set_msg_src_task_id(s_msg, ID_TASK_MQTT);
						task_post(ID_TASK_MQTT, s_msg);
					}
				}
			}
				break;
			case MQTT_STOP_TOKEN_FACTORY: {
				APP_PRINT("MQTT_STOP_TOKEN_FACTORY\n");

				{
					/* post message to mqtt task */
					ak_msg_t* s_msg = get_dymanic_msg();
					set_msg_sig(s_msg, MQTT_START_TOKEN_COMMUNICATION);
					//					set_data_dynamic_msg(s_msg, message->payload, message->payloadlen);
					set_msg_src_task_id(s_msg, ID_TASK_MQTT);
					task_post(ID_TASK_MQTT, s_msg);
				}
			}
				break;
			case MQTT_START_TOKEN_COMMUNICATION: {
				APP_PRINT("MQTT_START_TOKEN_COMMUNICATION\n");
				/*Add gateway mqtt*/
				string client_id_communication = string("iot-") + s_ieee_address + string("-gwsrv");
				mqttcomm = new mqtt_communication((const char*) client_id_communication.data(), (char*) g_config_parameters.mqtt_host, g_config_parameters.mqtt_port, (char*) g_config_parameters.mqtt_username, (char*) g_config_parameters.mqtt_password);

				// Send last status of gateway
				string comm_topic_status = string((char*) g_config_parameters.mqtt_topic) + string("/status/");
				APP_PRINT(" comm_topic_status: %s \n", comm_topic_status.data());
//				mqttcomm->will_set(comm_topic_status.data(), strlen((const char*) "offline"), "offline", 1, true);

				// Send status when gateway is online
				mqttcomm->set_topic_publish(comm_topic_status.data());
				mqttcomm->communication_public((uint8_t*)"online", strlen((const char*) "online"), true);

				string comm_topic_subscribe = string((char*) g_config_parameters.mqtt_topic) + string("/control/#");
				string comm_topic_subscribe_reset = string((char*) g_config_parameters.mqtt_topic) + string("/reset/");
				string comm_topic_subscribe_scene = string((char*) g_config_parameters.mqtt_topic) + string("/scene/");
				mqttcomm->set_topic_subcribe(comm_topic_subscribe.data());
				mqttcomm->set_topic_subcribe(comm_topic_subscribe_reset.data());
				mqttcomm->set_topic_subcribe(comm_topic_subscribe_scene.data());
				string comm_topic_publish = string((char*) g_config_parameters.mqtt_topic) + string("/data/");

				mqttcomm->set_topic_publish(comm_topic_publish.data());
				g_strTokenCommunicate = string((char*) g_config_parameters.token_communicate);
				APP_PRINT("information: \n");
				APP_PRINT(" user name: %s \n", g_config_parameters.mqtt_username);
				APP_PRINT(" password:%s \n", g_config_parameters.mqtt_password);
				APP_PRINT(" client id: %s \n", client_id_communication.data());
				APP_PRINT(" host: %s \n", g_config_parameters.mqtt_host);
				APP_PRINT(" topic public: %s \n", comm_topic_publish.data());
				APP_PRINT(" topic subscribe: %s \n", comm_topic_subscribe.data());
				APP_PRINT(" port: %d \n", g_config_parameters.mqtt_port);
				APP_PRINT(" key: %d \n", g_config_parameters.security_key);
				APP_PRINT(" start up coordinator success: %d \n", g_config_parameters.start_up_success);
			}
				break;
			case MQTT_DATA_COMMUNICATION: {
				int32_t outCommand;
				if (processingDataCommunication((uint8_t*) msg->header->payload, (int32_t) msg->header->len, ID_TASK_MQTT, &outCommand) == TRUE) {

					if (outCommand == sensordata) {
						if(get_app_connect_cloud_state() == APP_CONNECT_BY_LAN) {
							mqttcomm->communication_public(lastSensordataMessage, strlen((const char*) lastSensordataMessage), true);
						}
						else if (get_app_connect_cloud_state() == APP_CONNECT_BY_GSM) {
							ak_msg_t* smsg = get_dymanic_msg();
							set_data_dynamic_msg(smsg, lastSensordataMessage, strlen((const char*) lastSensordataMessage));
							set_msg_sig(smsg, GSM_MQTT_PUB_MSG);
							set_msg_src_task_id(smsg, ID_TASK_MQTT);
							task_post(ID_TASK_GSM, smsg);
						}
					}
				}

			}
				break;
			case MQTT_SENSOR_DATA: {
				if (mqttcomm != NULL) {
					uint8_t*data = (uint8_t*) msg->header->payload;
					int32_t message_len = *(int32_t*) &data[msg->header->len - sizeof(int32_t)];
					uint8_t topic_len = *(uint8_t*) &data[message_len];
					uint8_t *topic_string;
					if (topic_len > 0) {
						topic_string = (uint8_t*) malloc(topic_len);
						memcpy(topic_string, (uint8_t*) &data[message_len + 1], topic_len);
					} else {
						topic_string = (uint8_t*) malloc(1);
						*topic_string = 0;
					}
//					APP_PRINT("MQTT_SENSOR_DATA: %s\n topic len  = %d, topic_string = %s\n", data, topic_len, topic_string);
					string comm_topic_publish = string((char*) g_config_parameters.mqtt_topic) + string("/data/") + string((char*) topic_string);
//					APP_PRINT("[topic = %s]\n", comm_topic_publish.c_str());
					mqttcomm->set_topic_publish(comm_topic_publish.data());
					if (topic_len == 0) {
						if(get_app_connect_cloud_state() == APP_CONNECT_BY_LAN) {
							mqttcomm->communication_public(data, (int32_t) message_len, false);
						}
						else if (get_app_connect_cloud_state() == APP_CONNECT_BY_GSM) {
							ak_msg_t* smsg = get_dymanic_msg();
							set_data_dynamic_msg(smsg, data, message_len);
							set_msg_sig(smsg, GSM_MQTT_PUB_MSG);
							set_msg_src_task_id(smsg, ID_TASK_MQTT);
							task_post(ID_TASK_GSM, smsg);
						}
					} else {
						if(get_app_connect_cloud_state() == APP_CONNECT_BY_LAN) {
							mqttcomm->communication_public(data, (int32_t) message_len, true);
						}
						else if (get_app_connect_cloud_state() == APP_CONNECT_BY_GSM) {
							ak_msg_t* smsg = get_dymanic_msg();
							set_data_dynamic_msg(smsg, data, message_len);
							set_msg_sig(smsg, GSM_MQTT_PUB_MSG);
							set_msg_src_task_id(smsg, ID_TASK_MQTT);
							task_post(ID_TASK_GSM, smsg);
						}
					}

					free(topic_string);
				}

			}
				break;
			default:
				break;
			}
			/* free message */
			free_msg(msg);
		}

		usleep(1000);
	}

	return (void*) 0;
}
