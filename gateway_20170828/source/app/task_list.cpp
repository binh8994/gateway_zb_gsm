//#include "../ak/timer.h"

#include "task_list.h"

//#include "../ak/message.h"



ak_task_t task_list[] = {
	{	ID_TASK_TIMER,				TASK_PRI_LEVEL_1,	timer_entry							,	&timer_mailbox									,	"timer service"				},
	{	ID_TASK_IF_CONSOLE,			TASK_PRI_LEVEL_1,	gw_task_if_console_entry			,	&gw_task_if_console_mailbox						,	"terminal gate"				},
	{	ID_TASK_IF_ZNP,				TASK_PRI_LEVEL_1,	gw_task_if_znp_entry				,	&gw_task_if_znp_mailbox							,	"znp protocol"				},
	{	ID_TASK_HANDLE_MSG,			TASK_PRI_LEVEL_1,	gw_task_handle_msg_entry			,	&gw_task_handle_msg_mailbox						,	"handle message"			},
	{	ID_TASK_MQTT,				TASK_PRI_LEVEL_1,	gw_task_mqtt_entry					,	&gw_task_mqtt_mailbox							,	"task mqtt"					},
	{	ID_TASK_IF_SOCKET,			TASK_PRI_LEVEL_1,	gw_task_if_socket_entry				,	&gw_task_if_socket_mailbox						,	"task if socket"			},
	{	ID_TASK_LOG,				TASK_PRI_LEVEL_1,	gw_task_log_entry					,	&gw_task_log_mailbox							,	"task log"					},
//	{	ID_TASK_IF_SOCKET_ONLINE,	TASK_PRI_LEVEL_1,	gw_task_if_socket_online_entry		,	&gw_task_if_socket_online_mailbox				,	"task if socket online"		},
	{	ID_TASK_UDP_BROADCAST,		TASK_PRI_LEVEL_1,	gw_task_if_udp_broadcast_entry		,	&gw_task_if_udp_mailbox							,	"task udp broadcast"		},
    {   ID_TASK_SCENE,              TASK_PRI_LEVEL_1,   gw_task_scene_entry                 ,   &gw_task_scene_mailbox                      	,   "task scene data"           },
	{   ID_TASK_GSM,				TASK_PRI_LEVEL_1,   gw_task_gsm_entry					,	&gw_task_gsm_mailbox							,   "task gsm "					},
	//{   ID_TASK_SYNC,				TASK_PRI_LEVEL_1,   gw_task_sync_entry					,	&gw_task_sync_mailbox							,   "task sync "				},
};
