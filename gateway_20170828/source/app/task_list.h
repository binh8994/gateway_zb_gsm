#ifndef __TASK_LIST_H__
#define __TASK_LIST_H__

#include "../ak/ak.h"

#include "interfaces/if_console.h"
#include "interfaces/if_znp.h"
#include "interfaces/if_socket.h"
//#include "interfaces/if_socket_online.h"
#include "interfaces/if_udp_broadcast.h"
#include "task_handle_msg.h"
#include "task_mqtt.h"
#include "task_log.h"
#include "task_scene.h"
#include "task_gsm.h"
//#include "task_sync.h"

/** default if_des_type when get pool memory
 * this define MUST BE corresponding with app.
 */
#define AK_APP_TYPE_IF			(100)

enum {
	ID_TASK_TIMER, 			/*ID task timer*/
	ID_TASK_IF_CONSOLE, 	/*ID task if_console to test data (use another application to control)*/
	ID_TASK_IF_ZNP, 		/*ID task ZNP protocol communicate with zigbee CC2531*/
	ID_TASK_HANDLE_MSG, 	/*ID task handle message: parse message, permission, send mqtt */
	ID_TASK_MQTT,
	ID_TASK_IF_SOCKET,
	ID_TASK_LOG,
//	ID_TASK_IF_SOCKET_ONLINE,			/*ID task handle message: sending sensor data to server*/
	ID_TASK_UDP_BROADCAST,				/*ID task UDP broadcast in the same subnet network*/
    ID_TASK_SCENE,          			/*ID  task scene */
	ID_TASK_GSM,
	//ID_TASK_SYNC,
	AK_TASK_LIST_LEN					/* size of task list table */
};


extern ak_task_t task_list[];

#endif //__TASK_LIST_H__
