#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <semaphore.h>

#include "../ak/ak.h"
#include "../ak/timer.h"

#include "../sys/sys_dbg.h"
#include "../common/fifo.h"
#include "../common/global_parameters.h"

#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_gsm.h"

//#define MODULE_800C

#define GSM_DEVPATH					"/dev/ttyUSB0"
//#define GSM_DEVPATH				"/dev/ttyS2"
#define FIFO_BUF_SIZE				(4096)

#define UART_BUFFER_LENGTH			2048
#define TOPIC_BUFFER_LENGTH			255    //Maximum length allowed Topic
#define MESSAGE_BUFFER_LENGTH		2048  //Maximum length allowed data

#define HOST						"static1.888999.vn"
#define PORT						1883

// ######################################################################################################################
#define CONNECT     1   //Client request to connect to Server                Client          Server
#define CONNACK     2   //Connect Acknowledgment                             Server/Client   Server/Client
#define PUBLISH     3   //Publish message                                    Server/Client   Server/Client
#define PUBACK      4   //Publish Acknowledgment                             Server/Client   Server/Client
#define PUBREC      5   //Publish Received (assured delivery part 1)         Server/Client   Server/Client
#define PUBREL      6   //Publish Release (assured delivery part 2)          Server/Client   Server/Client
#define PUBCOMP     7   //Publish Complete (assured delivery part 3)         Server/Client   Server/Client
#define SUBSCRIBE   8   //Client Subscribe request                           Client          Server
#define SUBACK      9   //Subscribe Acknowledgment                           Server          Client
#define UNSUBSCRIBE 10  //Client Unsubscribe request                         Client          Server
#define UNSUBACK    11  //Unsubscribe Acknowledgment                         Server          Client
#define PINGREQ     12  //PING Request                                       Client          Server
#define PINGRESP    13  //PING Response                                      Server          Client
#define DISCONNECT  14  //Client is Disconnecting                            Client          Server

// QoS value bit 2 bit 1 Description
//   0       0       0   At most once    Fire and Forget         <=1
//   1       0       1   At least once   Acknowledged delivery   >=1
//   2       1       0   Exactly once    Assured delivery        =1
//   3       1       1   Reserved
#define DUP_Mask      8   // Duplicate delivery   Only for QoS>0
#define QoS_Mask      6   // Quality of Service
#define QoS_Scale     2   // (()&QoS)/QoS_Scale
#define RETAIN_Mask   1   // RETAIN flag

#define User_Name_Flag_Mask  128
#define Password_Flag_Mask   64
#define Will_Retain_Mask     32
#define Will_QoS_Mask        24
#define Will_QoS_Scale       8
#define Will_Flag_Mask       4
#define Clean_Session_Mask   2

struct gsm_cfg_t {
	uint8_t APN[20];
	uint8_t APN_user[20];
	uint8_t APN_pass[20];
} gsm_cfg;

struct mqtt_parser_t {
	pthread_mutex_t mt;

	/* using in parser process */
	uint8_t		parser_process;
	uint8_t		parser_index;
	uint32_t	parser_multiplier;
	uint32_t	parser_remain_byte;

	/* mqtt header */
	uint8_t		msg_type;
	uint8_t		QoS;
	uint8_t		DUP;
	uint8_t		RETAIN;
	uint32_t	control_pack_length;
	/* mqtt payload */
	uint8_t		recv_payload[UART_BUFFER_LENGTH];

	/* mqtt parser */
	char		recv_message[MESSAGE_BUFFER_LENGTH];
	uint32_t	recv_message_length;
	char		recv_topic[TOPIC_BUFFER_LENGTH];
	uint8_t		recv_topic_length;

	/* static variable */
	uint8_t		is_connected;
	uint32_t	last_msg_id;

} mqtt_parser;

q_msg_t gw_task_gsm_mailbox;

static int gsm_serial_fd;
static int gsm_serial_opentty(const char* devpath);

static uint64_t epoch_milli ;

static const char keepalive			= 60;//20s
//static const char* APN				= "v-internet";//Viettel
//static const char* APN_USR			= "";
//static const char* APN_PWD			= "";

static const char* mobile_number	="+841648645649";

//static const char* client_id		= "iot-linux-gsm";
//static const char* username			= "QET9pd";
//static const char* password			= "9zMRZyrPZtTEyCp3";

//static const char* sensor_topic		= "sensor_topic";
//static const char* control_topic	= "control_topic";

static fifo_t	uart_fifo;
static uint8_t	uart_fifo_buf[FIFO_BUF_SIZE];
static void		initialise_epoch (void);
static uint32_t millis();
static void		wait_for_sim_power_on(void);

static void		fifo_clear();
static uint32_t sim_puts(const char*);
static uint32_t sim_putc(char);
static uint8_t	send_wait_until(const char*, const char*, uint32_t, uint8_t, uint8_t);
static uint32_t generate_msg_id(void);
static void		send_length(uint32_t);
static void		send_UTF_string(char*);
static int		 read_APN_cfg_file(char * file_name);

/***************************
 * mqtt func
 * *************************/
static void		connect(const char*, char, char, const char*, const char*,\
						char, char, char, char, char*, char*);
static void		disconnect(void);
static void		publish(char, char, char, uint32_t, const char*, const char*);
static void		subscribe(char, uint32_t, const char*, char);
//static void	unsubscribe(char, uint32_t, const char*);

/***************************
 * calback func
 * *************************/
static void		autoconnect();
static void		on_connect();
static void		on_message(char* , uint32_t, char*, uint32_t);

static pthread_t gsm_serial_rx_thread;
static void* gsm_serial_rx_thread_handler(void*);

void* gw_task_gsm_entry(void*) {
	task_mask_started();
	wait_all_tasks_started();

	APP_DBG("[STARTED] mt_task_gsm_entry\n");

	set_app_connect_cloud_state(APP_CONNECT_BY_LAN);
	set_enable_connect_gsm(false);

	wait_for_sim_power_on();

	//read_APN_cfg_file("gsm_config.txt");

	if (gsm_serial_opentty(GSM_DEVPATH) < 0) {
		APP_DBG("[GSM MQTT] Cannot open %s !\n", GSM_DEVPATH);
	}
	else {
		APP_DBG("[GSM MQTT] Opened %s success !\n", GSM_DEVPATH);

		fifo_init(&uart_fifo, uart_fifo_buf, FIFO_BUF_SIZE, sizeof(uint8_t));
		memset( uart_fifo_buf, 0, FIFO_BUF_SIZE);
		pthread_create(&gsm_serial_rx_thread, NULL, gsm_serial_rx_thread_handler, NULL);
	}

	initialise_epoch();
	memset(&gsm_cfg, 0, sizeof(gsm_cfg_t));

	while (1) {
		while (msg_available(ID_TASK_GSM)) {
			/* get message */
			ak_msg_t* msg = rev_msg(ID_TASK_GSM);

			switch (msg->header->sig) {
			case GSM_VERIFY_REQ: {
				APP_DBG("GSM_VERIFY_REQ\n");

				fifo_init(&uart_fifo, uart_fifo_buf, FIFO_BUF_SIZE, sizeof(uint8_t));
				memset( &mqtt_parser, 0, sizeof(mqtt_parser_t));

				usleep(1100000);
				sim_puts("+++");
				usleep(1100000);
				if(send_wait_until("\r\nATE0\r\n", "OK", 2000, GSM_READY_POL, GSM_VERIFY_REQ)) {
					send_wait_until("\r\nAT+GSMBUSY=1\r\n", "OK", 2000, 0, 0);

				}
			}
				break;

			case GSM_READY_POL: {
				APP_DBG("GSM_READY_POL\n");

				if ( send_wait_until("\r\nAT+CREG?\r\n", "0,1", 3000, GSM_CHECK_TCP_STATE, GSM_READY_POL) ) {
					send_wait_until("\r\nAT+CIPSHUT\r\n", "SHUT OK", 5000, 0, 0);

					sim_puts("\r\nAT+CIPMUX=0\r\n");
					usleep(200000);
					sim_puts("\r\nAT+CIPMODE=1\r\n");
					usleep(200000);

					send_wait_until("\r\nAT+CGATT=1\r\n", "OK", 2000, 0, 0);
				}
			}
				break;

			case GSM_CHECK_TCP_STATE: {
				APP_DBG("GSM_CHECK_TCP_STATE\n");

				if ( send_wait_until("\r\nAT+CIPSTATUS\r\n", "STATE", 10000, 0, GSM_VERIFY_REQ) ) {
					usleep(500000);
					if (strstr((const char*)uart_fifo_buf, " INITIAL") ) {
						APP_DBG("GSM INITIAL\n");
						usleep(100000);

						if( read_APN_cfg_file("gsm_config.txt") == 0) {
							//AT+CSTT="v-internet","",""

							APP_DBG("APN:%d:%s\n", strlen((const char *)gsm_cfg.APN), gsm_cfg.APN);
							APP_DBG("APN_user:%d:%s\n", strlen((const char *)gsm_cfg.APN_user), gsm_cfg.APN_user);
							APP_DBG("APN_pass:%d:%s\n", strlen((const char *)gsm_cfg.APN_pass), gsm_cfg.APN_pass);

							sim_puts("\r\nAT+CSTT=");
							sim_puts((const char*)gsm_cfg.APN);			sim_puts(",");
							sim_puts((const char*)gsm_cfg.APN_user);	sim_puts(",");
							sim_puts((const char*)gsm_cfg.APN_pass);

							send_wait_until( "\r\n", "OK", 10000, GSM_CHECK_TCP_STATE, GSM_CHECK_TCP_STATE);
						}
						else {
							timer_set(ID_TASK_GSM, GSM_CHECK_TCP_STATE, 2000, TIMER_ONE_SHOT);
						}

					}
					else if (strstr((const char*)uart_fifo_buf, " START") ) {
						APP_DBG("GSM INITIAL\n");
						usleep(200000);

						send_wait_until( "\r\nAT+CIICR\r\n", "OK", 10000, GSM_CHECK_TCP_STATE, GSM_CHECK_TCP_STATE);
					}
					else if (strstr((const char*)uart_fifo_buf, " IP CONFIG") || \
							 strstr((const char*)uart_fifo_buf, " GPRSACT")) {
						APP_DBG("GSM START\n");
						usleep(300000);

						send_wait_until( "\r\nAT+CIFSR\r\n", ".", 10000, GSM_CHECK_TCP_STATE, GSM_CHECK_TCP_STATE);
					}
					else if ((strstr((const char*)uart_fifo_buf, " STATUS") ) || \
							 (strstr((const char*)uart_fifo_buf, " TCP CLOSED") )) {
						APP_DBG("GSM STATUS\n");
						usleep(400000);

						ak_msg_t* smsg = get_pure_msg();
						set_msg_sig(smsg, GSM_START_TCP);
						task_post(ID_TASK_GSM, smsg);

						break;
					}
					else if (strstr((const char*)uart_fifo_buf, " TCP CONNECTING") ) {
						APP_DBG("GSM CONNECTING\n");
						usleep(1000000);

						ak_msg_t* smsg = get_pure_msg();
						set_msg_sig(smsg, GSM_CHECK_TCP_STATE);
						task_post(ID_TASK_GSM, smsg);

					}
					else if ((strstr((const char*)uart_fifo_buf, " CONNECT OK") ) || \
							 (strstr((const char*)uart_fifo_buf, " CONNECT FAIL") ) || \
							 (strstr((const char*)uart_fifo_buf, " DEACT") )) {
						APP_DBG("GSM CONNECT OK\n");
						usleep(100000);

						mqtt_parser.is_connected = 0;

						timer_remove_attr(ID_TASK_GSM, GSM_MQTT_PING);

						ak_msg_t* smsg = get_pure_msg();
						set_msg_sig(smsg, GSM_SHUT_TCP);
						task_post(ID_TASK_GSM, smsg);

						break;
					}
				}
			}
				break;

			case GSM_START_TCP: {
				APP_DBG("GSM_START_TCP\n");

				char port_str[6];
				sprintf(port_str, "%d", PORT);

				sim_puts("\r\nAT+CIPSTART=\"TCP\",\"");
				sim_puts(HOST);
				sim_puts("\",\"");
				sim_puts(port_str);
				if(send_wait_until( "\"\r\n", "CONNECT", 20000, 0, 0) ) {

					pthread_mutex_lock(&(mqtt_parser.mt));
					mqtt_parser.parser_process = 1;
					mqtt_parser.is_connected = 0;
					pthread_mutex_unlock(&(mqtt_parser.mt));

					if(get_enable_connect_gsm()) {
						autoconnect();
					}
				}
				else {
					APP_DBG("GSM MQTT TCP can not connnect\n");

					pthread_mutex_lock(&(mqtt_parser.mt));
					mqtt_parser.is_connected = 0;
					pthread_mutex_unlock(&(mqtt_parser.mt));

					ak_msg_t* smsg = get_pure_msg();
					set_msg_sig(smsg, GSM_CHECK_TCP_STATE);
					task_post(ID_TASK_GSM, smsg);

				}
			}
				break;

			case GSM_SHUT_TCP:{
				APP_DBG("GSM_SHUT_TCP\n");

				sim_puts("\r\nAT+CIPSHUT\r\n");
				usleep(500000);

				ak_msg_t* smsg = get_pure_msg();
				set_msg_sig(smsg, GSM_VERIFY_REQ);
				task_post(ID_TASK_GSM, smsg);
			}
				break;

			case GSM_MQTT_PING: {

				if(mqtt_parser.is_connected) {
					pthread_mutex_lock(&(mqtt_parser.mt));
					APP_DBG("GSM_MQTT_PING\n");
					sim_putc(char(PINGREQ * 16));
					send_length(0);
					pthread_mutex_unlock(&(mqtt_parser.mt));

				}
			}
				break;

			case GSM_MQTT_SEND_CONNECT: {
				APP_DBG("GSM_MQTT_SEND_CONNECT\n");
				if(mqtt_parser.parser_process == 1) {
					if(!mqtt_parser.is_connected) {
						autoconnect();
					}
				}

			}
				break;

			case GSM_MQTT_SEND_DISCONNECT: {
				APP_DBG("GSM_MQTT_SEND_DISCONNECT\n");
				if(mqtt_parser.parser_process == 1) {
					if( mqtt_parser.is_connected) {
						disconnect();
					}
				}

			}
				break;

			case GSM_MQTT_PUB_MSG:{
				APP_DBG("GSM_MQTT_PUB_MSG\n");
				if(mqtt_parser.is_connected) {
					pthread_mutex_lock(&(mqtt_parser.mt));

					uint8_t* mes = (uint8_t*)malloc(get_data_len_dynamic_msg(msg));
					memset(mes, 0, get_data_len_dynamic_msg(msg));
					get_data_dynamic_msg(msg, mes, get_data_len_dynamic_msg(msg));
					string comm_topic_publish = string((char*) g_config_parameters.mqtt_topic) + string("/data/");
					APP_DBG("comm_topic_publish:%s\n", comm_topic_publish.data());
					APP_DBG("mes_publish:%s\n", mes);

					publish(0, 1, 1, generate_msg_id(), comm_topic_publish.data(), (const char*)mes);
					free(mes);
					pthread_mutex_unlock(&(mqtt_parser.mt));
				}
				else {
					APP_DBG("[GSM MQTT] not connected\n");
				}
			}
				break;

			case GSM_SEND_SMS: {
				APP_DBG("GSM_SEND_SMS\n");

				fifo_clear();
				if(send_wait_until("\r\nAT+CMGF=1\r\n", "OK", 2000,0,0)) {
					string cmd = string("\r\nAT+CMGS=\"") + string(mobile_number) + string("\"\r\n");
					if( send_wait_until(cmd.data(), ">", 1000, 0, 0) ) {
						//Send SMS content
						sim_puts("GATEWAY WARNING");
						usleep(100000);
						//Send Ctrl+Z / ESC to denote SMS message is complete
						sim_putc((char)26);
						usleep(100000);
					}
				}

			}
				break;

			case GSM_AT_CMD: {
				APP_DBG("GSM_AT_CMD\n");

				fifo_clear();

				uint8_t* cmd_s = (uint8_t*) malloc(get_data_len_dynamic_msg(msg));
				memset(cmd_s, 0, get_data_len_dynamic_msg(msg));
				get_data_dynamic_msg(msg, cmd_s, get_data_len_dynamic_msg(msg));
				sim_puts((const char*)cmd_s);
				sim_puts("\r\n");
				free(cmd_s);
			}


			default:
				break;
			}

			/* free message */
			free_msg(msg);
		}

		usleep(100);
	}

	return (void*)0;

}

int gsm_serial_opentty(const char* devpath) {
	struct termios options;
	SYS_DBG("[GSM SERIAL][gsm_serial_opentty] devpath: %s\n", devpath);

	gsm_serial_fd = open(devpath, O_RDWR | O_NOCTTY | O_NDELAY);
	if (gsm_serial_fd < 0) {
		return gsm_serial_fd;
	}
	else {
		fcntl(gsm_serial_fd, F_SETFL, 0);

		/* get current status */
		tcgetattr(gsm_serial_fd, &options);

		cfsetispeed(&options, B9600);
		cfsetospeed(&options, B9600);

		/* No parity (8N1) */
		options.c_cflag &= ~PARENB;
		options.c_cflag &= ~CSTOPB;
		options.c_cflag &= ~CSIZE;
		options.c_cflag |= CS8;

		options.c_cflag |= (CLOCAL | CREAD);
		options.c_cflag     &=  ~CRTSCTS;

		cfmakeraw(&options);

		tcflush(gsm_serial_fd, TCIFLUSH);
		if (tcsetattr (gsm_serial_fd, TCSANOW, &options) != 0) {
			SYS_DBG("error in tcsetattr()\n");
		}
	}
	return 0;
}

void* gsm_serial_rx_thread_handler(void*) {
	APP_DBG("gsm_serial_rx_thread_handler entry successed!\n");

	fifo_init(&uart_fifo, uart_fifo_buf, FIFO_BUF_SIZE, sizeof(uint8_t));

	//	ak_msg_t* smsg = get_pure_msg();
	//	set_msg_sig(smsg, GSM_VERIFY_REQ);
	//	task_post(ID_TASK_GSM, smsg);

	char inChar;
	while(1) {

BEGIN:
		if(read(gsm_serial_fd, &inChar, 1) > 0) {

			if(!mqtt_parser.parser_process) {
				fifo_put(&uart_fifo, &inChar);
				//printf("%c", inChar);
			}
			else {

				mqtt_parser.msg_type = (inChar / 16) & 0x0F;
				mqtt_parser.DUP = (inChar & DUP_Mask) / DUP_Mask;
				mqtt_parser.QoS = (inChar & QoS_Mask) / QoS_Scale;
				mqtt_parser.RETAIN = (inChar & RETAIN_Mask);

				if ((mqtt_parser.msg_type >= CONNECT) && (mqtt_parser.msg_type <= DISCONNECT)) {
					mqtt_parser.control_pack_length = 0;
					mqtt_parser.parser_multiplier = 1;
					usleep(10000);
					char Cchar = inChar;

					while (1) {
						if(read(gsm_serial_fd, &inChar, 1) > 0) {
							if ( ((Cchar == 'C') && (inChar == 'L') && (mqtt_parser.control_pack_length == 0)) || \
								 ( (Cchar == '+') && (inChar == 'P') && (mqtt_parser.control_pack_length == 0))){

								mqtt_parser.parser_index = 0;
								mqtt_parser.parser_process = 0;
								mqtt_parser.is_connected = 0;
								timer_remove_attr(ID_TASK_GSM, GSM_MQTT_PING);

								ak_msg_t* smsg = get_pure_msg();
								set_msg_sig(smsg, GSM_CHECK_TCP_STATE);
								task_post(ID_TASK_GSM, smsg);

								APP_DBG("[GSM MQTT] disconnecting\n");

								goto BEGIN;

							}
							else{
								if ((inChar & 128) == 128){
									mqtt_parser.control_pack_length += (inChar & 127) *  mqtt_parser.parser_multiplier;
									mqtt_parser.parser_multiplier *= 128;
								}
								else{
									mqtt_parser.control_pack_length += (inChar & 127) *  mqtt_parser.parser_multiplier;
									mqtt_parser.parser_multiplier *= 128;
									break;
								}
							}
						}
						usleep(500);
					}

					mqtt_parser.parser_remain_byte = mqtt_parser.control_pack_length;
					mqtt_parser.parser_index = 0;
					/* read all byte in payload */
					while ( (mqtt_parser.parser_remain_byte --) > 0 ) {
						if(read(gsm_serial_fd, &inChar, 1) > 0){
							mqtt_parser.recv_payload[mqtt_parser.parser_index ++] = inChar;
						}
						usleep(500);
					}
					/* handle mqtt message */
					if (mqtt_parser.msg_type == CONNACK){
						//APP_DBG("CONNACK\n");

						uint32_t ConnectionAck = mqtt_parser.recv_payload[0] * 256 + mqtt_parser.recv_payload[1];
						if (ConnectionAck == 0){
							APP_DBG("CONNACK OK: %d\n", ConnectionAck);
							if(keepalive) {
								timer_set(ID_TASK_GSM, GSM_MQTT_PING, keepalive*1000, TIMER_PERIODIC);
							}

							mqtt_parser.is_connected = 1;
							on_connect();

						} else {
							APP_DBG("CONNACK: %d\n", ConnectionAck);
						}
					}
					else if (mqtt_parser.msg_type == PUBLISH){
						//APP_DBG("PUBLISH\n");

						uint32_t topic_len, msg_len;
						uint32_t begin;
						uint32_t msg_id;

						topic_len = mqtt_parser.recv_payload[0] *256 + mqtt_parser.recv_payload[1];

						begin = 2;
						memcpy(mqtt_parser.recv_topic, &mqtt_parser.recv_payload[begin], topic_len);
						mqtt_parser.recv_topic[topic_len] = 0;

						begin = topic_len + 2;
						msg_id = 0;

						if (mqtt_parser.QoS != 0){
							begin += 2;
							msg_id = mqtt_parser.recv_payload[topic_len + 2] * 256 + mqtt_parser.recv_payload[topic_len + 3];
						}

						msg_len = mqtt_parser.control_pack_length - begin;

						memcpy(mqtt_parser.recv_message, &mqtt_parser.recv_payload[begin], msg_len);
						mqtt_parser.recv_message[msg_len] = 0;

						if (mqtt_parser.QoS == 1){
							sim_putc(char(PUBACK * 16));
							send_length(2);
							sim_putc(char(msg_id / 256));
							sim_putc(char(msg_id % 256));

						}
						else if (mqtt_parser.QoS == 2){
							sim_putc(char(PUBREC * 16));
							send_length(2);
							sim_putc(char(msg_id / 256));
							sim_putc(char(msg_id % 256));
						}

						/* on message of control topic */
						on_message(mqtt_parser.recv_topic, topic_len, mqtt_parser.recv_message, msg_len);
					}

					else if (mqtt_parser.msg_type == PUBREC){
						//APP_DBG("PUBREC\n");

						sim_putc(char(PUBREL * 16 + 0 * DUP_Mask + 1 * QoS_Scale));
						send_length(2);
						sim_putc(mqtt_parser.recv_payload[0]);
						sim_putc(mqtt_parser.recv_payload[1]);
					}
					else if (mqtt_parser.msg_type == PUBREL){
						//APP_DBG("PUBREL\n");

						sim_putc(char(PUBCOMP * 16));
						send_length(2);
						sim_putc(mqtt_parser.recv_payload[0]);
						sim_putc(mqtt_parser.recv_payload[1]);
					}
					else if (mqtt_parser.msg_type == PUBACK) {
						APP_DBG("PUBACK\n");
					}
					else if (mqtt_parser.msg_type == PUBCOMP){
						//APP_DBG("PUBCOMP\n");
					}
					else if (mqtt_parser.msg_type == SUBACK){
						APP_DBG("SUBACK\n");
					}
					else if (mqtt_parser.msg_type == UNSUBACK){
						//APP_DBG("UNSUBACK\n");
					}
					else if (mqtt_parser.msg_type == DISCONNECT){
						APP_DBG("DISCONNECT\n");
					}

				}
			}
		}
		else{
			APP_DBG("Error from read tty\n");
			close(gsm_serial_fd);

			FATAL("GSM", 0x01);
		}
		usleep(500);
	}

	return (void*)0;
}

void fifo_clear() {
	fifo_init(&uart_fifo, uart_fifo_buf, FIFO_BUF_SIZE, sizeof(uint8_t));
	memset( uart_fifo_buf, 0, FIFO_BUF_SIZE);
}

uint32_t sim_puts(const char * s) {
	return write(gsm_serial_fd, s, strlen(s));
}

uint32_t sim_putc(char c) {
	return write(gsm_serial_fd, &c, 1);
}

void initialise_epoch () {
	struct timeval tv ;
	gettimeofday (&tv, NULL) ;
	epoch_milli = (uint64_t)tv.tv_sec * (uint64_t)1000 + (uint64_t)(tv.tv_usec / 1000) ;
}

uint32_t millis() {
	uint64_t now ;
	struct timeval tv ;
	gettimeofday (&tv, NULL) ;
	now  = (uint64_t)tv.tv_sec * (uint64_t)1000 + (uint64_t)(tv.tv_usec / 1000) ;
	return (uint32_t)(now - epoch_milli) ;
}

uint32_t generate_msg_id() {
	if (mqtt_parser.last_msg_id < 65535) {
		return ++mqtt_parser.last_msg_id;
	}
	else{
		mqtt_parser.last_msg_id = 0;
		return mqtt_parser.last_msg_id;
	}
}

void send_UTF_string(char *s) {
	uint32_t localLength = strlen(s);
	sim_putc(char(localLength / 256));
	sim_putc(char(localLength % 256));
	sim_puts((char*)s);
}

void send_length(uint32_t len) {
	bool  length_flag = false;
	while (length_flag == false){
		if ((len / 128) > 0){
			sim_putc(char(len % 128 + 128));
			len /= 128;
		}
		else{
			length_flag = true;
			sim_putc(char(len));
		}
	}
}

void connect(const char *ClientIdentifier, char UserNameFlag, char PasswordFlag, const char *UserName, const char *Password,\
			 char CleanSession, char WillFlag, char WillQoS, char WillRetain, char *WillTopic, char *WillMessage) {
	APP_DBG("GSM MQTT connect\n");

	sim_putc(char(CONNECT * 16 ));
	char ProtocolName[7] = "MQIsdp";
	char ProtocolVersion = 3;
	int localLength = (2 + strlen(ProtocolName)) + 1 + 3 + (2 + strlen(ClientIdentifier));

	if (WillFlag != 0){
		localLength = localLength + 2 + strlen(WillTopic) + 2 + strlen(WillMessage);
	}

	if (UserNameFlag != 0){
		localLength = localLength + 2 + strlen(UserName);

		if (PasswordFlag != 0){
			localLength = localLength + 2 + strlen(Password);
		}
	}

	send_length(localLength);
	send_UTF_string(ProtocolName);
	sim_putc(char(ProtocolVersion));
	sim_putc(char(UserNameFlag * User_Name_Flag_Mask + PasswordFlag * Password_Flag_Mask + WillRetain * Will_Retain_Mask + WillQoS * Will_QoS_Scale + WillFlag * Will_Flag_Mask + CleanSession * Clean_Session_Mask));
	sim_putc(char(keepalive / 256));
	sim_putc(char(keepalive % 256));
	send_UTF_string((char*)ClientIdentifier);

	if (WillFlag != 0){
		send_UTF_string(WillTopic);
		send_UTF_string(WillMessage);
	}

	if (UserNameFlag != 0){
		send_UTF_string((char*)UserName);
		if (PasswordFlag != 0){
			send_UTF_string((char*)Password);
		}
	}
}



void publish(char DUP, char Qos, char RETAIN,\
			 uint32_t MessageID, const char *Topic, const char *Message){
	APP_DBG("GSM MQTT publish\n");

	sim_putc(char(PUBLISH * 16 + DUP * DUP_Mask + Qos * QoS_Scale + RETAIN));
	int localLength = (2 + strlen(Topic));

	if (Qos > 0){
		localLength += 2;
	}
	localLength += strlen(Message);
	send_length(localLength);
	send_UTF_string((char*)Topic);

	if (Qos > 0){
		sim_putc(char(MessageID / 256));
		sim_putc(char(MessageID % 256));
	}

	sim_puts(Message);
}

void subscribe(char DUP, uint32_t MessageID, const char *SubTopic, char SubQoS) {
	APP_DBG("GSM MQTT subscribe\n");
	sim_putc(char(SUBSCRIBE * 16 + DUP * DUP_Mask + 1 * QoS_Scale));
	int localLength = 2 + (2 + strlen(SubTopic)) + 1;
	send_length(localLength);
	sim_putc(char(MessageID / 256));
	sim_putc(char(MessageID % 256));
	send_UTF_string((char *)SubTopic);
	sim_putc(SubQoS);

}

//void unsubscribe(char DUP, uint32_t MessageID, char *SubTopic) {
//	APP_DBG("GSM MQTT unsubscribe\n");
//	sim_putc(char(UNSUBSCRIBE * 16 + DUP * DUP_Mask + 1 * QoS_Scale));
//	int localLength = (2 + strlen(SubTopic)) + 2;
//	send_length(localLength);

//	sim_putc(char(MessageID / 256));
//	sim_putc(char(MessageID % 256));

//	send_UTF_string(SubTopic);
//}


void disconnect(void) {
	APP_DBG("[GSM MQTT] disconnect\n");

	sim_putc(char(DISCONNECT * 16));
	send_length(0);
	timer_remove_attr(ID_TASK_GSM, GSM_MQTT_PING);
	mqtt_parser.is_connected = 0;
}


void autoconnect(){
	APP_DBG("GSM MQTT autoconnect\n");

	string client_id_communication = string("iot-sim-gwsrv");
	string comm_topic_status = string((char*) g_config_parameters.mqtt_topic) + string("/status/");
	APP_DBG("lient id:%s\n", client_id_communication.data());
	APP_DBG("user:%s\n", g_config_parameters.mqtt_username);
	APP_DBG("pass:%s\n", g_config_parameters.mqtt_password);
	APP_DBG("topic:%s\n", comm_topic_status.data());

	connect(client_id_communication.data(), \
			1, 1, (const char*)g_config_parameters.mqtt_username, (const char*)g_config_parameters.mqtt_password,\
			0, 1, 1, 1, (char*)comm_topic_status.data(), (char*)"offline");
}

void on_connect() {
	APP_DBG("GSM MQTT onconnect\n");

	set_app_connect_cloud_state(APP_CONNECT_BY_GSM);

	// Send last status of gateway
	string comm_topic_status = string((char*) g_config_parameters.mqtt_topic) + string("/status/");
	// Send status when gateway is online
	publish(0, 1, 1, generate_msg_id(), (const char*)comm_topic_status.data(), "online");

	string comm_topic_subscribe = string((char*) g_config_parameters.mqtt_topic) + string("/control/#");
	string comm_topic_subscribe_reset = string((char*) g_config_parameters.mqtt_topic) + string("/reset/");
	string comm_topic_subscribe_scene = string((char*) g_config_parameters.mqtt_topic) + string("/scene/");

	subscribe(0, generate_msg_id(), comm_topic_subscribe.data(), 1);
	subscribe(0, generate_msg_id(), comm_topic_subscribe_reset.data(), 1);
	subscribe(0, generate_msg_id(), comm_topic_subscribe_scene.data(), 1);
}

void on_message(char* topic, uint32_t topic_len, char * msg, uint32_t msg_len) {
	APP_DBG("on_message\n");
	APP_DBG("topic:%d:	%s\n", topic_len, topic);
	APP_DBG("message:%d:	%s\n", msg_len,msg);

	if (msg_len > 0) {
		APP_DBG("[GSM MQTT][on _message] topic:%s\tpayloadlen:%d\n", topic, msg_len);

#ifdef AES_ENCRYPT_FLAG
		int len = remove_all_chars((char*) msg, '\n', msg_len);
#else
		int len = msg_len;
#endif

		/* post message to mqtt task */
		ak_msg_t* s_msg = get_dymanic_msg();
		set_msg_sig(s_msg, MQTT_DATA_COMMUNICATION);
		set_data_dynamic_msg(s_msg, (uint8_t*) msg, len);

		set_msg_src_task_id(s_msg, ID_TASK_MQTT);
		task_post(ID_TASK_MQTT, s_msg);

		if (mqtt_parser.RETAIN == 1) {
			publish(0, 1, 1, generate_msg_id(), topic, "");
		}

	}

}

uint8_t send_wait_until(const char* cmd, const char* exp, uint32_t timeout, uint8_t success_sig, uint8_t timeout_sig) {

	fifo_clear();

	if (cmd != NULL) {
		sim_puts(cmd);
	}

	uint32_t prev_millis = millis();

	while ( millis() - prev_millis < timeout ){
		if(strstr( (const char*)uart_fifo_buf, exp)){

			if(success_sig) {
				ak_msg_t* smsg = get_pure_msg();
				set_msg_sig(smsg, success_sig);
				task_post(ID_TASK_GSM, smsg);
			}

			return 1;
		}
		usleep(1000);
	}

	if(timeout_sig) {
		ak_msg_t* smsg = get_pure_msg();
		set_msg_sig(smsg, timeout_sig);
		task_post(ID_TASK_GSM, smsg);
	}

	return 0;
}

void wait_for_sim_power_on(void) {
#if defined(MODULE_800L)
	//Reset pin 800L
	system("echo 3  > /sys/class/gpio/export");
	system("echo 0 > /sys/class/gpio/gpio3/active_low");
	//Reset sim 800L
	system("echo 0 > /sys/class/gpio/gpio3/value");
	system("echo out > /sys/class/gpio/gpio3/direction");
	usleep(500000);
	system("echo 1 > /sys/class/gpio/gpio3/value");
#endif

#if defined(MODULE_800C)
	FILE* fd = NULL;
	char buff[2];
	int status = 0;

	//Status pin 800C
	system("echo 65 > /sys/class/gpio/export");
	//Pwkey pin 800C
	system("echo 66 > /sys/class/gpio/export");

	system("echo 0 > /sys/class/gpio/gpio65/active_low");
	system("echo 0 > /sys/class/gpio/gpio66/active_low");

	//Triger pwkpin sim 800C
	system("echo 1 > /sys/class/gpio/gpio66/value");
	system("echo out > /sys/class/gpio/gpio66/direction");
	//usleep(1000000);
	//system("echo 0 > /sys/class/gpio/gpio66/value");

	//Status pin is INPUT
	system("echo in > /sys/class/gpio/gpio65/direction");

	while(status != 49) {
		if((fd = fopen("/sys/class/gpio/gpio65/value", "r"))<0) {
			APP_DBG("[GSM MQTT] open /gpio65/value\n");
			return 0;
		}

		fscanf(fd, "%s", buff);
		usleep(500000);
		APP_DBG("[GSM MQTT] POWER SIM IS OFF\n");
		status = (int)buff[0];
	}
	APP_DBG("[GSM MQTT] POWER SIM IS ON\n");
	return 1;
#endif
}

int read_APN_cfg_file(char * file_name) {
	struct stat file_info;
	int32_t cfg_file_obj = -1;
	int buffer_len;
	char* buffer;

	string path = static_cast<string>(APP_ROOT_PATH) + static_cast<string>("/config/") + static_cast<string>(file_name);

	/*Open config file*/
	cfg_file_obj = open(path.data(), O_RDONLY);

	if (cfg_file_obj < 0) {
		return -1;
	}

	fstat(cfg_file_obj, &file_info);
	buffer_len = file_info.st_size + 1;
	buffer = (char*) malloc(buffer_len);
	buffer[buffer_len - 1] = '\0';

	if (buffer == NULL) {
		return -1;
	}

	/*get data*/
	pread(cfg_file_obj, buffer, file_info.st_size, 0);
	//APP_PRINT("buffer: %s\n", buffer);
	close(cfg_file_obj);

	/*Parse data*/
	//"v-internet"
	//""
	//""

	char* p1 = buffer;
	char* p2 = strstr(buffer, "\n");
	//APP_DBG("p1:%s\n", p1);
	//APP_DBG("p2:%s\n", p2);
	memcpy(gsm_cfg.APN, p1, p2 - p1);
	memset(p1, 0, p2 - p1 + 1);


	p1 = p2 + 1;
	p2 = strstr(p1, "\n");
	//APP_DBG("p1:%s\n", p1);
	//APP_DBG("p2:%s\n", p2);
	memcpy(gsm_cfg.APN_user, p1, p2 - p1);
	memset(p1, 0, p2 - p1 + 1);

	p1 = p2 + 1;
	p2 = strstr(p1, "\n");
	//APP_DBG("p1:%s\n", p1);
	//APP_DBG("p2:%s\n", p2);
	memcpy(gsm_cfg.APN_pass, p1, p2 - p1);
	memset(p1, 0, p2 - p1 + 1);

//	APP_DBG("APN:%d:%s\n", strlen((const char *)gsm_cfg.APN), gsm_cfg.APN);
//	APP_DBG("APN_user:%d:%s\n", strlen((const char *)gsm_cfg.APN_user), gsm_cfg.APN_user);
//	APP_DBG("APN_pass:%d:%s\n", strlen((const char *)gsm_cfg.APN_pass), gsm_cfg.APN_pass);

	free(buffer);
	return 0;

}
