#ifndef _AES_H_
#define _AES_H_

#ifdef __cplusplus
extern "C" {
#endif

int setKey(unsigned char *key, unsigned key_len);
void handleErrors(void);
int aes_encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key, unsigned char *iv, unsigned char *ciphertext);
int aes_decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *key, unsigned char *iv, unsigned char *plaintext);
int iot_final_encrypt(unsigned char* client_message, int client_message_length);
int iot_final_decrypt(unsigned char* decryptedtext, unsigned char* ciphertext, int len_ciphertext);
#ifdef __cplusplus
}
#endif

#endif //_AES_H_
