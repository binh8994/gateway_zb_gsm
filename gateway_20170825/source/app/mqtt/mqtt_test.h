#ifndef __MQTT_TEST_H__
#define __MQTT_TEST_H__


#include <stdint.h>
#include <string>

#include <mosquittopp.h>

using namespace std;

class mqtt_test: public mosqpp::mosquittopp {
public:
	mqtt_test(const char *id, const char *host, int port, char* username, char* password);
	~mqtt_test();

	void test_public(const char* topic, uint8_t* msg, uint32_t len, bool retain = false);

	/* call back functions */
	void on_connect(int rc);
	void on_disconnect(int rc);
	void on_message(const struct mosquitto_message *message);

private:
	char m_connect_ok_flag;
	int m_mid;
};

#endif // __MQTT_TEST_H__
