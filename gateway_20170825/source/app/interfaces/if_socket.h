/*
 * if_socket.h
 *
 *  Created on: Jun 2, 2017
 *      Author: User
 */

#ifndef SOURCE_APP_INTERFACES_IF_SOCKET_H_
#define SOURCE_APP_INTERFACES_IF_SOCKET_H_

extern q_msg_t gw_task_if_socket_mailbox;
extern void* gw_task_if_socket_entry(void*);

#endif /* SOURCE_APP_INTERFACES_IF_SOCKET_H_ */
